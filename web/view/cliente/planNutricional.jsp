<%@page import="controller.ctrPlanNutricional"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>


<%-- 
    Document   : planNutricional
    Created on : 18-mar-2016, 14:25:15
    Author     : daniel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="model.mdlPlanNutricional"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="headerCliente.jsp"/>
<%
    String idPersona = String.valueOf(ht.getAttribute("idpersona"));
    mdlPlanNutricional planN = new mdlPlanNutricional();
    planN.setIdpersona(Integer.parseInt(idPersona));
    ResultSet res = planN.planNutricionalCliente();
    ctrPlanNutricional alimento = new ctrPlanNutricional();
    ResultSet ali = alimento.listarAlimento();
%>

<table  class="table table-bordered table-vmiddle">
    <caption>Plan Nutricional</caption>

    <colgroup span="1" style="color:red;" />
    <colgroup span="3" style="color:blue;" />

    <thead>
        <tr>
            <%
                ali.beforeFirst();
                while (ali.next()) {
            %>
            <th scope="col"><%=ali.getString("nombre")%></th>
                <%}%>
        </tr>
    </thead>

    <tbody>
        <%
            res.beforeFirst();
            while (res.next()) {
        %>
        <tr>
            <%
                if (res.getString("alimento").equals("Frutas") && res.getString("horario") != null) {
            %>
            <td><%=res.getString("porcion")%> Porcion En <%=res.getString("horario")%></td>
            <%} else {%>
            <td></td>
            <%}%>
            <%
                if (res.getString("alimento").equals("Vegetales") && res.getString("horario") != null) {
            %>
            <td><%=res.getString("porcion")%> Porcion En <%=res.getString("horario")%></td>
            <%} else {%>
            <td></td>
            <%}%>
            <%
                if (res.getString("alimento").equals("Proteinas") && res.getString("horario") != null) {
            %>
            <td><%=res.getString("porcion")%> Porcion En <%=res.getString("horario")%></td>
            <%} else {%>
            <td></td>
            <%}%>
            <%
                if (res.getString("alimento").equals("Harinas") && res.getString("horario") != null) {
            %>
            <td><%=res.getString("porcion")%> Porcion En <%=res.getString("horario")%></td>
            <%} else {%>
            <td></td>
            <%}%>
            <%
                if (res.getString("alimento").equals("Bebidas") && res.getString("horario") != null) {
            %>
            <td><%=res.getString("porcion")%> Porcion En <%=res.getString("horario")%></td>
            <%} else {%>
            <td></td>
            <%}%>
        </tr>
        <%}%>
    </tbody>
</table>
<br><br>
<table class="table table-bordered table-vmiddle">
    <thead>
        <tr>
            <th>Horario</th>
            <th>Alimento</th>
            <th>Porcion</th>
        </tr>
    </thead>
    <tbody>
        <%
            res.beforeFirst();
            while (res.next()) {
        %>
        <tr>
            <td><%=res.getString("horario")%></td>
            <td><%=res.getString("alimento")%></td>
            <td><%=res.getString("porcion")%></td>
        </tr>
        <%}%>
    </tbody>
</table>

<jsp:include page="footerCliente.jsp"/>
<script>

    $("#planNutricional").each(function () {
        var $this = $(this);
        var newrows = [];
        $this.find("tr").each(function () {
            var i = 0;
            $(this).find("td").each(function () {
                i++;
                if (newrows[i] === undefined) {
                    newrows[i] = $("<tr></tr>");
                }
                newrows[i].append($(this));
            });
        });
        $this.find("tr").remove();
        $.each(newrows, function () {
            $this.append(this);
        });
    });

</script>
<%        } else {
        response.sendRedirect("../../index.jsp");
    }
%>