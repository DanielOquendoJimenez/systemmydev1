<%-- 
    Document   : inicioCliente
    Created on : 25-nov-2015, 23:06:03
    Author     : daniel
--%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="headerCliente.jsp"/>

<style>
    .timeline {
        position: relative;
        padding: 21px 0px 10px;
        margin-top: 4px;
        margin-bottom: 30px;
    }

    .timeline .line {
        position: absolute;
        width: 4px;
        display: block;
        background: currentColor;
        top: 0px;
        bottom: 0px;
        margin-left: 30px;
    }

    .timeline .separator {
        border-top: 1px solid currentColor;
        padding: 5px;
        padding-left: 40px;
        font-style: italic;
        font-size: .9em;
        margin-left: 30px;
    }

    .timeline .line::before { top: -4px; }
    .timeline .line::after { bottom: -4px; }
    .timeline .line::before,
    .timeline .line::after {
        content: '';
        position: absolute;
        left: -4px;
        width: 12px;
        height: 12px;
        display: block;
        border-radius: 50%;
        background: currentColor;
    }

    .timeline .panel {
        position: relative;
        margin: 10px 0px 21px 70px;
        clear: both;
    }

    .timeline .panel::before {
        position: absolute;
        display: block;
        top: 8px;
        left: -24px;
        content: '';
        width: 0px;
        height: 0px;
        border: inherit;
        border-width: 12px;
        border-top-color: transparent;
        border-bottom-color: transparent;
        border-left-color: transparent;
    }

    .timeline .panel .panel-heading.icon * { font-size: 20px; vertical-align: middle; line-height: 40px; }
    .timeline .panel .panel-heading.icon {
        position: absolute;
        left: -59px;
        display: block;
        width: 40px;
        height: 40px;
        padding: 0px;
        border-radius: 50%;
        text-align: center;
        float: left;
    }

    .timeline .panel-outline {
        border-color: transparent;
        background: transparent;
        box-shadow: none;
    }

    .timeline .panel-outline .panel-body {
        padding: 10px 0px;
    }

    .timeline .panel-outline .panel-heading:not(.icon),
    .timeline .panel-outline .panel-footer {
        display: none;
    }

</style>
<div class="container">

    <!-- Timeline -->
    <div class="timeline">

        <!-- Line component -->
        <h3>&nbsp</h3>
        <div class="line text-muted">  
            <h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lunes</h2>
        </div>

        <!-- Separator -->
        <div class="separator text-muted">
            <time></time>
        </div>
        <!-- /Separator -->

        <article class="panel panel-primary">
            <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="lunes">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->

        <!-- Separator -->
        <div class="separator text-muted">
            <h2> Martes</h2>
        </div>
        <!-- /Separator -->

        <!-- Panel -->
        <article class="panel panel-success">
            <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="martes">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->

        <!-- Separator -->
        <div class="separator text-muted">
            <h2> Miércoles</h2>
        </div>
        <!-- /Separator -->

        <!-- Panel -->
        <article class="panel panel-success">
           <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="miercoles">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->

        <!-- Separator -->
        <div class="separator text-muted">
            <h2> Jueves</h2>
        </div>
        <!-- /Separator -->

        <!-- Panel -->
        <article class="panel panel-success">
           <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="jueves">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->

        <!-- Separator -->
        <div class="separator text-muted">
            <h2> Viernes</h2>
        </div>
        <!-- /Separator -->

        <!-- Panel -->
        <article class="panel panel-success">
           <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="viernes">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->

        <!-- Separator -->
        <div class="separator text-muted">
            <h2> Sábado</h2>
        </div>
        <!-- /Separator -->

        <!-- Panel -->
        <article class="panel panel-success">
            <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="sabado">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->

        <!-- Separator -->
        <div class="separator text-muted">
            <h2> Domingo</h2>
        </div>
        <!-- /Separator -->

        <!-- Panel -->
        <article class="panel panel-success">
            <div class="panel-body">
                <div class="row" id="contente-list">
                    <div id="domingo">

                    </div>
                </div>                              
            </div>
        </article>
        <!-- /Panel -->
    </div>
    <!-- /Timeline -->

</div>

<jsp:include page="footerCliente.jsp"/>

<script>
    $(document).ready(function () {

        var request = <%=ht.getAttribute("idpersona")%>;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {request: request},
            url: "../../ctrPlanEntrenamiento"
        }).done(function (data) {
            console.log(data);
            $("#lunes").html(data.lunes);
            $("#martes").html(data.martes);
            $("#miercoles").html(data.miercoles);
            $("#jueves").html(data.jueves);
            $("#viernes").html(data.viernes);
            $("#sabado").html(data.sabado);
            $("#domingo").html(data.domingo);

        }).fail(function (e) {
            console.log(e);
        });
    });
</script>

<%        } else {
        response.sendRedirect("../../index.jsp");
    }
%>