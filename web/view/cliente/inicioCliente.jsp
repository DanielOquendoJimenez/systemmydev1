<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>

<%-- 
    Document   : inicioCliente
    Created on : 25-nov-2015, 23:06:03
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="headerCliente.jsp"/>
<div class="container">


    <div class="jumbotron"> <!-- Destacar título -->
        <div class="container-fluid"> <!-- CLASSE QUE DEFINE O CONTAINER COMO FLUIDO (100%) -->
            <h1 class="text-center">IMC</h1>

            
                <fieldset>
                    <legend class="text-center">Calculandor de IMC</legend> <!-- Form Name -->

                    <!-- Text input-->
                    <div class="control-group">
                        <div class="controls">
                            <input id="peso" name="peso" id="peso" type="text" placeholder="Digite su peso" class="form-control" required="">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                        <div class="controls">
                            <br>
                            <input id="altura" name="altura" id="altura" type="text" placeholder="Digite su altura" class="form-control" required="">
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="control-group">
                        <div class="controls">
                            <br>
                            <button id="calcular" name="calcular" onclick="calculateImc()" class="btn btn-primary">Calcular</button>
                        </div>
                    </div>

                </fieldset>
         

            <!-- Text input-->
            <div class="control-group pull-right">
                <h2 id="resultado" class="control-label" ></h2>
            </div>


        </div>
    </div>

    <table width="100%" height="239" border="2" align="center" cellpadding="0" cellspacing="0" bordercolor="#77b538">
        <tbody>
            <tr bgcolor="#FFFFFF">
                <td width="22%" height="32" align="center" valign="middle" bgcolor="#77b538"><strong><span style="color: #ffffff;">Índice de Masa Corporal (IMC)</span></strong></td>
                <td width="78%" valign="middle" bgcolor="#77b538"><strong><span style="color: #ffffff;">&nbsp;Clasificación</span></strong></td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">Menor a  18</td>
                <td valign="top">&nbsp;Peso bajo. Necesario valorar signos de desnutrición</td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">18 a 24.9</td>
                <td valign="top">&nbsp;Normal</td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">25 a 26.9</td>
                <td valign="top"><strong>&nbsp;Sobrepeso</strong></td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">Mayor a 27</td>
                <td valign="top"><strong>&nbsp;Obesidad</strong></td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">27 a 29.9</td>
                <td valign="top"><strong>&nbsp;Obesidad grado I.</strong> Riesgo relativo <strong>alto</strong> para desarrollar enfermedades cardiovasculares</td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">30 a 39.9</td>
                <td valign="top"><strong>&nbsp;Obesidad grado II.</strong> Riesgo relativo <strong>muy alto</strong> para el desarrollo de enfermedades cardiovasculares</td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td align="center" bgcolor="#FFFFFF">Mayor a 40</td>
                <td valign="top"><strong>&nbsp;Obesidad grado III Extrema o Mórbida.</strong> Riesgo relativo <strong>extremadamente alto</strong> para el desarrollo de enfermedades cardiovasculares</td>
            </tr>
        </tbody>
    </table> 

</div>

<jsp:include page="footerCliente.jsp"/>

<script>
    function calculateImc(){
        var peso = $('#peso').val();
        var altura = $('#altura').val();
        
        var resultado = 0;
        
        resultado = (peso/Math.pow(altura,2))*10000;
        
        $('#resultado').html(resultado);
        
        return false;
    }
</script>


<%
    } else {
        response.sendRedirect("../../index.jsp");
    }
%>