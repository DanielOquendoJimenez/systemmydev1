
<%
HttpSession ht = request.getSession();
if (ht.getAttribute("nombre") != null) {
%><%-- 
Document   : inicio
Created on : 14-oct-2015, 9:16:47
Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
		<li data-target="#myCarousel" data-slide-to="3"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
		<img src="../images/galería/1 (12).jpg" alt="Chania">
		</div>

		<div class="item">
			<img src="../images/galería/1 (12).jpg" alt="Chania">
		</div>

		<div class="item">
			<img src="../images/galería/1 (12).jpg" alt="Flower">
		</div>

		<div class="item">
			<img src="../images/galería/1 (12).jpg" alt="Flower">
		</div>
	</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>

<jsp:include page="footer.jsp"/>
<%    } else {
response.sendRedirect("../index.jsp");
}
%>
