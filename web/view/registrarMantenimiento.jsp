<%-- 
    Document   : registrarEncuesta
    Created on : 8/02/2016, 06:22:33 PM
    Author     : YEISSON
--%>



<%@page import="controller.ctrHerramientas"%>
<%@page import="controller.ctrPersona"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrMantenimiento"%>


<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>


<%-- 
  REGISTRAR ENCUESTA
--%>
<section>
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Seccion 1">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon glyphicon-heart-empty"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Seccion 2">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completo">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" role="tabpanel" id="step1">
                <!--registrar Valoracion Inicial-->
                <div class="pmb-block">
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Fecha Mantenimiento</dt>
                                <dd>
                                    <div class="dtp-container dropdown fg-line">
                                        <input type='text' class="form-control date-picker" data-toggle="dropdown" id="txtFecha" name="txtFecha" placeholder="Fecha"><span  id="mensaje1" hidden>Complete este campo</span>
                                    </div>
                                </dd>
                            </dl>

                            <dl class="dl-horizontal">
                                <dt>Descripción</dt>
                                <dd>
                                    <textarea  class="form-control" type="text" class="form-control input-mask"  id="txt_descripcion" name="txt_descripcion"></textarea><span  id="mensaje2" hidden>Complete este campo</span>
                                </dd>
                            </dl>




                        </div>
                    </div>
                </div>
                <!-- cerra registrar Valoracion Inicial-->
                <ul class="list-inline pull-right">
                    <li><button type="button" class="btn btn-primary next-step" id="siguiente">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
                </ul>
            </div>
            <div class="tab-pane" role="tabpanel" id="step2">
                <!--registrar Valoracion Inicial-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">


                            <dl class="dl-horizontal">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-4">
                                            Descripcion del mantenimiento:
                                        </div>
                                        <div class="col-md-4">
                                            <dt  id="txt_descripcionDe"></dt> 
                                        </div>
                                    </div>
                                </div>
                            </dl>


                            <dl class="dl-horizontal">

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <dt>Técnico</dt>
                                        </div>
                                        <div class="col-md-4">
                                            <select  class="selectpicker" data-live-search="true" name="tecnico"  id="tecnico">
                                                <option></option>
                                                <%
                                                    ctrPersona per1 = new ctrPersona();
                                                    ResultSet res1 = per1.consultarP();
                                                    while (res1.next()) {
                                                        if (res1.getString("idTipo_persona").equals("5")) {

                                                %>
                                                <option value="<%=res1.getString("idpersona")%>"><%=res1.getString("primer_nombre")%>  <%=res1.getString("primer_apellido")%></option>
                                                <%}
                                            }%>
                                            </select><span  id="mensaje4" hidden>Complete este campo</span>
                                        </div>
                                    </div>
                                </div>

                            </dl>


                            <h2 class="text-center">HERRAMIENTAS</h2>



                            <div class="t-body tb-padding">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Pregunta</label>
                                        <select type="text" class="selectpicker" data-live-search="true" name="slcHerramienta" id="slcHerramienta">
                                            <option></option> 
                                            <%
                                                ctrHerramientas lis = new ctrHerramientas();
                                                ResultSet resI = lis.listarHerramientas();
                                                while (resI.next()) {
                                            %>
                                            <option value="<%=resI.getString("idherramienta")%>"><%=resI.getString("nombre")%></option> 
                                            <%}%>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-sm-4"> 
                                    <input type="text" id="idMantenimiento" value="" >


                                    <br>
                                    <button  class="btn btn-primary btn-sm m-t-5" id="agregarValor">Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" id="tablaMantenimiento"></div>
                    </div>
                </div>                   
                <!-- cerra registrar -->
                <ul class="list-inline pull-right">
                    <li><button type="button" class="btn btn-default prev-step"><i class="zmdi zmdi-arrow-left m-r-5"></i> Atras</button></li>
                    <li><button type="button" class="btn btn-primary next-step">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
                </ul>
            </div>
            <div class="tab-pane" role="tabpanel" id="complete">
                <h3 class="text-center">Completo</h3>
                <p class="text-center">Mantenimiento Registrado Corectamente</p>
                <div class="text-center">
                    <a href="registrarMantenimiento.jsp" class="btn btn-default">Ok</a>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<%-- 
  FIN________FREGISTRAR ENCUESTA
--%>



<jsp:include page="footer.jsp"/>
<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {
            var fechaV = $("#txtFecha").val();
            var des = $("#txt_descripcion").val();
            if (fechaV == "" || des == "") {
                if (fechaV == "") {
                    $("#mensaje1").show();
                }

                if (des == "") {
                    $("#mensaje2").show();
                }


            } else {
                $.ajax({type: 'POST',
                    dataType: 'text',
                    data: {fechaV: fechaV, des: des}, url: "../ctrMantenimiento"
                }).done(function (data) {
                    if (data !== null) {
                        $("#idMantenimiento").val(data);
                        $("#txt_descripcionDe").html(des);
                        $("#siguiente").attr("disabled", true)
                    }
                }).fail(function () {

                });
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);

                return false;
            }
        });
        $(".prev-step").click(function (e) {
            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }


    $("#agregarValor").click(function () {


        var idHerramienta = $("#slcHerramienta").val();
        var idMantenimiento = $("#idMantenimiento").val();
        var idPersona = $("#tecnico").val()

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {idHerramienta: idHerramienta, idMantenimiento: idMantenimiento, idPersona: idPersona},
            url: "../ctrMantenimiento"
        }).done(function (data) {
            $("#tablaMantenimiento").html(data);
        }).fail(function () {

        });
        return false;
    });

    $("#txtFecha").keyup(function () {
        $("#mensaje").hide();
        $("#mensaje2").hide();
        $("#mensaje3").hide();
        $("#mensaje4").hide();
    });


</script>
<script>
    function eliminarItem(id) {
        var idDetalle = id;
        var idPersona = $("#tecnico").val();
        var idMantenimiento = $("#idMantenimiento").val();

        if (idDetalle !== null) {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idDetalle: idDetalle, idPersona: idPersona, idMantenimiento: idMantenimiento},
                url: "../ctrMantenimiento"
            }).done(function (tabla) {
                if (tabla !== null) {
                    $("#tablaMantenimiento").html(tabla);
                }
            }).fail(function () {

            });
            return false;
        }
    }
</script>
<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>