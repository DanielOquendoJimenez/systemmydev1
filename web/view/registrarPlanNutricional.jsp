<%-- 
    Document   : registrarPlanNutricional
    Created on : 10-mar-2016, 17:48:22
    Author     : daniel
--%>

<%@page import="controller.ctrPlanNutricional"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container">
    <div class="row">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-folder-open"></i>
                                </span>
                            </a>
                        </li>

                        <li role="presentation" class="disabled">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>

                <form role="form">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <div class="pmb-block">
                                <div class="pmbb-body p-l-30">
                                    <div class="pmbb-view">
                                        <dl class="dl-horizontal">
                                            <dt>Persona</dt>
                                            <dd>
                                                <select  class="selectpicker" data-live-search="true" name="slcPersonaP" id="slcPersonaP">
                                                    <option></option>
                                                    <%
                                                        ctrPersona per = new ctrPersona();
                                                        ResultSet res = per.consultarP();
                                                        while (res.next()) {
                                                            if (res.getString("idTipo_persona").equals("3")) {

                                                    %>
                                                    <option value="<%=res.getString("idpersona")%>"><%=res.getString("primer_nombre")%>  <%=res.getString("primer_apellido")%></option>
                                                    <%}
                                                        }%>
                                                </select><span  id="mensaje2" hidden>Complete este campo</span>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Fecha del Plan Nutricional</dt>
                                            <dd>
                                                <div class="dtp-container dropdown fg-line">
                                                    <input type='text' class="form-control date-picker" data-toggle="dropdown" id="txtFechaPlanN" name="txtFechaPlanN" placeholder="Fecha" disabled=""><span  id="mensaje" hidden>Complete este campo</span>
                                                </div>
                                            </dd>
                                        </dl>
                                        <div id="valoracion"></div>
                                    </div>
                                </div>
                            </div>



                            <!--<div class="pmb-block">
                                <div class="pmbb-body p-l-30">
                                    <input type='text' id="txtIdvaloracion" name="txtIdvaloracion" hidden="" value="">
                                    <div class="pmbb-view">
                                        <dl class="dl-horizontal">
                                            <dt>Nombre</dt>
                                            <dd>Daniel</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Apellido</dt>
                                            <dd>Oquendo</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Objetivo</dt>
                                            <dd>bajar de peso</dd>
                                        </dl>
                                        <div class="table-responsive m-t-20">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Item</th>
                                                        <th>Resultado</th>
                                                        <th>Accion</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Alexandra</td>
                                                        <td>Christopher</td>
                                                        <td><button type="button" class="btn btn-primary next-step"><i class="zmdi zmdi-arrow-right m-r-5"></i></button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>-->



                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-primary" id="btnSiguiente">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
                            </ul>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <h3 class="text-center" id="nombrePersonaP"></h3>





                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="text-center" id="nombrePersona"></h2> 
                                        <div class="t-body tb-padding">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Horario</label>
                                                    <select type="text" class="selectpicker" data-live-search="true" id="slcHorario">
                                                        <%
                                                            ctrPlanNutricional horario = new ctrPlanNutricional();
                                                            ResultSet resHorario = horario.listarHorarioDia();
                                                            while (resHorario.next()) {
                                                        %>
                                                        <option value="<%=resHorario.getString("idhorarioxdia")%>"><%=resHorario.getString("nombre")%></option>
                                                        <%}%>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Alimento</label>
                                                    <select type="text" class="selectpicker" data-live-search="true" id="slcIAlimento">
                                                        <%
                                                            ResultSet resAlimento = horario.listarAlimento();
                                                            while (resAlimento.next()) {
                                                        %>
                                                        <option value="<%=resAlimento.getString("idalimento")%>"><%=resAlimento.getString("nombre")%></option>
                                                        <%}%>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Porcion</label>
                                                    <input type="text" id="idPlanNutricional" value="" hidden="">
                                                    <input type="text" class="form-control" id="txtPorcion" placeholder="Resultado" data-mask="000">
                                                </div>
                                            </div>
                                            <div class="col-sm-3"> 
                                                <br>
                                                <button  class="btn btn-primary btn-sm m-t-5" id="agregarPlanNutricional">Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12" id="tablaPlanNutricional"></div>
                                </div>
                            </div>  






                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-default prev-step"><i class="zmdi zmdi-arrow-left m-r-5"></i> Atras</button></li>
                                <li><button type="button" class="btn btn-primary next-step">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
                            </ul>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="complete">
                            <h3 class="text-center">Completo</h3>
                            <p class="text-center">Valoracion Registrada Corectamente</p>
                            <div class="text-center">
                                <a  href="registrarPlanNutricional.jsp" class="btn btn-default" id="btnOk" name="btnOk">Ok</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<jsp:include page="footer.jsp"/>
<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $("#btnSiguiente").click(function (e) {
            var idValoracion = $("#txtIdvaloracion").val();
            var fechaPlanNutricional = $("#txtFechaPlanN").val();
            var Nombre = $("#slcPersonaP option:selected").html();
            if (idValoracion !== "" && fechaPlanNutricional !== "") {

                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {idValoracion: idValoracion, fechaPlanNutricional: fechaPlanNutricional},
                    url: "../ctrPlanNutricional"
                }).done(function (data) {
                    if (data != null) {
                        $("#idPlanNutricional").val(data);
                        $("#nombrePersonaP").html(Nombre);
                    }
                }).fail(function () {

                });

                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            } else {
                $("#mensaje").show();
            }

        });
        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $("#slcPersonaP").change(function () {
        var idPersona = $("#slcPersonaP").val();
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {idPersona: idPersona},
            url: "../ctrPlanNutricional"
        }).done(function (data) {
            $("#valoracion").html(data);
        }).fail(function () {

        });
    });

    $("#agregarPlanNutricional").click(function () {
        var idPlanNutricional = $("#idPlanNutricional").val();
        var idHorario = $("#slcHorario").val();
        var idAlimento = $("#slcIAlimento").val();
        var porcion = $("#txtPorcion").val();

        if (idPlanNutricional !== "") {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idPlanNutricional: idPlanNutricional, idHorario: idHorario, idAlimento: idAlimento, porcion: porcion},
                url: "../ctrPlanNutricional"
            }).done(function (data) {
                if (data !== null) {
                    $("#tablaPlanNutricional").html(data);
                }
            }).fail(function () {

            });
        }
        return false;
    });

</script>
<script>
    function seleccionar(idValoracion) {
        var idvaliracion = idValoracion;
        if (idvaliracion != "") {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idvaliracion: idvaliracion},
                url: "../ctrPlanNutricional"
            }).done(function (data) {
                if (data != null) {
                    $("#valoracion").html(data);
                    $("#txtFechaPlanN").attr("disabled", false);
                }
            }).fail(function () {

            });
        }
        return false;
    }
</script>
<script>
    function eliminarPlanN(idPlanN) {
        var idDetallePLanNutricional = idPlanN;
        var idPlanNutricional = $("#idPlanNutricional").val();

        if (idDetallePLanNutricional !== "" && idPlanNutricional !== "") {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idDetallePLanNutricional: idDetallePLanNutricional, idPlanNutricional: idPlanNutricional},
                url: "../ctrPlanNutricional"
            }).done(function (data) {
                if (data != null) {
                    $("#tablaPlanNutricional").html(data);
                }
            }).fail(function () {

            });

        }
    }
</script>
<script>
    $("#txtFechaPlanN").blur(function () {
        var fecha = $("#txtFechaPlanN").val();
        var fechaActual = new Date();
        var dd = fechaActual.getDate();
        var mm = fechaActual.getMonth() + 1;
        var yy = fechaActual.getFullYear();
        var fechaActual = dd + "/" + mm + "/" + yy;

        FechaNac = fecha.split("/");
        var diaC = FechaNac[0];
        var mmC = FechaNac[1];
        var yyyyC = FechaNac[2];
//retiramos el primer cero de la izquierda
        if (mmC.substr(0, 1) == 0) {
            mmC = mmC.substring(1, 2);
        }
//retiramos el primer cero de la izquierda
        if (diaC.substr(0, 1) == 0) {
            diaC = diaC.substring(1, 2);
        }

        fecha = diaC + "/" + mmC + "/" + yyyyC;

        if (fecha == fechaActual) {
            $('#mensaje').hide();
            $('#btnSiguiente').attr("disabled", false);
            $("#txtFechaPlanN").css({
                border: "1px green solid"
            });
        } else {
            $('#btnSiguiente').attr("disabled", true);
            $("#txtFechaPlanN").css({
                border: "1px red solid"
            });
        }

    });

</script>
<!--<table>
    <thead id="myTable" class="table table-bordered table-vmiddle">
        <tr>
            <th>Fecha de la Valoracion</th>
            <th>Objetivo</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td><button type="button" class="btn btn-primary" onclick="">Seleccionar</button></td>
        </tr>
    </tbody>
</table>-->


