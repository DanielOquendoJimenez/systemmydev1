<%-- 
Document   : preguntas
Created on : 05-ene-2016, 12:49:20
Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar pregunta</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-wrench m-r-5"></i> PREGUNTAS</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrPregunta">
                                <dl class="dl-horizontal">
                                    <dt>Nombre de la pregunta</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_Pregunta" name="txt_nombrePregunta" />
                                    </dd>
                                </dl>

                                <div class="row">
                                    <div class="col-md-4">
                                       <div class="m-t-30">
                                           <a href="consultarPreguntas.jsp">
                                           <button type="button" class="btn btn-info">Consultar Preguntas</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>                                
                                <div class="col-md-4">
                                    <div class="m-t-30" id="ver1">
                                        <input type="submit" class="btn btn-success" value="Guardar" id="enviar-btn" name="guardar" />
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> 

<jsp:include page="footer.jsp"/>