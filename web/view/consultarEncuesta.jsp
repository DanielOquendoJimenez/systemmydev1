<%-- 
Document   : consultarHerramientas
Created on : 13/02/2016, 11:53:46 AM
Author     : YEISSON
--%>

<%@page import="model.mdlExtras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.mdlEncuesta"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrEncuesta"%>

<jsp:include page="header.jsp"/>
<%-- 
MODIFICAR UNA HERRAMIENTA
--%>
<%
    String modificar = request.getParameter("accion");
    if (modificar != null) {
        mdlEncuesta mod = new mdlEncuesta();
        mod.setIdencuesta(Integer.parseInt(modificar));
        ResultSet res = mod.consultarDetalleEncuestaPersona();
        if (res.next()) {
%>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Modificar Herramienta <div class="pull-right"><a href="consultarHerramientas.jsp" style="color: black">X</a></div></div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i> Herramienta</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrHerramientas">
                                <dl class="dl-horizontal">
                                    <dt>Nombre de la herramienta</dt>
                                    <dd>
                                        <input type="hidden" hidden class="form-control input-mask" id="txt_idHerramienta" name="txt_idHerramienta" value="<%=res.getString("idEncuesta")%>" />
                                        <input type="text" class="form-control input-mask" id="txt_nombre" name="txt_nombre" value="<%=res.getString("fechaEncuesta")%>" />
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Código</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_codigo" name="txt_codigo" value="<%=res.getString("pronostico")%>"/>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descripción</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_descripcion" name="txt_descripcion" value="<%=res.getString("primerNombre")%>"/>
                                    </dd>
                                </dl>

                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="registrarEncuesta.jsp">
                                            <button type="button" class="btn btn-success">Registrar Encuesta</button>
                                        </a>
                                    </div>
                                    <div class="col-md-5"></div>
                                    <div class="col-md-3">
                                        <div class="m-t-30" id="ver1">
                                            <input type="submit" class="btn btn-success" value="Modificar" id="enviar-btn" name="btnModificar" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<%}%>
<%}%>
<%-- 
FIN_________MODIFICAR UNA HERRAMIENTA
--%>
<!--
LISTO EN UNA TABLA TODAS LAS HERRAMIENTAS
-->
<h3 class="text-center">Encuesta de la Persona <small></small></h3>
<table id="myTable" class="table table-bordered table-vmiddle">
    <thead>
        <tr>
            <th>Id encuesta</th>
            <th>Nombre</th>            
            <th>Fecha encuesta</th>
            <th>Descripciòn</th>   
            <th>Acciòn</th>   
        </tr>
    </thead>
    <tbody>
        <%
            ctrEncuesta ch = new ctrEncuesta();
            ResultSet her = ch.consultarDetalleEncuestaPersona();
            while (her.next()) {
                mdlExtras fecha = new mdlExtras();
                String fechaE = her.getString("fechaEncuesta");
                fecha.setFecha(fechaE);
                fechaE = fecha.revertirFecha();
        %>
        <tr>
            <td><%=her.getString("idEncuesta")%></td>
            <td><%=her.getString("primerNombre")%></td>
            <td><%=fechaE%></td>
            <td><%=her.getString("pronostico")%></td>
            <td>          
                <button class="btn btn-success" data-toggle="modal" data-target="#<%=her.getString("idEncuesta")%>" ><i class="zmdi zmdi-collection-text"></i></button>
            </td> 
        </tr>
        <!-- Modal -->
    <div class="modal fade" id="<%=her.getString("idEncuesta")%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">  <%=her.getString("primerNombre")%></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <p class="c-black f-500"> Fecha encuesta:</p>
                            </div>
                            <div class="col-md-3">
                                <%

                                %>
                                <%=fechaE%>                          
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <p class="c-black f-500"> Descripcion:</p>
                            </div>
                            <div class="col-md-3">
                                <%=her.getString("pronostico")%>
                            </div>
                        </div>
                    </div>  
                    <h4>
                        <div class="container-fluid text-left">
                            <div class="row">
                                <div class="col-md-2">  
                                </div>
                                <div class="col-md-7">  
                                    Pregunta 
                                </div>                             
                                <div class="col-md-3">  
                                    Respuesta 
                                </div> 
                            </div>     
                        </div>
                    </h4>
                    <ul>
                        <%
                            mdlEncuesta mod1 = new mdlEncuesta();
                            mod1.setIdencuesta(Integer.parseInt(her.getString("idEncuesta")));
                            ResultSet her1 = mod1.consultarDetalleEncuestaPersonaId();
                            while (her1.next()) {
                        %> 
                        <h5>
                            <div class="container-fluid text-left">
                                <div class="row">
                                    <div class="col-md-10">  
                                        <li>
                                            <%=her1.getString("pregunta")%> : 
                                    </div>
                                    <%
                                        if (her1.getString("respuesta").equals("1")) {
                                    %>
                                    si <br>
                                    <%
                                    } else {
                                    %>
                                    no <br>
                                    <%} %>
                                    <%}%>
                                </div>
                            </div>
                        </h5>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <%}%>
</tbody>
</table>

<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-3">
        <a href="registrarEncuesta.jsp">
            <button type="button" class="btn btn-success">Registrar Encuesta</button>
        </a>
    </div>
    <div class="col-md-3"></div>
</div>
<!--
FIN DEL LISTADO DE LAS HERRAMIENTAS
-->
<jsp:include page="footer.jsp"/>