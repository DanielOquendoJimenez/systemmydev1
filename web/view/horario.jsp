
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>

<%-- 
    Document   : Horario
    Created on : 24/02/2016, 07:10:06 PM
    Author     : Maychan
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="javax.swing.DefaultComboBoxModel"%>
<%@page import="controller.ctrCronograma"%>
<%@page import="model.mdlCronograma"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="header.jsp"/>

<!-- registrar horario-->

<div class="container c-boxed">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <header class="page-header">
            <h3>Objetivo<small>Este Objetivo se creo para complementar las clases del cronograma podrá sufrir cambios y modificaciones de cada semana, por eso el Gimnasio Ser Athletic recomienda consultar constantemente su versión en línea, la cual se encuentra en el siguiente enlace: xxxxxxxxxxxxxxxxxxx.</small></h3>
        </header>

        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center"><h3>Crear Horario</h3></div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h3 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i>Horario</h3>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">                            
                            <form  role="form"  method="POST" action="../ctrCronograma">
                                <dl class="dl-horizontal">
                                    <dt>Hora inicio (*)</dt>
                                    <dd>
                                    <input type="text" class="input-sm form-control" id="txt_horai" name="txt_horai" ><span id="mensaje" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Hora final (*)</dt>
                                    <dd>
                                    <input type="text" class="input-sm form-control" id="txt_horaf" name="txt_horaf" ><span id="mensaje1" hidden>Complete este campo</span>
                                    </dd>
                                </dl>                               
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary" value="Crear Horario" id="txt_guardarhora" name="txt_guardarhora">
                                    <input type="button" id="btn_verHorario" class="btn btn-success" name="btn_verHorario" value="Consultar Horario">
                                </div>
                            </form>   
                        </div>
                    </div> 
                </div>  
            </div>
            <div id="tabla"></div>
        </div>
        <!-- cierrra registrar horario-->

        <!-- modal modiifcar horario-->
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Modificar Horario</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="POST" action="../ctrCronograma">
                            <dl class="dl-horizontal">
                                <dt><center>Horario</center></dt>
                                <dd>
                                    <input type="hidden" name="txtidh" id="txtidh" class="input-sm form-control" >  
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Hora inicio (*)</dt>
                                <dd>
                                <input type="text" name="txthi" id="txthi" class="input-sm form-control" ><span id="mensaje2" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Hora final  (*)</dt>
                                <dd>
                                <input type="text" id="txthf" name="txthf" class="input-sm form-control" ><span id="mensaje3" hidden>Complete este campo</span>
                                </dd>
                            </dl>                               
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="txt_modif" name="txt_modif">modificar horario</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<!-- cierra modal modiifcar horario-->

<jsp:include page="footer.jsp"/>


<script>
    $("#txt_horai").timepicki();
    $("#txt_horaf").timepicki();
</script>

<script>
    $("#txthi").timepicki();
    $("#txthf").timepicki();
</script>


<!-- editar horario-->
<script>
    function editarHorario(id, hin, hfn) {
        $(document).on("click", ".open-Modal", function () {
        });
        $("#txtidh").val(id);
        $("#txthi").val(hin);
        $("#txthf").val(hfn);
    }
</script>
<!-- cierra editar horario-->

<!-- ver horario-->
<script>
    $("#btn_verHorario").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {listare: "si"},
            url: "../ctrCronograma"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    });
</script>
<!-- cierra ver horario-->

<!-- estado horario-->
<script>
    function estadoHorario(estado, idhorario) {

        swal("Cambio de estado", "Exitoso", "success");

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {estado: estado, idhorario: idhorario},
            url: "../ctrCronograma"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("Malo");
        });
        return false;
    }
</script>
<!-- cierra estado horario-->
<!-- validacion horario-->
<script>
    $(document).ready(function () {
        $("#txt_guardarhora").click(function () {
            var txt_horai = $("#txt_horai").val();
            var txt_horaf = $("#txt_horaf").val();

            if (txt_horai == "" || txt_horaf == "") {
                if (txt_horai == "") {
                    $("#mensaje").show();
                } else {
                    $("#mensaje").hide();
                }
                if (txt_horaf == "") {
                    $("#mensaje1").show();
                } else {
                    $("#mensaje1").hide();
                }
                return false;
            }
        });

        $("#txt_modif").click(function () {
            var txthi = $("#txthi").val();
            var txthf = $("#txthf").val();

            if (txthi == "" || txthf == "") {
                if (txthi == "") {
                    $("#mensaje2").show();
                } else {
                    $("#mensaje2").hide();
                }
                if (txthf == "") {
                    $("#mensaje3").show();

                } else {
                    $("#mensaje3").hide();
                }
                return false;
            }
        });
    }
    );
</script>

<%
    } else {
        response.sendRedirect("../index.jsp");
    }
%>
