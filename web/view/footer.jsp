

</section>

<footer id="footer">
    Copyright � <a id="fechaActual" style="color: #A2A2A2"></a> System MYDE

    <ul class="f-menu">
        <li><a href="#">Inicio</a></li>
        <li><a href="#">Actualizar Datos</a></li>
        <li><a href="#">Plan De Entrenamiento</a></li>
        <li><a href="#">Plan Nutricional</a></li>
        <li><a href="#">Valoraci�n</a></li>
    </ul>
</footer>









<!-- Older IE Warning Message -->
<!--[if lt IE 9]>
    <div class="ie-warning">
        <h1 class="c-white">Advertencia!!</h1>
        <p>Est� utilizando una versi�n antigua de Internet Explorer, por favor, actualice <br/>a cualquiera de los siguientes navegadores de Internet para acceder a este sitio web.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="img/browsers/chrome.png" alt="">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="img/browsers/firefox.png" alt="">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="img/browsers/opera.png" alt="">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/"> 
                        <img src="img/browsers/safari.png" alt="">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="img/browsers/ie.png" alt="">
                        <div>IE (New)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>�Lo siento por los inconvenientes ocasionados!</p>
    </div>   
    <![endif]-->

<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
<script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script src="vendors/bower_components/bootstrap-select/dist/js/defaults-es_ES.min.js" type="text/javascript"></script>
<script src="vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
<script src="vendors/bower_components/summernote/dist/summernote.min.js"></script>
<script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/data.js" type="text/javascript"></script>
<script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.js"></script>
<script src="vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/data.js" type="text/javascript"></script>
<script src="vendors/bower_components/jquery.bootgrid/dist/jquery.dataTables.js" type="text/javascript"></script>
<script src="vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
<script src="vendors/fileinput/fileinput.min.js"></script>
<script src="vendors/input-mask/input-mask.min.js"></script>
<script src="vendors/farbtastic/farbtastic.min.js"></script>

<script src="vendors/bower_components/jquery.bootgrid/dist/jquery.bootgrid-override.min.js"></script>

<!-- Data Table -->
<script src="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>

<script src="vendors/bower_components/fullcalendar/dist/fullcalendar.js"></script>
<script src="vendors/bower_components/fullcalendar/dist/es.js" type="text/javascript"></script>
<!-- LightGallery + Plugins -->
<script src="vendors/bower_components/lightgallery/src/js/lightgallery.js"></script>
<script src="vendors/bower_components/lightgallery/src/js/lg-video.js"></script>
<script src="vendors/bower_components/lightgallery/src/js/lg-fullscreen.js"></script>
<script src="vendors/bower_components/lightgallery/src/js/lg-zoom.js"></script>

<script src="js/parsley.min.js" type="text/javascript"></script>
<script src="js/es.js" type="text/javascript"></script>
<!--<script src="vendors/bower_components/flot/jquery.flot.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.pie.js"></script>
<script src="vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
<script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>                         
<script src="vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
<script src="vendors/sparklines/jquery.sparkline.min.js"></script>
<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>-->
<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
    <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
    <![endif]-->

<!--<script src="js/flot-charts/curved-line-chart.js"></script>
<script src="js/flot-charts/line-chart.js"></script>
<script src="js/flot-charts/bar-chart.js"></script>
<script src="js/flot-charts/dynamic-chart.js"></script>
<script src="js/flot-charts/pie-chart.js"></script>-->

<script src="js/functions.js"></script>
<script src="js/demo.js"></script>
<!--<script src="js/charts.js"></script>-->
<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //Basic Example
        $("#data-table").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less',
                iconSearch: 'zmdi-search'
            },
        });

        //Selection
        $("#data-table-selection").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less',
                iconSearch: 'zmdi-search'

            },
            selection: true,
            multiSelect: true,
            rowSelect: true,
            keepSelection: true
        });

        //Command Buttons
        $("#data-table-command").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {
                    return "<button type=\"button\" class=\"btn btn-icon btn-info command-edit\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> "; //+
                    // "<button type=\"button\" class=\"btn btn-icon btn-info command-delete\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                }
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var cId = $('#calendar'); //Change the name if you want. I'm also using thsi add button for more actions

        //Generate the Calendar
        cId.fullCalendar({
            header: {
                right: '',
                center: 'prev, title, next',
                left: ''
            },
            theme: true, //Do not remove this as it ruin the design
            selectable: true,
            selectHelper: true,
            editable: true,
            //Add Events
            events: [
                {
                    title: 'All Day Event',
                    start: '2016-01-01'
                },
                {
                    title: 'Long Event',
                    start: '2016-01-07',
                    end: '2016-01-10'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-01-09T16:00:00'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-01-16T16:00:00'
                },
                {
                    title: 'Conference',
                    start: '2016-01-11',
                    end: '2016-01-13'
                },
                {
                    title: 'Meeting',
                    start: '2016-01-12T10:30:00',
                    end: '2016-01-12T12:30:00'
                },
                {
                    title: 'Lunch',
                    start: '2016-01-12T12:00:00'
                },
                {
                    title: 'Meeting',
                    start: '2016-01-12T14:30:00'
                },
                {
                    title: 'Happy Hour',
                    start: '2016-01-12T17:30:00'
                },
                {
                    title: 'Dinner',
                    start: '2016-01-12T20:00:00'
                },
                {
                    title: 'Birthday Party',
                    start: '2016-01-13T07:00:00'
                },
                {
                    title: 'Click for Google',
                    url: 'http://google.com/',
                    start: '2016-01-28'
                },
            ],
            //On Day Select
            select: function (start, end) {
                $('#addNew-event').modal('show');
                $('#addNew-event input:text').val('');
                $('#getStart').val(start);
                $('#getEnd').val(end);
            }

        });

        //Create and ddd Action button with dropdown in Calendar header. 
        var actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
                '<li class="dropdown">' +
                '<a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>' +
                '<ul class="dropdown-menu">' +
                '<li class="active">' +
                '<a data-view="month" href="">Month View</a>' +
                '</li>' +
                '<li>' +
                '<a data-view="basicWeek" href="">Week View</a>' +
                '</li>' +
                '<li>' +
                '<a data-view="agendaWeek" href="">Agenda Week View</a>' +
                '</li>' +
                '<li>' +
                '<a data-view="basicDay" href="">Day View</a>' +
                '</li>' +
                '<li>' +
                '<a data-view="agendaDay" href="">Agenda Day View</a>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</li>';


        cId.find('.fc-toolbar').append(actionMenu);

        //Event Tag Selector
        (function () {
            $('body').on('click', '.event-tag > span', function () {
                $('.event-tag > span').removeClass('selected');
                $(this).addClass('selected');
            });
        })();

        //Add new Event
        $('body').on('click', '#addEvent', function () {
            var eventName = $('#eventName').val();
            var tagColor = $('.event-tag > span.selected').attr('data-tag');

            if (eventName != '') {
                //Render Event
                $('#calendar').fullCalendar('renderEvent', {
                    title: eventName,
                    start: $('#getStart').val(),
                    end: $('#getEnd').val(),
                    className: tagColor

                }, true); //Stick the event

                $('#addNew-event form')[0].reset()
                $('#addNew-event').modal('hide');
            }

            else {
                $('#eventName').closest('.form-group').addClass('has-error');
            }
        });

        //Calendar views
        $('body').on('click', '#fc-actions [data-view]', function (e) {
            e.preventDefault();
            var dataView = $(this).attr('data-view');

            $('#fc-actions li').removeClass('active');
            $(this).parent().addClass('active');
            cId.fullCalendar('changeView', dataView);
        });
    });
</script>
<script>
    var ano = (new Date).getFullYear();

    $(document).ready(function () {
        $("#fechaActual").text(ano);
    });
</script>


<!--<script src="vendors/bower_components/flot/jquery.flot.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="vendors/bower_components/flot/jquery.flot.pie.js"></script>
<script src="vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
<script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>                         
<script src="vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
<script src="vendors/sparklines/jquery.sparkline.min.js"></script>
<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>



<!-- Charts - Please read the read-me.txt inside the js folder-->
<!--<script src="js/flot-charts/curved-line-chart.js"></script>
<script src="js/flot-charts/line-chart.js"></script>
<script src="js/flot-charts/bar-chart.js"></script>
<script src="js/flot-charts/dynamic-chart.js"></script>
<script src="js/flot-charts/pie-chart.js"></script>


<script src="js/charts.js"></script>-->



</body>
</html>
