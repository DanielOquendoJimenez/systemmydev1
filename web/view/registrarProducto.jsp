<%-- 
Document   : registrarProducto
Created on : 13/02/2016, 10:00:45 AM
Author     : YEISSON
--%>
<%@page import="controller.ctrPlan"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="header.jsp"/>

<%-- REGISTRO DE UN producto--%>
<div class="container" id="form"> 
    <form action="../ctrProducto" method="post" enctype="multipart/form-data" data-validate="parsley">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tile">
                <div class="t-header th-alt bg-blue">
                    <div class="th-title text-center">Registrar Producto</div>
                </div>
                <div class="t-body tb-padding">             
                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2 class="text-center"><i class="zmdi zmdi-assignment m-r-5"></i> Producto</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">

                                <center>
                                    <div class="panel panel-default" class="col-md-offset-4">
                                        <div class="panel-body">
                                            <div class="form-group" >
                                                <div  id="form1" runat="server">
                                                    <img id="image_upload_preview" height="150" width="150" src="img/producto.png" alt="your image"/>
                                                    <input  class="btn btn-file" type='file' id="inputFile" name="fleFoto"   />
                                                </div>
                                            </dd>
                                        </div>
                                    </div>
                                </div>
                            </center>

                            <dl class="dl-horizontal">
                                <dt>Nombre del Producto</dt>
                                <dd>
                                    <input type="text" class="form-control input-mask" id="txt_nombreProducto" name="txt_nombreProducto" /><span  id="mensaje1" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Valor del Producto</dt>
                                <dd>
                                    <input type="text" class="form-control input-mask" id="txt_valorProducto" name="txt_valorProducto"   /><span  id="mensaje2" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Descripción del Producto</dt>
                                <dd>
                                    <input type="text" class="form-control input-mask" id="txt_descripcion" name="txt_descripcion" />
                                    <span  id="mensaje3" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                        </div>
                        <div class="row">
                         <div class="col-md-3"> 
                            <div class="m-t-30">                                       
                                <a href="consultarProducto.jsp">
                                    <button type="button" class="btn btn-primary">Consultar Productos</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-5"></div>
                        <div class="col-md-4">
                            <div class="m-t-30" id="ver1">
                                <input type="submit" class="btn btn-success" value="Guardar" id="btnGuardar2" name="btnGuardar2" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
<div  id="tabla">
</div>
</div>
</form>
</div> 
<%-- FIN_________REGISTRO DE UN producto--%>

<jsp:include page="footer.jsp"/>

<%-- OBTENER IMAGEN PARA MOSTRARLA--%>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
</script>
<%-- FIN___OBTENER IMAGEN PARA MOSTRARLA--%>

<%-- VALIDAR CAMPOS VACIOS--%>
<script type="text/javascript">
    $(document).ready(function () {

        $("btn").click(function () {
            $("valNombre").toggle();
        });

        $("#btnGuardar2").click(function () {

            var nom = $("#txt_nombreProducto").val();
            var valor = $("#txt_valorProducto").val();
            var des = $("#txt_descripcion").val();
            if (nom == "" || valor == "" || des == "") {
                if (nom == "") {
                    $("#mensaje1").show();

                } else {
                    $("#mensaje1").hide();
                }
                if (valor == "") {
                    $("#mensaje2").show();
                } else {
                    $("#mensaje2").hide();
                }
                if (des == "") {
                    $("#mensaje3").show();
                } else {
                    $("#mensaje3").hide();
                }
                return false;
            }
        });
    });
</script>

<%-- FIN______VALIDAR CAMPOS VACIOS--%>
