<%-- 
    Document   : planEntrenamiento
    Created on : 12/11/2016, 11:44:39 AM
    Author     : daniel
--%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%@page import="controller.ctrCronograma"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<section>
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-folder-open"></i>
                        </span>
                    </a>
                </li>

                <li role="presentation" class="disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </span>
                    </a>
                </li>
                <!--<li role="presentation" class="disabled">
                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-picture"></i>
                        </span>
                    </a>
                </li>-->
                <li role="presentation" class="disabled">
                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>

        <form role="form">
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">


                    <div class="pmb-block">
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Fecha</dt>
                                    <dd>
                                        <div class="dtp-container dropdown fg-line">
                                            <input type='text' class="form-control date-picker" data-toggle="dropdown" id="txtFechaP" name="txtFechaP" placeholder="Fecha"><span  id="mensaje" hidden>Complete este campo</span>
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Persona</dt>
                                    <dd>
                                        <select  class="selectpicker" data-live-search="true" name="slcPersonaP" id="slcPersonaP">
                                            <option></option>
                                            <%

                                                ctrPersona persona = new ctrPersona();
                                                ResultSet personas = persona.consultarP();
                                                while (personas.next()) {

                                                    if (personas.getString("idTipo_persona").equals("3")) {
                                            %>
                                            <option value="<%=personas.getString("idpersona")%>"><%=personas.getString("primer_nombre")%>  <%=personas.getString("primer_apellido")%></option>
                                            <%}
                                                }%>
                                        </select><span  id="mensaje2" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Objetivo</dt>
                                    <dd>
                                        <textarea class="form-control" type="text" class="form-control input-mask"  id="txtObjetivoP" name="txtObjetivoP"></textarea><span  id="mensaje3" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>



                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-primary next-step-training">Siguiente</button></li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">


                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-center" id="nombrePersona"><i class="zmdi zmdi-assignment-o m-r-5"></i></h2> 
                                <div class="t-body tb-padding">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Item de Valoracion</label>
                                            <select type="text" class="selectpicker" data-live-search="true" id="slcClase">
                                                <option></option> 
                                                <%
                                                    ctrCronograma cronograma = new ctrCronograma();
                                                    ResultSet cronogramas = cronograma.consultarCronograma();
                                                    while (cronogramas.next()) {
                                                %>
                                                <option value="<%=cronogramas.getString("idcronograma")%>"><%=cronogramas.getString("nombre")%>/<%=cronogramas.getString("hora_inicio")%>/<%=cronogramas.getString("descripcion")%></option> 
                                                <%}%>
                                            </select><span  id="mensaje4" hidden>Complete este campo</span>
                                        </div>
                                    </div>                                 
                                    <div class="col-sm-4"> 
                                        <br>
                                        <button type="button" class="btn btn-primary btn-sm m-t-5" id="agregarClase">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12" id="tablaValoracion">
                                <div class="table-responsive">
                                    <table id="tab_logic" class="table table-bordered table-vmiddle">
                                        <thead>
                                            <tr>
                                                <td  data-column-id="Dia">Dia</td>
                                                <td  data-column-id="Horario">Horario</td>
                                                <td  data-column-id="Clase">Clase</td>
                                                <td  data-column-id="Accion" data-formatter="commands" data-sortable="false">Accion</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="addr1">
                                                <td></td>
                                                <td></td>
                                                <td></td> 
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  





                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Atras</button></li>
                        <li><button type="button" class="btn btn-primary next-step">Terminar</button></li>
                    </ul>
                </div>
                <!--<div class="tab-pane" role="tabpanel" id="step3">
                    <h3>Step 3</h3>
                    <p>This is step 3</p>
                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                        <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                        <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                    </ul>
                </div>-->
                <div class="tab-pane" role="tabpanel" id="complete">
                    <h3 class="text-center">Completo</h3>
                    <p class="text-center">Plan de entrenamiento Registrado Corectamente.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</section>
<jsp:include page="footer.jsp"/>

<script>
    var clases = [];

    $(document).ready(function () {

        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step-training").click(function (e) {
            if ($("#txtFechaP").val() === "" || $("#slcPersonaP").val() === "" || $("#txtObjetivoP").val() === "") {
                if ($("#txtFechaP").val() === "") {
                    $("#mensaje").show();
                }
                if ($("#slcPersonaP").val() === "") {
                    $("#mensaje2").show();
                }
                if ($("#txtObjetivoP").val() === "") {
                    $("#mensaje3").show();
                }

            } else {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            }
        });
        $(".next-step").click(function (e) {
            swal({
                title: "Esto no es Reversible",
                text: "Esta seguro de terminar el eso proceso",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Confirmar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        idPerson: $("#slcPersonaP").val(),
                        dateTraining: $("#txtFechaP").val(),
                        objetive: $("#txtObjetivoP").val(),
                        trainingClass: clases
                    },
                    url: "../ctrPlanEntrenamiento"
                }).done(function (data) {
                    console.log(data);
                    if (!data.error) {
                        setTimeout(function () {
                            swal("¡Buen trabajo!", "Guardado correctamente", "success");
                            var $active = $('.wizard .nav-tabs li.active');
                            $active.next().removeClass('disabled');
                            nextTab($active);
                        }, 2000);
                    } else {
                        swal("Cancelado", "cancelado por el usuario1", "error");
                    }
                }).fail(function (e) {
                    console.log(e);
                    swal("Cancelado", "cancelado por el usuario2", "error");
                });

            });
        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });

        var i = 1;

        $("#agregarClase").click(function () {

            if ($("#slcClase").val() !== "") {

                var claseC = clases.indexOf($("#slcClase").val());

                if (claseC >= 0) {
                    swal("Error", "Esta Clase ya se agrego", "error");
                } else {
                    clases.push($("#slcClase").val());

                    var clas = $("#slcClase option:selected").text();
                    var schedule = clas.split("/");
                    var idCron = $("#slcClase").val();
                    $('#addr' + i).html("<td>" + schedule[0] + "</td>\n\
                                <td>" + schedule[1] + "</td>\n\
                                <td>" + schedule[2] + "</td>\n\
                                <td><button type='button' onclick='eliminarClase(" + i + ", " + idCron + ")' class='btn btn-danger'><i class='zmdi zmdi-delete'></i></button></td>");

                    $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
                    i++;
                }
            } else {
                $("#mensaje4").show();
            }
        });
        $("#delete_row").click(function () {
            if (i > 1) {
                $("#addr" + (i - 1)).html('');
                i--;
            }
        });

        $("#txtFechaP").blur(function () {
            $("#mensaje").hide();
        });
        $("#slcPersonaP").change(function () {
            $("#mensaje2").hide();
        });
        $("#slcClase").change(function () {
            $("#mensaje4").hide();
        });
        $("#txtObjetivoP").keyup(function () {
            $("#mensaje3").hide();
        });

    });
    function eliminarClase(elem, conten) {
        var index = clases.indexOf(conten.toString());
        clases.splice(index, 1);
        $("#addr" + elem).html('');
    }

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $("#slcPersonaP").change(function () {
        var str = "";
        $("#slcPersonaP option:selected").each(function () {
            str += $(this).text() + " ";
        });
        $("#nombrePersona").text(str);
    }).change();

</script>
<%        } else {
        response.sendRedirect("../../index.jsp");
    }
%>