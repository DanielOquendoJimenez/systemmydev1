<%@page import="model.mdlExtras"%>
<%@page import="model.mdlPlanNutricional"%>
<%@page import="model.mdlValoracion"%>
<%@page import="model.mdlPlanEntrenamiento"%>
<%@page import="model.mdlPlan"%>
<%@page import="model.mdlPersona"%>
<%@page import="controller.ctrPlan"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page import="controller.ctrConsultaD"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : consultaDetalladaP
    Created on : 04-nov-2015, 12:37:15
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<style>
    @import url("http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css");

    .panel-pricing {
        -moz-transition: all .3s ease;
        -o-transition: all .3s ease;
        -webkit-transition: all .3s ease;
    }
    .panel-pricing:hover {
        box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.2);
    }
    .panel-pricing .panel-heading {
        padding: 20px 10px;
    }
    .panel-pricing .panel-heading .fa {
        margin-top: 10px;
        font-size: 58px;
    }
    .panel-pricing .list-group-item {
        color: #777777;
        border-bottom: 1px solid rgba(250, 250, 250, 0.5);
    }
    .panel-pricing .list-group-item:last-child {
        border-bottom-right-radius: 0px;
        border-bottom-left-radius: 0px;
    }
    .panel-pricing .list-group-item:first-child {
        border-top-right-radius: 0px;
        border-top-left-radius: 0px;
    }
    .panel-pricing .panel-body {
        background-color: #f0f0f0;
        font-size: 40px;
        color: #777777;
        padding: 20px;
        margin: 0px;
    }
</style>
<form action="consultaDetalladaP.jsp" method="post">
    <div class="container"> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tile">
                <div class="t-header th-alt bg-blue">
                    <div class="th-title text-center">Buscar persona</div>
                </div>
                <div class="t-body tb-padding">             
                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2 class="text-center"><i class="zmdi zmdi-account m-r-5"></i> Persona</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">

                                <dl class="dl-horizontal">
                                    <dt>Documento</dt>
                                    <dd>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control autocompleto" id="txtBuscarD" placeholder="Buscar..." required name="txtBuscarD"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-success" id="btnBuscarD" name="btnBuscarD">Buscar <i class="zmdi zmdi-search m-r-5"></i></button>
                                        </div>
                                    </dd>
                                </dl>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<%
    String txtBuscarD = request.getParameter("txtBuscarD");
    if (txtBuscarD != null) {
        mdlPersona idpersona = new mdlPersona();
        mdlPlan planAc = new mdlPlan();
        mdlPlanEntrenamiento planE = new mdlPlanEntrenamiento();
        mdlPlanNutricional nutricional = new mdlPlanNutricional();
        mdlValoracion valoracion = new mdlValoracion();
        idpersona.setDocumento_identidad(txtBuscarD);
        ResultSet res = idpersona.optenerIdpersona();
        if (res.next()) {
            int id = res.getInt("idpersona");
            idpersona.setIdpersona(id);
            planAc.setIdpersona(id);
            planE.setIdPersona(id);
            nutricional.setIdpersona(id);
            valoracion.setIdpersona(id);
            ResultSet data = idpersona.consultarPm();
            ResultSet planA = planAc.optenetFechaAc();
            ResultSet entrenamiento = planE.consultarPlanEntrenamientoCliente();
            ResultSet nutri = nutricional.planNutricionalCliente();
            if (data.next()) {
                mdlExtras rfecha = new mdlExtras();
                rfecha.setFecha(data.getString("fecha_nacimiento"));
                String fecha = rfecha.revertirFecha();
%>

<div class="container">
    <div class="tile" id="profile-main">
        <div class="pm-overview c-overflow-dark">
            <div class="pmo-pic">
                <div class="p-relative">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div id="form1" runat="server">
                                    <img id="image_upload_preview" height="259" width="259" src="../upload/<%=data.getString("foto")%>" alt="your image" class="btn-block" value="img/profile-pic.jpg" />
                                    <!--<input class="btn btn-file" type='file' id="inputFile" name="fleFoto"  value="img/profile-pic.jpg" />-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="pmo-block pmo-contact hidden-xs">
                <h2>Contact</h2>
                <ul>
                    <li><i class="zmdi zmdi-phone"></i> 00971 12345678 9</li>
                    <li><i class="zmdi zmdi-email"></i> malinda-h@gmail.com</li>
                    <li><i class="zmdi zmdi-facebook-box"></i> malinda.hollaway</li>
                    <li><i class="zmdi zmdi-twitter"></i> @malinda (twitter.com/malinda)</li>
                </ul>
            </div>-->
        </div>
        <div>
            <div class="pm-body clearfix">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos Personales</a></li>
                    <li role="presentation"><a href="#planes" aria-controls="profile" role="tab" data-toggle="tab">planes adquiridos</a></li>
                    <li role="presentation"><a href="#entrenamiento" aria-controls="entrenamiento" role="tab" data-toggle="tab">Plan de entrenamiento</a></li>
                    <li role="presentation"><a href="#nutricion" aria-controls="nutricion" role="tab" data-toggle="tab">Plan nutricional</a></li>
                    <!--<li role="presentation"><a href="#valoracion" aria-controls="valoracion" role="tab" data-toggle="tab">Valoracion</a></li>-->
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">  
                        <form method="post" action="../ctrDatos">
                            <div class="pmb-block">
                                <div class="pmbb-header">
                                    <h2><i class="zmdi zmdi-account m-r-5"></i> Información Basica</h2>

                                    <!--<ul class="actions">
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a data-pmb-action="edit" href="#">Editar</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>-->
                                </div>
                                <div class="pmbb-body p-l-30">
                                    <div class="pmbb-view">
                                        <input type="hidden" name="txtIdpersonaM"  value="<%=data.getString("idpersona")%>">
                                        <input type="hidden" name="txtRollM"  value="<%=data.getString("idTipo_persona")%>">
                                        <dl class="dl-horizontal">
                                            <dt>Primer nombre</dt>
                                            <dd><%=data.getString("primer_nombre")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Segundo nombre</dt>
                                            <dd><%=data.getString("segundo_nombre")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Primer apellido</dt>
                                            <dd><%=data.getString("primer_apellido")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Segundo apellido</dt>
                                            <dd><%=data.getString("segundoApellido")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Documento</dt>
                                            <dd><%=data.getString("documento_identidad")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Fecha de nacimiento</dt>
                                            <dd><%=fecha%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Profeciono</dt>
                                            <dd><%=data.getString("nspecialidad")%></dd>
                                        </dl>
                                    </div>
                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Primer nombre</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtPrimerNombreM" value="<%=data.getString("primer_nombre")%>" class="form-control" placeholder="Primer Nombre">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Segundo nombre</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtSegundoNombreM" value="<%=data.getString("segundo_nombre")%>" class="form-control" placeholder="Segundo Nombre">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Primer apellido</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type='text' name="txtPrimerApellidoM" value="<%=data.getString("primer_apellido")%>" class="form-control" data-toggle="dropdown" placeholder="Primer apellido">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Segundo apellido</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtSegundoApellidoM"  value="<%=data.getString("segundoApellido")%>" class="form-control" placeholder="Segundo apellido">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Documento</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtDocumentoM" value="<%=data.getString("documento_identidad")%>" class="form-control" placeholder="Documento">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Fecha de nacimiento</dt>
                                            <dd>
                                                <div class="dtp-container dropdown fg-line">
                                                    <input type='text' name="txtFechaM" value="<%=fecha%>" class="form-control date-picker" data-toggle="dropdown" placeholder="Fecha de nacimiento">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Profecion</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type='text' name="txtProfecionM" value="<%=data.getString("nspecialidad")%>" class="form-control" data-toggle="dropdown" placeholder="Profecion">
                                                </div>
                                            </dd>
                                        </dl>

                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" name="btnModificar">Guardar</button>
                                            <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pmb-block">
                                <div class="pmbb-header">
                                    <h2><i class="zmdi zmdi-phone m-r-5"></i> Informació de contacto</h2>

                                    <!--<ul class="actions">
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>

                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a data-pmb-action="edit" href="#">Editar</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>-->
                                </div>
                                <div class="pmbb-body p-l-30">
                                    <div class="pmbb-view">
                                        <dl class="dl-horizontal">
                                            <dt>Celular</dt>
                                            <dd><%=data.getString("Celular")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Telefono fijo</dt>
                                            <dd><%=data.getString("telefono")%></dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Correo</dt>
                                            <dd><%=data.getString("correo")%></dd>
                                        </dl>
                                    </div>

                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Celular</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtCelularM" value="<%=data.getString("Celular")%>" class="form-control" placeholder="Celular">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Telefono Fijo</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtTelefonoM" value="<%=data.getString("telefono")%>" class="form-control" placeholder="Telefono">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Correo</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="txtCorreoM" value="<%=data.getString("correo")%>" class="form-control" placeholder="Correo electronico">
                                                </div>
                                            </dd>
                                        </dl>
                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" name="btnModificar">Guardar</button>
                                            <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--<div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-lock m-r-5"></i> Accseso</h2>
                                <!--<ul class="actions">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>

                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a data-pmb-action="edit" href="#">Editar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Usuario</dt>
                                        <dd></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Contraseña</dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                                <div class="pmbb-edit">
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Usuario</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" class="form-control" placeholder="eg. 00971 12345678 9">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Contraseña anterior</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="email" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Nueva contraseña</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="email" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Confirmar contraseña</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="email" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <div class="m-t-30">
                                        <button class="btn btn-primary btn-sm">Guardar</button>
                                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <div role="tabpanel" class="tab-pane" id="planes">
                        <div class="container">
                            <div class="tile">
                                <div class="action-header clearfix">
                                    <h2 class="ah-label hidden-xs">Planes</h2>

                                    <div class="ah-search">
                                        <input type="text" placeholder="Start typing..." class="ahs-input">

                                        <i class="ahs-close">&times;</i>
                                    </div>

                                    <ul class="ah-actions actions">
                                        <li>
                                            <a data-toggle="modal" href="#modalDefault"><i class="zmdi zmdi-plus-circle"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="p-timeline">
                                    <div class="pt-line c-gray text-right">
                                        <span class="d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        &nbsp;
                                    </div>  
                                    <div class="pt-body">
                                        <h2 class="ptb-title">Plan Actual</h2>
                                        <div class="lightbox clearfix">
                                            <ul class="clist clist-star">
                                                <%
                                                    if (planA.next()) {
                                                %>
                                                <li><%=planA.getString("nombre")%> inicio <%=planA.getString("fecha_inicio")%> hasta <%=planA.getString("fecha_final")%></li>
                                                <li>Estado
                                                    <ul>
                                                        <%
                                                            if (planA.getString("estado").equals("1")) {%>
                                                        <li>Activo</li>
                                                            <%} else {%>
                                                        <li>Inactivo</li>
                                                            <%}%>
                                                    </ul>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-timeline">
                                    <div class="pt-line c-gray text-right">
                                        <span class="d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        &nbsp;
                                    </div>  
                                    <div class="pt-body">
                                        <h2 class="ptb-title">Adquiridos Pilates</h2>
                                        <div class="lightbox clearfix">
                                            <ul class="clist clist-star">
                                                <li>Trimestre-02/05/2015</li>                                              
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-timeline">
                                    <div class="pt-line c-gray text-right">
                                        <span class="d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        &nbsp;
                                    </div>  
                                    <div class="pt-body">
                                        <h2 class="ptb-title">Adquiridos Normal</h2>
                                        <div class="lightbox clearfix">
                                            <ul class="clist clist-star">
                                                <li>Lorem ipsum dolor sit amet</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="entrenamiento">
                        <div class="container">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Lunes
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 1) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Martes
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <%entrenamiento.beforeFirst();%>
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 2) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>                  
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Miercoles
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <%entrenamiento.beforeFirst();%>
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 3) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>            
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingFoor">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFoor" aria-expanded="false" aria-controls="collapseFoor">
                                                Jueves
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFoor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFoor">
                                        <div class="panel-body">
                                            <%entrenamiento.beforeFirst();%>
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 4) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>          
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                Viernes
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            <%entrenamiento.beforeFirst();%>
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 5) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>           
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingSix">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                                Sabado
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                        <div class="panel-body">
                                            <%entrenamiento.beforeFirst();%>
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 6) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>          
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingSeven">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                                Domingo
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                        <div class="panel-body">
                                            <%entrenamiento.beforeFirst();%>
                                            <ul>
                                                <%
                                                    while (entrenamiento.next()) {
                                                        if (entrenamiento.getInt("iddia") == 7) {
                                                %>            
                                                <li><%=entrenamiento.getString("nombre")%> <%=entrenamiento.getString("hora_inicio")%></li>
                                                    <% }
                                                        }
                                                    %>
                                            </ul>    
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="nutricion">
                        <div class="container">
                            <div class="row">
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-danger panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-cloud"></i>
                                            <h3>Mañana</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Mañana")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>

                                    </div>
                                </div>
                                <!-- /item -->
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-warning panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-coffee"></i>
                                            <h3>Media Mañana</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Media Mañana")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /item -->
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-warning panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-cutlery"></i>
                                            <h3>Almuerzo</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Almuerzo")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /item -->
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-warning panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-sun-o"></i>
                                            <h3>Algo 4pm</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Algo 4 pm")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /item -->
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-warning panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-bell"></i>
                                            <h3>Algo 6pm</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Algo 6 pm")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /item -->
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-warning panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-moon-o"></i>
                                            <h3>Comida</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Comida")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /item -->
                                <!-- item -->
                                <div class="col-md-4 text-center">
                                    <div class="panel panel-warning panel-pricing">
                                        <div class="panel-heading">
                                            <i class="fa fa-heart"></i>
                                            <h3>Merienda</h3>
                                        </div>
                                        <ul class="list-group text-center">
                                            <%
                                                nutri.beforeFirst();
                                                while (nutri.next()) {
                                                    if (nutri.getString("horario").equals("Merienda")) {
                                            %>

                                            <li class="list-group-item">Porciones: <%=nutri.getString("porcion")%> <br> Alimento: <%=nutri.getString("alimento")%></li>
                                                <%}
                                                    }%>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /item -->
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="valoracion">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="../ctrPlan">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Plan</h4>
                </div>
                <div class="modal-body">

                    <div class="t-body tb-padding">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                <select name="slcPlan" class="selectpicker" data-live-search="true">
                                    <option></option>
                                    <optgroup label="Normal"> 
                                        <%
                                            ctrPlan lis = new ctrPlan();
                                            ResultSet rp = lis.listarPlan();
                                            while (rp.next()) {
                                                if (rp.getString("nombreTp").equals("Normal") && rp.getString("estado").equals("1")) {

                                        %> 
                                        <option value="<%=rp.getString("idplan")%>"><%=rp.getString("nombre")%></option>

                                        <%}%>
                                        <%}%>
                                    </optgroup> 
                                    <% rp.beforeFirst();%> 
                                    <optgroup label="Pilates"> 
                                        <%
                                            while (rp.next()) {
                                                if (rp.getString("nombreTp").equals("Pilates") && rp.getString("estado").equals("1")) {
                                        %>
                                        <option value="<%=rp.getString("idplan")%>"><%=rp.getString("nombre")%></option>
                                        <%}%>                                       
                                        <%}%>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                <input type="text" hidden="" value="<%=data.getString("idpersona")%>" name="txtIdPersona"/>
                                <div class="dtp-container dropdown fg-line">
                                    <input type='text' class="form-control date-picker" data-toggle="dropdown" name="txtFechaPlan" placeholder="Fecha Inicio del Plan">
                                </div>
                            </div>
                        </div>
                    </div>         
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary" name="btnGuardarPlan" id="btnGuardarPlan">Guardar</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>                   
        </div>
    </div>
</div>
<%
            }
        }
    }
%>
<jsp:include page="footer.jsp"/>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
</script>
<script>
    if ($('.autocompleto')[0]) {
    var statesArray = [
    <%
        ctrPersona doc = new ctrPersona();
        ResultSet res = doc.consultarP();
        while (res.next()) {
    %>
    '<%=res.getString("documento_identidad")%>',
    <%}%>
    ];
            var states = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: statesArray
            });
            $('.autocompleto').typeahead({
    hint: true,
            highlight: true,
            minLength: 1
    },
    {
    name: 'states',
            source: states
    });
    }
</script>
<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>