<%
if (request.getParameter("estado") != null) {
if (request.getParameter("estado").equals("1")) {
HttpSession se = request.getSession();
se.invalidate();
response.sendRedirect("view/inicio.jsp");
}
}
%>
<%-- 
Document   : index
Created on : 14-oct-2015, 13:11:01
Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ser Athltic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/sl-slide.css">

    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

    <style type="text/css">
        .zoom1{
            transition: 1.5s ease;
            -moz-transition: 1.5s ease; /* Firefox */
            -webkit-transition: 1.5s ease; /* Chrome - Safari */
            -o-transition: 1.5s ease; /* Opera */
        }
        .zoom1:hover{
            transform : scale(2);
            -moz-transform : scale(2); /* Firefox */
            -webkit-transform : scale(2); /* Chrome - Safari */
            -o-transform : scale(2); /* Opera */
            -ms-transform : scale(2); /* IE9 */
        }
        .zoom{
            transition: 1.5s ease;
            -moz-transition: 1.5s ease; /* Firefox */
            -webkit-transition: 1.5s ease; /* Chrome - Safari */
            -o-transition: 1.5s ease; /* Opera */
        }
        .zoom:hover{
            transform : scale(2.5);
            -moz-transform : scale(2.5); /* Firefox */
            -webkit-transform : scale(2.5); /* Chrome - Safari */
            -o-transform : scale(2.5); /* Opera */
            -ms-transform : scale(2.5); /* IE9 */
        }
    </style>

</head>

<body>

    <!--Header-->
    <header class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a id="logo" class="pull-left" href="index.jsp"></a>
                <div class="nav-collapse collapse pull-right">
                    <ul class="nav">
                        <li class="active"><a href="index.jsp">Inicio</a></li>
                        <li><a href="#nosotros">Nosotros</a></li>
                        <li><a href="#services">Servicios</a></li>
                        <li><a href="#galeria">Galería</a></li>
                        <li><a href="#bottom">Contactenos</a></li>
                        <li class="login">
                            <a data-toggle="modal" data-toggle="modal" data-target="#loginModal"><i class="icon-lock"></i> Inicio Sesión</a>
                        </li>
                    </ul>        
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </header>
    <!-- /header -->

    <!--Slider-->
    <section id="slide-show">
        <div id="slider" class="sl-slider-wrapper"> <!--Slider Items-->    
            <div class="sl-slider">  <!--Slider Item1-->
                <div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                    <div class="sl-slide-inner">
                        <div class="container">
                            <img class="pull-right" src="images/sample/slider/img1.PNG.jpg" alt="" />
                            <h2>PilatesReformer</h2>
                            <h3 class="gap" align="justify">Entrenamiento personaizado</h3>
                            <a class="btn btn-large btn-transparent" href="http://inmapilates.blogspot.com/2011/12/el-pilates-reformer-y-sus-beneficios.html " target="_black">Leer Mas</a>
                        </div>
                    </div>
                </div>  <!--/Slider Item1-->

                <!--Slider Item2-->
                <div class="sl-slide item2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
                    <div class="sl-slide-inner">
                        <div class="container">
                            <img class="pull-right" src="images/sample/slider/img2.png.jpg" alt="" />
                            <h2>Gran Caminata</h2>
                            <h3 class="gap">Caminar es uno de los mejores ejercicios que existen </h3>
                            <a class="btn btn-large btn-transparent" href="#">Leer Mas</a>
                        </div>
                    </div>
                </div>
                <!--Slider Item2-->

                <!--Slider Item3-->
                <div class="sl-slide item3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
                    <div class="sl-slide-inner">
                        <div class="container">
                            <img class="pull-right" src="images/sample/slider/img3.png.jpg" alt="" />
                            <h2>Nuestros Servicios</h2>
                            <h3 class="gap">visitar los cuatro espacios que Ser Athletic tiene diseñados para entrenar con toda</h3>
                            <a class="btn btn-large btn-transparent" href="#">Leer Mas</a>
                        </div>
                    </div>
                </div>  <!--Slider Item3-->
            </div>  <!--/Slider Items-->

            <!--Slider Next Prev button-->
            <nav id="nav-arrows" class="nav-arrows">
                <span class="nav-arrow-prev"><i class="icon-angle-left"></i></span>
                <span class="nav-arrow-next"><i class="icon-angle-right"></i></span> 
            </nav> <!--/Slider Next Prev button-->
        </div>    <!-- /slider-wrapper -->           
    </section>    <!--/Slider-->

    <section class="main-info" id="nosotros">
        <div class="container">
            <div class="row-fluid">
                <div class="span9">
                    <h4>visitanos en nuestra pagina de Facebook</h4>
                    <p class="no-margin">conose todos los eventos y beneficios que tenemos para ti</p>
                </div>
                <div class="span3">
                    <a class="btn btn-info btn-large pull-right" href="https://es-la.facebook.com/">Facebook</a>
                </div>
            </div>
        </div>
    </section>

    <!--Nosotros-->
    <section id="nosotros1">
        <div class="container">
            <div class="row">
                <h2>Ser Athletic</h2>
                <p class="lead">
                    En su primer año ha logrado un progreso significativo, mejorando en el proceso de métodos de entrenamientos preventivos como herramientas para garantizar un buen estilo de vida saludable.
                    Este proceso lo hemos desarrollado mediante convenios con empresas aliadas prestadora de servicios de salud “Mentalfit” y además contamos con profesionales en deporte y actividad física haciendo un acompañamiento constante a nuestros usuarios para garantizar mayores resultados.
                    Además resaltamos que somos una empresa legalmente constituida con registro mercantil Nit 8356340-7, prestamos los servicios de clases grupales como aeróbicos, rumba, tonificación, yoga y estiramientos, contamos con zona cardiovascular, zona de musculación y salón de Pilates especializado para brindar un servicio más personalizado.
                </p>	
                <p class="lead">
                    <strong>Ser Athletic, un estilo de vida.</strong>
                </p>
            </div>
        </div>
    </section>

    <!--Services-->
    <section id="services">
        <div class="container">
            <div class="center gap">
                <h3>Servicios</h3>
                <p class="lead">
                    Somos un Centro de acondicionamiento físico inspirado en rutinas de entrenamiento semipersonalizado, incluida dentro de nuestro paquete de entrenamiento.
                </p>
            </div>

            <div class="row-fluid">
                <div class="span4">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-thumbs-up-alt icon-medium"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Entrenamiento funcional:</h4>
                            <p>
                                La evolución de las pesas y el trabajo monótono ha cambiado.
                            </p>
                            <p>
                                Por eso contamos con rutinas de entrenamiento funcional cada hora. 
                            </p>
                            <p>
                                Esto a través de un circuito que involucra todo el cuerpo, combinado con ejercicios cardiovasculares.
                            </p>
                        </div>
                    </div>
                </div>            

                <div class="span4">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-thumbs-up-alt icon-medium"></i>
                        </div>
                        <div class="media-body">
                            <p>
                                <h4>Oferta:</h4>
                                Tenemos clases para cuidar la integridad física del cuerpo, al tiempo que se evidencian resultados.
                            </p>
                            <p>
                                Exclusivos ejercicios en Plataforma vibratoria que optimizan el resultado del entrenamiento.
                            </p>
                        </div>
                    </div>
                </div>  

                <div class="span4">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-thumbs-up-alt icon-medium"></i>
                        </div>
                        <div class="media-body">
                            <p>
                                <h4 class="media-heading"></h4>

                                <h4 class="media-heading">A.C.C: </h4>Abdomen, cintura y cadera.
                            </p>
                            <p>
                                <h4 class="media-heading">Crossfit: </h4>Entrenamiento funcional, para definir y ganar masa muscular.
                            </p>
                            <p>
                                <h4 class="media-heading">Pilates Reformer: </h4>Ejercicio en máquinas y Vital Plate especial para tonificar y estirar, ideal para fortalecer columna y rodillas.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gap"></div>
            <div class="row-fluid">
                <div class="span4">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-thumbs-up-alt icon-medium"></i>
                        </div>
                        <div class="media-body">
                            <p>
                                <h4 class="media-heading">Clases grupales:</h4> Tae- Bo, Rumba, Stretching, Cross fit, Power, Aeróbicos, RTG, Kick Boxing y Spinning.
                            </p>
                            <p>
                                <h4 class="media-heading">Pesas:</h4> Alternativa para quienes buscan armonía, quieren bajar de peso, definir y tonificar.
                            </p>
                            <p>
                                <h4 class="media-heading">Pilates reformer:</h4> Técnica para mejorar la fuerza, definición, fortalecimiento muscular, flexibilidad y estado físico.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="span4">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-thumbs-up-alt icon-medium"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"></h4>
                            <p>
                                <h4 class="media-heading">RIP 60:</h4> Prueba muscular por 1 minuto.
                            </p>
                            <p>
                                <h4 class="media-heading">Rumba:</h4> Clase para mejorar la condición física por medio del baile.
                            </p>
                            <p>
                                <h4 class="media-heading">Stretching:</h4> 100% estiramiento, fortalece las articulaciones.
                            </p>
                            <p>
                                <h4 class="media-heading">Power: </h4>Entrenamiento de fuerza, pesas.
                            </p>
                        </div>
                    </div>
                </div>   
                <div class="span4">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-thumbs-up-alt icon-medium"></i>
                        </div>
                        <div class="media-body">
                            <p>
                                <h4 class="media-heading">Tae-bo:</h4> Artes marciales.
                            </p>
                            <p>
                                <h4 class="media-heading">Tono:</h4> Aeróbicos y tonificación.
                            </p>
                            <p>
                                <h4 class="media-heading">Yoga:</h4> Clase de relajación, postura muscular.
                            </p>
                            <p>
                                <h4 class="media-heading">R.T.G: </h4>Reducción de Tejido Graso.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/Services-->

    <!-- galeria-->
    <section id="galeria">
        <div class="container">
            <div class="center">
                <h3>Galería</h3>
                <p class="lead">La vida está llena de RETOS, preparate para enfrentarlos.</p>
            </div>  
            <div class="gap"></div>
            <ul class="gallery col-4">
                <!--Item 1-->
                <li class="spam3">
                    <div class="w3-container ">
                        <img class="zoom1" src="images/galería/1 (12).jpg">
                    </div>
                    <div class="desc">
                        <h5>Hoy es Martes de RUMBA, nos vemos en Ser Athletic, entre Familia y Amigos es mas vacano! Hora 7:10 pm.</h5>
                    </div>                 
                </li>
                <!--/Item 1--> 

                <!--Item 2-->
                <li class="spam3">
                    <div class="w3-container ">
                        <img class="zoom1" src="images/galería/1 (2).jpg">
                    </div>
                    <div class="desc">
                        <h5>Ser Athletic felicita a todos sus amigos que con su ejemplo y dedicación enseñan a sus hijos con el amor, sembrando una semilla de esperanza para que ellos aprendan a tener una vida balanceada ejercicio, alimentación y familia.</h5>
                    </div>					
                </li>
                <!--/Item 2-->

                <!--Item 3-->
                <li class="spam3">
                    <div class="w3-container ">
                        <img class="zoom1" src="images/galería/1 (3).jpg">
                    </div>
                    <div class="desc">
                        <h5>Maratón Dia Ser Athletic, entrenamiento cardio muscular, JUEVES 23 de junio hora 7:10 pm, te retamos a participar con tús amigos y familiares y demuestra que eres el mejor!</h5>
                    </div>                 
                </li>
                <!--/Item 3--> 

                <!--Item 4-->
                <li class="spam3">
                    <div class="w3-container ">
                        <img class="zoom1" src="images/galería/1 (9).jpg">
                    </div>
                    <div class="desc">
                        <h5>Los viernes tenemos una super rutina de abdomen, no faltes porque el entrenador Carlos Grisales va hoy con toda.!</h5>
                    </div>         
                </li>
                <!--/Item 4-->               
            </ul>
        </div>
    </section>

    <!--Bottom-->
    <section id="bottom" class="main">
        <div class="container">   <!--Container-->
            <div class="row-fluid">   <!--row-fluids-->

                <!--Contact Form-->
                <div class="span3">
                    <h4>DIRECCION</h4>
                    <ul class="unstyled address">
                        <li>
                            <i class="icon-home"></i><strong>Direccion:</strong> Calle 31A Sur N 43A 16.<br> Envigado – Antioquia.
                        </li>
                        <li>
                            <i class="icon-envelope"></i>
                            <strong>Email: </strong> mercadeoserathletic@gmail.com<br> serathletic@gmail.com<br>alejandropilates@hotmail.com<br>
                        </li>
                        <li>
                            <i class="icon-phone"></i>
                            <strong>Telefonos:</strong> 3669887,3146149292 3015537179
                        </li>
                    </ul>
                </div>
                <!--End Contact Form-->

                <!--Important Links-->
                <div id="tweets" class="span3">
                    <h4>Mapa de sitio</h4>
                    <div>
                        <ul class="arrow">
                            <li class="active"><a href="index.jsp">Inicio</a></li>
                            <li><a href="#nosotros">Nosotros</a></li>
                            <li><a href="#services">Servicios</a></li>
                            <li><a href="#galeria">Galería</a></li>
                            <li><a href="#bottom">Contactenos</a></li>
                        </ul>
                    </div>  
                </div>
                <!--Important Links-->

                <!--Archives-->
                <div id="archives" class="span3">
                    <h4>Redes sociales</h4>
                    <div>
                        <ul class="arrow">
                            <li>
                                <i class="icon-globe"></i>
                                <strong>Facebook:</strong> <a href="https://www.facebook.com/SerAthletic/">Ser Athletic</a>
                            </li>
                            <li>
                                <i class="icon-globe"></i>
                                <strong>Google+:</strong> <a href="https://plus.google.com/u/0/109416323977989250666/posts">Ser Athletic</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--End Archives-->

                <!--galeria 2-->
                <div class="span3">
                    <h4>Galería</h4>
                    <div class="row-fluid first">
                        <ul class="thumbnails">
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (4).jpg">
                            </li>
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (7).jpg">
                            </li>
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (10).jpg" >
                            </li>
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (11).jpg" >
                            </li>
                        </ul>
                    </div>

                    <div class="row-fluid">
                        <ul class="thumbnails">
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (13).jpg" >
                            </li>
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (16).jpg" >
                            </li>
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (6).jpg" >
                            </li>
                            <li class="span3">
                                <img class="zoom" src="images/galería/1 (1).jpg" />
                            </li>
                        </ul>
                    </div>
                </div>				<!--galeria 2-->
            </div>			<!--/row-fluid-->
        </div>		<!--/container-->
    </section>	<!--/bottom-->

    <!--Footer-->
    <footer id="footer">
        <div class="container">
            <div class="row-fluid">
                <div class="span5 cp">
                    &copy; <a id="fechaActual"></a>  <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">SystemMYDE</a>. Todos los derechos reservados.
                </div>
                <!--/Copyright-->

                <div class="span6">
                    <ul class="social pull-right">
                        <li><a href="https://es-es.facebook.com/SerAthletic"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                        <li><a href="https://plus.google.com/u/0/109416323977989250666/posts"><i class="icon-google-plus"></i></a></li>                       
                        <li><a href="https://www.youtube.com/channel/UC6WNpMNunSwHgAHH8f8at4w"><i class="icon-youtube"></i></a></li>
                        <li><a href="#"><i class="icon-tumblr"></i></a></li>                        
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                        <li><a href="#"><i class="icon-rss"></i></a></li>
                        <li><a href="#"><i class="icon-github-alt"></i></a></li>
                        <li><a href="#"><i class="icon-instagram"></i></a></li>                   
                    </ul>
                </div>

                <div class="span1">
                    <a id="gototop" class="gototop pull-right" href="#"><i class="icon-angle-up"></i></a>
                </div>
                <!--/Goto Top-->
            </div>
        </div>
    </footer>
    <!--/Footer-->


    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div  class="modal hide fade in modal-sm" id="loginModal" aria-hidden="false">
                    <div class="modal-header">
                        <i class="icon-remove" data-dismiss="modal" aria-hidden="true"></i>
                        <h4>Iniciar Sesión</h4>
                    </div>                 
                    <div class="modal-body">
                        <form class="form-inline" action="ctrLogin" method="post" id="form-login">
                            <input type="text" class="input-small" placeholder="Usuario" name="txtUsuario" required>
                            <input type="password" class="input-small" placeholder="Contraseña" name="txtClave" required>
                            <label class="checkbox"><input type="checkbox"> Recordarme</label>
                            <button type="submit" class="btn btn-primary" name="btnIniciar">Iniciar</button>
                        </form>
                        <a href="recuperarClave.jsp">Olvidaste tu contraseña?</a>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <script src="js/vendor/jquery-1.9.1.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- Required javascript files for Slider -->
    <script src="js/jquery.ba-cond.min.js"></script>
    <script src="js/jquery.slitslider.js"></script>
    <!-- /Required javascript files for Slider -->

    <!-- SL Slider -->
    <script type="text/javascript">
        $(function () {
            var Page = (function () {

                var $navArrows = $('#nav-arrows'),
                slitslider = $('#slider').slitslider({
                    autoplay: true
                }),
                init = function () {
                    initEvents();
                },
                initEvents = function () {
                    $navArrows.children(':last').on('click', function () {
                        slitslider.next();
                        return false;
                    });

                    $navArrows.children(':first').on('click', function () {
                        slitslider.previous();
                        return false;
                    });
                };

                return {init: init};

            })();

            Page.init();
        });
    </script>
    <!-- /SL Slider -->
    <script>
        var ano = (new Date).getFullYear();

        $(document).ready(function () {
            $("#fechaActual").text(ano);
        });
    </script>
</body>
</html>