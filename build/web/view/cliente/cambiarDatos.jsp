<%@page import="model.mdlPlan"%>
<%@page import="controller.ctrPlan"%>
<%@page import="model.mdlExtras"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="model.mdlPersona"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : inicioCliente
    Created on : 25-nov-2015, 23:06:03
    Author     : daniel
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="headerCliente.jsp"/> 
<div class="container">
    <header class="page-header">
    </header> 
    <div class="tile" id="profile-main"> 
        <div class="pm-overview c-overflow-dark">
            <div class="pmo-pic">
                <div class="p-relative"> 
                    <%
                        String idPersona = String.valueOf(ht.getAttribute("idpersona"));
                        if (idPersona != null) {
                            mdlPersona md = new mdlPersona();
                            md.setIdpersona(Integer.parseInt(idPersona));
                            ResultSet res = md.consultarPm();
                            if (res.next()) {

                    %>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <form id="form1" runat="server" method="post" action="../../ctrArchivos" enctype="multipart/form-data">
                                    <img id="image_upload_preview" height="259" width="259" src="../../upload/<%=res.getString("foto")%>" alt="your image" class="btn-block" value="../../upload/<%=ht.getAttribute("foto")%>" />
                                    <input class="btn btn-file" type='file' id="inputFile" name="fleFoto"  value="../../upload/<%=res.getString("foto")%>" />
                                    <hr hidden id="guardarFotoH">
                                    <input type="text" hidden value="<%=res.getString("idpersona")%>" name="txtIdPersona">
                                    <button hidden class="btn btn-block btn-success" type="submit" id="guardarFoto">Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <%}%>
                </div>
                <!--<div class="pmo-stat">
                    <h2 class="m-0 c-white">1562</h2>
                    Total Connections
                </div>-->
            </div>

            <div class="pmo-block pmo-contact hidden-xs">
                <h2>Datos</h2>

                <ul>
                    <%                        mdlPlan op = new mdlPlan();
                        op.setIdpersona(Integer.parseInt(String.valueOf(ht.getAttribute("idpersona"))));
                        ResultSet lis = op.optenetFechaAc();
                        if (lis.next()) {
                    %>
                    <li> Plan Actual:  <i class="zmdi zmdi-assignment-o"></i><%=lis.getString("nombre")%> </li>
                        <%}%>
                    <!-- <li>
                         <i class="zmdi zmdi-pin"></i>
                         <address class="m-b-0">
                             10098 ABC Towers, <br/>
                             Dubai Silicon Oasis, Dubai, <br/>
                             United Arab Emirates
                         </address>
                     </li>-->
                    <li><i class="zmdi zmdi-male"></i> peso</li>
                    <li><i class="zmdi zmdi-male-alt"></i> Altura</li>
                    <li><i class="zmdi zmdi-run"></i> cintura</li>
                </ul>
            </div>

            <!--<div class="pmo-block pmo-items hidden-xs">
                <h2>Connections</h2>

                <div class="pmob-body">
                    <div class="row">
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/1.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/2.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/3.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/4.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/5.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/6.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/7.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/8.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/1.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/2.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/3.jpg" alt="">
                        </a>
                        <a href="#" class="col-xs-2">
                            <img class="img-avatar" src="img/profile-pics/4.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>-->
        </div>

        <div class="pm-body clearfix">
            <ul class="tab-nav tn-justified">
                <li class="active"><a href="profile-about.html">Perfil</a></li>
                <!--<li><a href="profile-photos.html">Photos</a></li>
                <li><a href="profile-connections.html">Connections</a></li>-->
            </ul>
            <%
                res.beforeFirst();
                if (res.next()) {
                    mdlExtras rfecha = new mdlExtras();
                    rfecha.setFecha(res.getString("fecha_nacimiento"));
                    String fecha = rfecha.revertirFecha();
            %>
            <form method="post" action="../../ctrPersona">
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-account m-r-5"></i> Información básica</h2>

                        <ul class="actions">
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>

                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-pmb-action="edit" href="#">Editar</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Primer Nombre</dt>
                                <dd><%=res.getString("primer_nombre")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Segundo Nombre</dt>
                                <dd><%=res.getString("segundo_nombre")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Primer Apellido</dt>
                                <dd><%=res.getString("primer_apellido")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Segundo Apellido</dt>
                                <dd><%=res.getString("segundoApellido")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Documento</dt>
                                <dd><%=res.getString("documento_identidad")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Fecha de Nacimiento</dt>
                                <dd><%=fecha%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Profecion</dt>
                                <dd><%=res.getString("nspecialidad")%></dd>
                            </dl>
                        </div>

                        <div class="pmbb-edit">
                            <input type="hidden" name="txtIdpersonaM"  value="<%=res.getString("idpersona")%>">
                            <input type="hidden" name="txtRollM"  value="<%=res.getString("idTipo_persona")%>">
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Primer Nombre</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("primer_nombre")%>" placeholder="Primer Nombre" name="txtPrimerNombreM" required>
                                    </div>

                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Segundo Nombre</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("segundo_nombre")%>" placeholder="Segundo Nombre" name="txtSegundoNombreM">
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Primer Apellido</dt>
                                <dd>
                                    <div class="dtp-container dropdown fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("primer_apellido")%>" placeholder="Primer Apellido" name="txtPrimerApellidoM" required>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Segundo Apellido</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("segundoApellido")%>" placeholder="Segundo Apellido" name="txtSegundoApellidoM" required>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Documento</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("documento_identidad")%>" placeholder="documento identidad" name="txtDocumentoM" required>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Fecha De Nacimiento</dt>
                                <dd>
                                    <div class="fg-line">
                                        <div class="dtp-container dropdown fg-line">
                                            <input type='text' class="form-control date-picker" data-toggle="dropdown" value="<%=fecha%>" placeholder="Fecha Nacimiento" name="txtFechaM" required>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Profecion</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("nspecialidad")%>" placeholder="Profecion" name="txtProfecionM">
                                    </div>
                                </dd>
                            </dl>

                            <div class="m-t-30">
                                <button type="submit" class="btn btn-primary btn-sm" name="btnModificar" id="btnGuardarDC">Guardar</button>
                                <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-phone m-r-5"></i> Información del contacto</h2>

                        <ul class="actions">
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>

                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-pmb-action="edit" href="#">Edita</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Celular</dt>
                                <dd><%=res.getString("Celular")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Telefono fijo</dt>
                                <dd><%=res.getString("telefono")%></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Correo Electronico</dt>
                                <dd><%=res.getString("correo")%></dd>
                            </dl>
                        </div>

                        <div class="pmbb-edit">
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Celular</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("Celular")%>" placeholder="Celular" name="txtCelularM">
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Telefono fijo</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" value="<%=res.getString("telefono")%>" placeholder="Telefono fijo" name="txtTelefonoM" >

                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Correo Electronico</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="email" class="form-control" value="<%=res.getString("correo")%>" placeholder="eg. malinda.h@gmail.com" name="txtCorreoM">
                                    </div>
                                </dd>
                            </dl>

                            <div class="m-t-30">
                                <button class="btn btn-primary btn-sm" name="btnModificar">Guardar</button>
                                <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <%
                    }
                }
            %>
            <div class="pmb-block">
                <div class="pmbb-header">
                    <h2><i class="zmdi zmdi-lock m-r-5"></i> Cambiar contraseña</h2>

                    <ul class="actions">
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>

                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a data-pmb-action="edit" href="#">Editar</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="pmbb-body p-l-30">
                    <!--<div class="pmbb-view">
                        <dl class="dl-horizontal">
                            <dt>Full Name</dt>
                            <dd>Mallinda Hollaway</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Gender</dt>
                            <dd>Female</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Birthday</dt>
                            <dd>June 23, 1990</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Martial Status</dt>
                            <dd>Single</dd>
                        </dl>
                    </div>-->
                    <form action="../../ctrPersona" method="post">
                        <input type="hidden" name="txtId" value="<%=ht.getAttribute("idusuario")%>">
                        <div class="pmbb-edit">
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Contraseña Anterior</dt>
                                <dd>
                                    <div class="fg-line">
                                        <div  class="" id="validacion">
                                            <input type="password" class="form-control" name="txtClaveAnteriorC" id="txtClaveAnteriorC" onchange="validarClave(<%=ht.getAttribute("nombre")%>, this.value)" required>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Nueva contraseña</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="password" class="form-control" min="6" max="10" placeholder="Escriba la contraseña" id="txtNuevaClaveC" name="txtNuevaClaveC" required>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Confirme contreseña</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="password" class="form-control" min="6" max="10" placeholder="Vuelva a escribir la contraseña" id="txtConfirmaClaveC" name="txtConfirmaClaveC" required>
                                        <span id="mensaje" hidden></span>
                                    </div>
                                </dd>
                            </dl>
                            <div class="m-t-30">
                                <button type="submit" class="btn btn-primary btn-sm" name="btnGuardarClave" id="btnGuardarClave">Guardar</button>
                                <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>                
            </div>
        </div>
    </div>
</div>
<jsp:include page="footerCliente.jsp"/>

<script>
    function validarClave(usuario, clave) {
        var usuario = usuario;
        var clave = clave;
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {usuario: usuario, clave: clave},
            url: "../../ctrPersona"
        }).done(function (d) {
            if (d == "") {
                $('#btnGuardarClave').attr("disabled", false);
                $("#validacion").attr("class", "form-group has-success has-feedback");
            } else {
                $('#btnGuardarClave').attr("disabled", true);
                $("#validacion").attr("class", "form-group has-error has-feedback");
            }

        }).fail(function () {
            alert("error");
        });
    }
</script>
<script>
    $(document).ready(function () {
        $("#guardarFoto").hide();
        $("#txtConfirmaClaveC").keyup(function () {
            var dato1 = $("#txtNuevaClaveC").val();
            var dato2 = $("#txtConfirmaClaveC").val();
            valid(dato1, dato2);
        });
        function valid(dato1, dato2) {
            if (dato1 != dato2) {
                $('#btnGuardarClave').attr("disabled", true);
                $("#mensaje").show();
                $("#mensaje").text("no coinciden");
            }
            if (dato1.length == 0 || dato1 == "") {
                $('#btnGuardarClave').attr("disabled", true);
                $("#mensaje").show();
                $("#mensaje").text("llene el campo anterior");
            }
            if (dato1.length != 0 && dato1 == dato2) {
                $('#btnGuardarClave').attr("disabled", false);
                $("#mensaje").show();
                $("#mensaje").text("coinciden");
            }
        }
    });
    function notify(from, align, icon, type, animIn, animOut, title, message) {
        $.growl({
            icon: icon,
            title: title || 'Bootstrap Growl ',
            message: message || 'Turning standard Bootstrap alerts into awesome notifications',
            url: ''
        }, {
            element: 'body',
            type: type,
            allow_dismiss: true,
            placement: {
                from: from,
                align: align
            },
            offset: {
                x: 20,
                y: 85
            },
            spacing: 10,
            z_index: 1031, delay: 2500,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            animate: {
                enter: animIn,
                exit: animOut
            },
            icon_type: 'class',
            template: '<div data-growl="container" class="alert" role="alert">' +
                    '<button type="button" class="close" data-growl="dismiss">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '<span class="sr-only">Close</span>' +
                    '</button>' +
                    '<span data-growl="icon"></span>' +
                    '<span data-growl="title"></span>' +
                    '<span data-growl="message"></span>' +
                    '<a href="#" data-growl="url"></a>' +
                    '</div>'
        });
    }
    ;
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        $("#guardarFotoH").show();
        $("#guardarFoto").show();
        readURL(this);
    });</script>
<script>

    <%        } else {
            response.sendRedirect("../../index.jsp");
        }
    %>
