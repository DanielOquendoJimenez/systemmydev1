<%-- 
    Document   : valoracion
    Created on : 26-nov-2015, 11:23:32
    Author     : daniel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="model.mdlValoracion"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="headerCliente.jsp"/>

<div class="container">
    <!--<header class="page-header">
        <h3>Flot Charts <small>Flot is a pure JavaScript plotting library for jQuery, with a focus on simple usage, attractive looks and interactive features.</small></h3>
    </header>-->
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <div class="t-header">
                    <div class="th-title">Esta es la grafica de estado fisico <small>que hemos hecho en Ser Athletic.</small></div>
                </div>
                <div class="t-body">
                    <div id="grfPeso" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <div class="t-header">
                    <div class="th-title">Esta es la grafica de estado fisico <small>que hemos echo en Ser Athlrtic.</small></div>
                </div>
                <div class="t-body">
                    <div id="grfMasa" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <div class="t-header">
                    <div class="th-title">Esta es la grafica de estado fisico <small>que hemos echo en Ser Athlrtic.</small></div>
                </div>
                <div class="t-body">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <div class="t-header">
                    <div class="th-title">Esta es la grafica de estado fisico <small>que hemos echo en Ser Athlrtic.</small></div>
                </div>
                <div class="t-body">
                    <div id="fisico" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="footerCliente.jsp"/>
<script type="text/javascript">
    $(function () {
    $('#grfPeso').highcharts({
    title: {
    text: 'Valoracion en Peso',
            x: - 20 //center
    },
            subtitle: {
            text: 'Historico',
                    x: - 20
            },
            xAxis: {
            categories: [
    <%
        mdlValoracion list = new mdlValoracion();
        String idPersona = String.valueOf(ht.getAttribute("idpersona"));
        list.setIdpersona(Integer.parseInt(idPersona));
        ResultSet res = list.graficaPeso();
        while (res.next()) {
    %>
            '<%=res.getString("fecha_valoracion")%>',
    <%}%>
            ]
            },
            yAxis: {
            title: {
            text: 'Peso (Kg)'
            },
                    plotLines: [{
                    value: 0,
                            width: 1,
                            color: '#808080'
                    }]
            },
            tooltip: {
            valueSuffix: ' Kg'
            },
            legend: {
            layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
            },
            series: [{
    <%

        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Peso")) {
    %>

            name: '<%=res.getString("nombre")%>',
    <%
            }
        }
    %>
            data: [

    <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Peso")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>

            ]}]
    });
    });

</script>
<script type="text/javascript">
    $(function () {
    $('#grfMasa').highcharts({
    title: {
    text: 'Valoracion en Litros',
            x: - 20 //center
    },
            subtitle: {
            text: 'Historico',
                    x: - 20
            },
            xAxis: {
            categories: [
    <%
       res.beforeFirst();        
        while (res.next()) {
    %>
            '<%=res.getString("fecha_valoracion")%>',
    <%}%>
            ]
            },
            yAxis: {
            title: {
            text: 'Peso (Kg)'
            },
                    plotLines: [{
                    value: 0,
                            width: 1,
                            color: '#808080'
                    }]
            },
            tooltip: {
            valueSuffix: ' Kg'
            },
            legend: {
            layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
            },
            series: [{
    <%

        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Agua")) {
    %>

            name: '<%=res.getString("nombre")%>',
    <%
            }
        }
    %>
            data: [

    <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Masa")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>

            ]}]
    });
    });

</script>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Valoracion en Kilogramos',
            x: -20 //center
        },
        subtitle: {
            text: 'Historico',
            x: -20
        },
        xAxis: {
            categories: [
                
                <%
       res.beforeFirst();        
        while (res.next()) {
    %>
            '<%=res.getString("fecha_valoracion")%>',
    <%}%>
                
            ]
        },
        yAxis: {
            title: {
                text: 'Kilogramos (KG)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'Kg'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
           name: 'Masa',
            data: [
                
        <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Masa")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
            
    ]
        }, {
            name: 'Masa M',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Masa M")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        }, {
            name: 'Masa O',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Masa O")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        }, {
            name: 'Grasa',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Grasa")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        }]
    });
});
		
</script>
<script type="text/javascript">
$(function () {
    $('#fisico').highcharts({
        title: {
            text: 'Valoracion en Centimetros',
            x: -20 //center
        },
        subtitle: {
            text: 'Historico',
            x: -20
        },
        xAxis: {
            categories: [
                
                <%
       res.beforeFirst();        
        while (res.next()) {
    %>
            '<%=res.getString("fecha_valoracion")%>',
    <%}%>
                
            ]
        },
        yAxis: {
            title: {
                text: 'Centimetros (CM)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'Cm'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
           name: 'Espalda',
            data: [
                
        <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Espalda")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
            
    ]
        }, {
            name: 'Cintura',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Cintura")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        }, {
            name: 'Cadera',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Cadera")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        }, {
            name: 'Pierna',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Pierna")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        },{
            name: 'Gemelos',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Gemelos")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        },{
            name: 'Biceps',
            data: [
                
                 <%
        res.beforeFirst();
        while (res.next()) {
            if (res.getString("nombre").equals("Biceps")) {
    %>
    <%=res.getString("resultado_item")%>,
    <%}
        }%>
                
            ]
        }]
    });
});
		
</script>

<%        } else {
        response.sendRedirect("../../index.jsp");
    }
%>
