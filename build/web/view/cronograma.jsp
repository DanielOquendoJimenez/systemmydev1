<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrCronograma"%>
<%@page import="controller.ctrPersona"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : cronograma
    Created on : 05-nov-2015, 10:45:09
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="header.jsp"/>
<div class="container">
    <!--<header class="page-header">
        <h3>Photo Gallery <small>JQuery lightGallery is a lightweight jQuery lightbox gallery for displaying image and video gallery</small></h3>
    </header>-->
    <div class="tile">
        <div class="action-header clearfix">
            <h2 class="ah-label hidden-xs">Cronograma Ser Athletic</h2>

            <ul class="ah-actions actions">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="true">
                        <i class="zmdi zmdi-sort"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#" id="UpdateTable">Actualizar</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a data-toggle="modal" href="#addModal">
                        <i class="zmdi zmdi-plus-circle-o"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div id="dataTable"></div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formScheduleAdd" name="formScheduleAdd">
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva Clase</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <select data-live-search="true" class="selectpicker" name="slcScheduleAddClass" id="slcScheduleAddClass" required>
                                    <%
                                        ctrCronograma cron = new ctrCronograma();
                                        ResultSet res = cron.listClass();
                                        while (res.next()) {
                                    %>
                                    <option value="<%=res.getString("idclase")%>"><%=res.getString("nombre")%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <select data-live-search="true" class="selectpicker" name="slcScheduleAddDay" id="slcScheduleAddDay" required>
                                    <%
                                        ResultSet day = cron.listDay();
                                        while (day.next()) {
                                    %>
                                    <option value="<%=day.getString("iddia")%>"><%=day.getString("nombre")%></option>
                                    <%}%>
                                </select>
                            </div>                      
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <select data-live-search="true" class="selectpicker" name="slcScheduleAddHorary" id="slcScheduleAddHorary" required>
                                    <%
                                        ResultSet horary = cron.listarHi();
                                        while (horary.next()) {
                                    %>
                                    <option value="<%=horary.getString("idhorario")%>"><%=horary.getString("hora_inicio")%> a <%=horary.getString("hora_fin")%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <select data-live-search="true" class="selectpicker" name="slcScheduleAddInstructor" id="slcScheduleAddInstructor" required>
                                    <%
                                        ctrPersona person = new ctrPersona();
                                        ResultSet people = person.consultarP();
                                        while (people.next()) {
                                            if (people.getString("idTipo_persona").equals("2")) {
                                    %>
                                    <option value="<%=people.getString("idpersona")%>"><%=people.getString("primer_nombre")%>  <%=people.getString("primer_apellido")%></option>
                                    <%}
                                        }%>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="saveSchedule" name="saveSchedule" class="btn btn-sm btn-primary">Guardar</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>             
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"/>
<script>
    function optenerDia(dia) {
        var diaC = $("tr th")[dia].innerHTML;
        //var hotario = $("td#hora")[0].innerHTML;
        $("#diaV").val(diaC);
        //$("#horario").val(hotario);
        $('#addNew-event').modal({
            show: 'false'
        });
    }
    function editSchedule(id, idDay, idClass, idper) {
        $("#txtIdSchedule").val(id);
        $('#editModal').modal('show');
    }
    function deleteSchedule(id) {
        var idCronograma = id;
        swal({
            title: "estas seguro?",
            text: "Esto no es reversible!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {idCronograma: idCronograma},
                    url: "../ctrCronograma"
                }).done(function (tabla) {
                    if (tabla !== null) {
                        swal("Eliminado!", "tu registro a sido eliminado.", "success");
                        $("#dataTable").html(tabla);
                    }
                }).fail(function () {

                });
                return false;
            } else {
                swal("Cancelado", "cancelado por el usuario", "error");
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        var request = 'load';
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {request: request},
            url: "../ctrCronograma"
        }).done(function (data) {
            $("#dataTable").html(data);

        }).fail(function () {
        });
        $("#formScheduleAdd").submit(function () {
            var idDia = $("#slcScheduleAddDay").val();
            var idHorario = $("#slcScheduleAddHorary").val();
            var idClase = $("#slcScheduleAddClass").val();
            var idInstructor = $("#slcScheduleAddInstructor").val();
            $("#saveSchedule").attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idDia: idDia, idHorario: idHorario, idClase: idClase, idInstructor: idInstructor},
                url: "../ctrCronograma"
            }).done(function (data) {
                if (data !== null) {
                    $("#saveSchedule").attr("disabled", false);
                    $('#addModal').modal('hide');
                    $("#dataTable").html(data);
                    swal("¡Buen trabajo!", "Guardado correctamente", "success");
                } else {

                }
            }).fail(function () {

            });
            return false;
        });
    });
</script>

<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>



