<%-- 
    Document   : registrarAlimento
    Created on : 06-ene-2016, 15:30:07
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar horario</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-wrench m-r-5"></i> ALIMENTO</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrItems">
                                <dl class="dl-horizontal">
                                    <dt>Nombre del alimento</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_nombreHorario" name="txt_nombreAli" />
                                    </dd>
                                </dl>
                                 <dl class="dl-horizontal">
                                     <dt>Descripcion del alimento</dt>
                                    <dd>
                                        <textarea type="text" class="form-control input-mask" id="txt_nombreHorario" name="txt_descripcionAli"></textarea>
                                    </dd>
                                </dl>
                                <div class="m-t-30" id="ver1">
                                    <input type="submit" class="btn btn-success" value="Guardar" id="enviar-btn" name="guardarA" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<jsp:include page= "footer.jsp"/>