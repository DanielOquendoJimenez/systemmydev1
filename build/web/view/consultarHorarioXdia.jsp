<%-- 
    Document   : consultarHorarioXdia
    Created on : 01-mar-2016, 19:23:44
    Author     : YEISSON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.mdlHorarioXdia"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrHorarioXdia"%>

<jsp:include page="header.jsp"/>
<%-- 
    MODIFICAR UN HORARIO
--%>
<%

    String modificar = request.getParameter("accion");

    if (modificar != null) {
        mdlHorarioXdia mod = new mdlHorarioXdia();
        mod.setIdHorario(Integer.parseInt(modificar));
        ResultSet res = mod.listarHorarioId();
        if (res.next()) {

%>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Modificar Horario <div class="pull-right"><a href="consultarHerramientas.jsp" style="color: black">X</a></div></div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i> Horario</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrHorarioXdia">
                                <dl class="dl-horizontal">
                                    <dt>Nombre del horario</dt>
                                    <dd>
                                        <input type="hidden" hidden class="form-control input-mask" id="txt_idHorario" name="txt_idHorario" value="<%=res.getString("idhorarioxdia")%>" />
                                        <input type="text" class="form-control input-mask" id="txt_nombre" name="txt_nombre" value="<%=res.getString("nombre")%>" />
                                    </dd>
                                </dl>
                              
                               

                                <div class="m-t-30" id="ver1">
                                    <input type="submit" class="btn btn-success" value="Modificar" id="enviar-btn" name="btnModificar" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<%}%>
<%}%>
<%-- 
    FIN_________MODIFICAR HORARIO
--%>

<!--
LISTO EN UNA TABLA TODOS HORARIOS
-->

<h3 class="text-center">HORARIOS <small></small></h3>
<table id="myTable" class="table table-bordered table-vmiddle">
    <thead>
        <tr>
            <th>Nombre</th>
           
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
        <%

            ctrHorarioXdia ch = new ctrHorarioXdia();
            ResultSet her = ch.consultarHerramientas();

            while (her.next()) {
        %>
        <tr>
            <td><%=her.getString("nombre")%></td>
           

            <td>
                <%
                    if (her.getString("estado").equals("1")) {
                %>
                <button class="btn btn-success" onclick="estadop(0,<%=her.getString("idhorarioxdia")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                    <%} else {%>
                <button class="btn btn-danger" onclick="estadop(1,<%=her.getString("idhorarioxdia")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                    <%}%>
                <a href="consultarHorarioXdia.jsp?accion=<%=her.getString("idhorarioxdia")%>" class="btn btn-primary"><i class="zmdi zmdi-edit"></i></a>
            </td>
        </tr>  
        <%}%>
    </tbody>
</table>

<!--
FIN DEL LISTADO DE LAS HERRAMIENTAS
-->
<jsp:include page="footer.jsp"/>
<script>
    //CAMBIAR ESTADO DE LA HERRAMIENTA 
    function estadop(estado, id) {
        swal({
            title: "estas seguro?",
            text: "Esto no es reversible!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Cambiar!",
            cancelButtonText: "No, Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            // swal("Eliminado!", "tu registro a sido cambiado.", "success");
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {estado: estado, id: id},
                    url: "../ctrHorarioXdia"

                }).done(function (data) {

                    if (data) {


                        swal("ESTADO", "Su registro ha cambiado de estado", "info");
                        setTimeout("location.reload()", 1000);

                    }
                }).fail(function () {

                });
                return true;

            } else {
                swal("Cancelado", "cancelado por el usuario :)", "error");
            }
        });



    }


    //FIN CAMBIO DE ESTADO DE LA HERRAMIENTA
</script>
