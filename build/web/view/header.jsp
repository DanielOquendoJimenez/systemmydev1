
<%
    HttpSession ht = request.getSession();
%>
<%-- 
Document   : headerCliente
Created on : 25-nov-2015, 23:03:57
Author     : daniel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="model.mdlPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            if (ht.getAttribute("roll").equals("2")) {
        %>
        <title>Instructor Ser Athletic</title>
        <%} else {%>
        <title>Administrador Ser Athletic</title>
        <%}%>

        <link rel="shortcut icon" href="img/favicon.ico">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <meta name="format-detection" content="telephone=no">

        <meta charset="UTF-8">

        <meta name="description" content="SuperFlat Responsive Admin Template">

        <meta name="keywords" content="SuperFlat Admin, Admin, Template, Bootstrap">

        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">

        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

        <link href="vendors/bower_components/jquery.bootgrid/dist/jquery.bootgrid.override.min.css" rel="stylesheet">

        <link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">

        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

        <link href="vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">

        <link href="vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">

        <link href="vendors/farbtastic/farbtastic.css" rel="stylesheet">

        <link href="vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

        <link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">

        <link href="vendors/bower_components/lightgallery/src/css/lightgallery.css" rel="stylesheet">

        <link href="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>

        <link href="css/app.min.css" rel="stylesheet">

        <link href="css/demo.css" rel="stylesheet">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>

        <style type="text/css"> 
            .toggle-switch .ts-label {
                min-width: 130px;
            }
        </style>
    </head>
    <body>

        <header id="header" class="clearfix" data-spy="affix" data-offset-top="65">
            <ul class="header-inner">

                <!-- Logo -->
                <li class="logo">
                    <a href="inicio.jsp"><img src="img/logo.png" alt=""></a>
                    <div id="menu-trigger"><i class="zmdi zmdi-menu"></i></div>
                </li>

                <!-- Settings -->
                <li class="pull-right dropdown hidden-xs">
                    <a href="#" data-toggle="dropdown">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="fullscreen" href="#">Toggle Fullscreen</a></li>
                        <li><a data-toggle="localStorage" href="#">Clear Local Storage</a></li>
                        <li><a href="#">Account Settings</a></li>
                        <li><a href="#">Other Settings</a></li>
                    </ul>
                </li>

                <!-- Quick Apps -->
                <li class="hidden-xs dropdown pull-right">
                    <a href="#" data-toggle="dropdown">
                        <i class="zmdi zmdi-apps"></i>    
                    </a>

                    <div class="dropdown-menu pull-right" id="launch-apps">
                        <div class="dropdown-header bg-teal">Opciones</div>

                        <div class="la-body">
                            <div class="lab-item">
                                <a href="cambiarDatos.jsp" class="bg-red">
                                    <i class="zmdi zmdi-account"></i>
                                </a>
                                <small>Actualizar</small>
                            </div>

                            <div class="lab-item">
                                <a href="#" class="bg-green">
                                    <i class="zmdi zmdi-calendar"></i>
                                </a>
                                <small>Entrenamiento</small>
                            </div>

                            <div class="lab-item">
                                <a href="#" class="bg-blue">
                                    <i class="zmdi zmdi-favorite"></i>
                                </a>
                                <small>Valoracion</small>
                            </div>

                            <div class="lab-item">
                                <a href="#" class="bg-orange">
                                    <i class="zmdi zmdi-male"></i>
                                </a>
                                <small>Nutricional</small>
                            </div>

                            <div class="lab-item">
                                <a href="#" class="bg-cyan">
                                    <i class="zmdi zmdi-view-headline"></i>
                                </a>
                                <small>News</small>
                            </div>

                            <div class="lab-item">
                                <a href="#" class="bg-teal">
                                    <i class="zmdi zmdi-image"></i>
                                </a>
                                <small>Gallery</small>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- Time -->
                <li class="pull-right hidden-xs">
                    <div id="time">
                        <span id="t-hours"></span>
                        <span id="t-min"></span>
                        <span id="t-sec"></span>
                    </div>
                </li>
            </ul>
        </header>  

        <aside id="sidebar"> 

            <!--| MAIN MENU |-->
            <ul class="side-menu">
                <li class="sm-sub sms-profile">
                    <%
                        String idPersona = String.valueOf(ht.getAttribute("idpersona"));
                        if (idPersona != null) {
                            mdlPersona md = new mdlPersona();
                            md.setIdpersona(Integer.parseInt(idPersona));
                            ResultSet res = md.consultarPm();
                            if (res.next()) {

                    %>
                    <a class="clearfix" href="#">
                        <img src="../upload/<%=res.getString("foto")%>" alt="">

                        <span class="f-11">
                            <span class="d-block"><%=res.getString("primer_nombre")%></span>
                            <small class="text-lowercase"><%=res.getString("primer_apellido")%> <%=res.getString("segundoApellido")%></small>
                        </span>
                    </a>
                    <%}
                        }
                    %>
                    <ul>
                        <li><a href="DatosModificados.jsp">Modificar Datos</a></li> 
                        <li><a href="../index.jsp?estado=1">Cerrar Sesión</a></li>
                    </ul>
                </li>

                <li>
                    <a href="inicio.jsp">
                        <i class="zmdi zmdi-home"></i>
                        <span>Inicio</span>
                    </a>
                </li>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-account-o"></i>
                        <span>Personas</span>
                    </a>
                    <ul>
                        <li><a href="registrarPersona.jsp">Registrar Persona</a></li>
                        <li><a href="consultarPersona.jsp">Listar Personas</a></li>
                        <li><a href="consultaDetalladaP.jsp">Consultar</a></li>
                    </ul>
                </li>
                <%
                    if (ht.getAttribute("roll").equals("2")) {
                %>
                <%} else {%>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-assignment-o"></i>
                        <span>Plan</span>
                    </a>
                    <ul>
                        <li><a href="plan.jsp">Registrar Plan</a></li>
                        <li><a href="consultarPlan.jsp">Ver Planes</a></li>
                        <li><a href="registrarPromocion.jsp">Promociones</a></li>
                    </ul>
                </li>
                <%}%>               
                <%
                    if (ht.getAttribute("roll").equals("2")) {
                %>

                <%} else {%>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi zmdi-calendar-alt"></i>
                        <span>Cronograma</span>
                    </a>
                    <ul>
                        <li><a href="cronograma.jsp">Ver Cronograma</a></li>
                    </ul>
                </li>
                <%}%>

                <%
                    if (ht.getAttribute("roll").equals("2")) {
                %>
                <%} else {%>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-star-circle"></i>
                        <span>Elementos deportivos</span>
                    </a>
                    <ul>
                        <li><a href="registrarHerramienta.jsp">Registrar elemento</a></li>
                        <li><a href="consultarHerramientas.jsp">Consultar elementos</a></li>
                        <li><a href="registrarMantenimiento.jsp">Registrar Mantenimiento</a></li>
                    </ul>
                </li>
                <%}%>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-swap-alt"></i>
                        <span>Plan de Entrenamiento</span>
                    </a>
                    <ul>
                        <li><a href="planEntrenamiento.jsp">registrar plan de entrenamiento</a></li>
                        <!--<li><a href="animations.html">Animations</a></li>-->
                    </ul>
                </li>
                <li>
                    <a href="valoracion.jsp">
                        <i class="zmdi zmdi-trending-up"></i>
                        <span>valoración</span>
                    </a>
                </li>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-run"></i>
                        <span>Plan Nutricional</span>
                    </a>
                    <ul>
                        <li><a href="registrarPlanNutricional.jsp">Registrar Plan Nutricional</a></li>
                    </ul>
                </li>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-assignment-check"></i>
                        <span>Preguntas</span>
                    </a>
                    <ul>
                        <li><a href="preguntas.jsp">Registrar Preguntas</a></li>
                    </ul>
                </li>
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-info"></i>
                        <span>Encuesta</span>
                    </a>
                    <ul>
                        <li><a href="registrarEncuesta.jsp">Registrar Encuesta</a></li>
                        <li><a href="consultarEncuesta.jsp">Consultar Encuesta</a></li>
                    </ul>
                </li>
                <!--<li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-info"></i>
                        <span>items</span>
                    </a>
                    <ul>
                        <li><a href="registrarHorarioXdia.jsp">Horario del día</a></li>
                        <li><a href="consultarHorarioXdia.jsp">Consultar del día</a></li>
                        <li><a href="registrarAlimento.jsp">Registrar Alimento</a></li>
                    </ul>
                </li>-->
                <li class="sm-sub">
                    <a href="#">
                        <i class="zmdi zmdi-info"></i>
                        <span>Productos</span>
                    </a>
                    <ul>
                        <li><a href="registrarProducto.jsp">Registrar Producto</a></li>
                        <li><a href="consultarProducto.jsp">Consultar Producto</a></li>
                    </ul>
                </li>
                 <li class="sm-sub">
                    <a href="#">

                        <span></span>
                    </a>
                    <ul>
                        <li><a href="registrarProducto.jsp">Registrar Producto</a></li>
                        <li><a href="consultarProducto.jsp">Consultar Producto</a></li>
                    </ul>
                </li>
                <li class="sm-sub">
            </ul>
        </aside>

        <section id="content">