
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
Document   : plan
Created on : 05-nov-2015, 11:29:53
Author     : daniel
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPlan"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar Plan</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i> Planes</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="">
                                <dl class="dl-horizontal">
                                    <dt>Nombre del Plan</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_nombre" name="txt_nombre" /><span id="mensaje1" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Valor</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" data-mask="00000000000000000" id="txt_valor" name="txt_valor"/><span id="mensaje2" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descuento</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" data-mask="000"  id="txt_descuento" name="txt_descuento" /><span id="mensaje3" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Días</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" data-mask="000" id="txt_dias" name="txt_dias" /><span id="mensaje4" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Tipo de Plan</dt>
                                    <dd>
                                        <select type="text" class="selectpicker" data-live-search="true" id="txt_idtipo_plan" name="txt_idtipo_plan" >
                                            <option></option>
                                            <%
                                                ctrPlan tp = new ctrPlan();
                                                ResultSet rt = tp.listarTipoPersona();
                                                while (rt.next()) {
                                            %>          
                                            <option value="<%=rt.getString("idtipo_plan")%>"><%=rt.getString("nombreTp")%></option>
                                            <%}%>
                                        </select><span id="mensaje5" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>   
                                <dl class="dl-horizontal">
                                    <dt>Promociones</dt>
                                    <dd>
                                        <div class="input-group">
                                            <select type="text" class="form-control" id="slcPromocion" disabled="disabled">
                                                <option></option> 
                                                <%
                                                    ResultSet pro = tp.listarPromcion();
                                                    while (pro.next()) {
                                                %>
                                                <option value="<%=pro.getString("idpromociones")%>"><%=pro.getString("nombre")%></option> 
                                                <%}%>
                                            </select>
                                            <input type="text" hidden value="id" id="idPlan"> 
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary"type="button" disabled id="btnAgregar">+</button>
                                            </span>
                                        </div>
                                        <br>
                                        <div id="lista"></div>
                                    </dd>
                                </dl> 

                                <div class="row">
                                    <div class="col-md-4"> 
                                        <div class="m-t-30"> 
                                            <div id="divAgregarPromocion2">
                                                <input type="submit" class="btn btn-success" value="Guardar Plan" id="enviar-btn" name="enviar-btn" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="m-t-30">
                                            <a href="consultarPlan.jsp">
                                                <button type="button" class="btn btn-info">Consultar plan</button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="m-t-30" id="ver1">
                                            <div id="divAgregarPromocion"> 
                                                <input type="submit" class="btn btn-primary" value="Agregar Promiciones" id="agregarPromocion" name="enviar-btn" />
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="promocion"></div>
    </div>
</div> 

<jsp:include page="footer.jsp"/>
<script type="text/javascript">

    $(document).ready(function () {

        $("#enviar-btn").click(function () {

            //Obtenemos el valor del campo nombre
            var nombre = $("input#txt_nombre").val();
            var valor = $("input#txt_valor").val();
            var descuento = $("input#txt_descuento").val();
            var dias = $("input#txt_dias").val();
            var tipoPlan = $("select#txt_idtipo_plan").val();

            //Validamos el campo nombre, simplemente miramos que no esté vacío
            if (nombre == "") {
                $("label#name_error").show();
                $("input#name").focus();
                return false;
            }

            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {nombre: nombre, valor: valor, descuento: descuento, dias: dias, tipoPlan: tipoPlan},
                url: '../ctrPlan'
            }).done(function (s) {
                if (s !== null) {
                    function notify(from, align, icon, type, animIn, animOut, title, message) {
                        $.growl({
                            icon: icon,
                            title: title || 'Bootstrap Growl ',
                            message: message || 'Turning standard Bootstrap alerts into awesome notifications',
                            url: ''
                        }, {
                            element: 'body',
                            type: type,
                            allow_dismiss: true,
                            placement: {
                                from: from,
                                align: align
                            },
                            offset: {
                                x: 20,
                                y: 85
                            },
                            spacing: 10,
                            z_index: 1031, delay: 2500,
                            timer: 1000,
                            url_target: '_blank',
                            mouse_over: false,
                            animate: {
                                enter: animIn,
                                exit: animOut
                            },
                            icon_type: 'class',
                            template: '<div data-growl="container" class="alert" role="alert">' +
                                    '<button type="button" class="close" data-growl="dismiss">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                    '<span class="sr-only">Close</span>' +
                                    '</button>' +
                                    '<span data-growl="icon"></span>' +
                                    '<span data-growl="title"></span>' +
                                    '<span data-growl="message"></span>' +
                                    '<a href="#" data-growl="url"></a>' +
                                    '</div>'
                        });
                    }
                    ;

                    notify('bottom', 'right', '', 'success', 'animated bounceIn', 'animated bounceIn', 'Guardado ', 'Corectamente');
                    //setTimeout("location.reload()", 3000);
                    $("input#txt_nombre").val("");
                    $("input#txt_valor").val("");
                    $("input#txt_descuento").val("");
                    $("input#txt_dias").val("");
                    $("select#txt_idtipo_plan").val("");
                    $("input#txt_nombre").focus();
                } else {
                    notify('bottom', 'right', '', 'danger', 'animated bounceIn', 'animated bounceIn', 'Error ', 'Registro No Guardado');
                }

            }).fail(function () {
                alert("");
            });
            return false;
        });
    });
</script>

<script>

    $("#agregarPromocion").click(function () {
        var nombre = $("#txt_nombre").val();
        var valor = $("#txt_valor").val();
        var dec = $("#txt_descuento").val();
        var dias = $("#txt_dias").val();
        var tPlan = $("#txt_idtipo_plan").val();

        if (nombre == "" || valor == "" || dec == "" || dias == "" || tPlan == "") {
            if (nombre == "") {
                $("#mensaje1").show();

            }
            if (valor == "") {
                $("#mensaje2").show();
            }
            if (dec == "") {
                $("#mensaje3").show();
            }
            if (dias == "") {
                $("#mensaje4").show();
            }
            if (tPlan == "") {
                $("#mensaje5").show();
            }
        } else {
            var nombre = $("#txt_nombre").attr("disabled", true).val();
            var valor = $("#txt_valor").attr("disabled", true).val();
            var descuento = $("#txt_descuento").attr("disabled", true).val();
            var dias = $("#txt_dias").attr("disabled", true).val();
            var idTipoPlan = $("#txt_idtipo_plan").attr("disabled", true).val();
            $("#enviar-btn").attr("disabled", true);
            $("#slcPromocion").attr("disabled", false);
            $("#btnAgregar").attr("disabled", false);
            if (nombre !== null) {
                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {nombre: nombre, valor: valor, descuento: descuento, dias: dias, tipoPlan: idTipoPlan},
                    url: "../ctrPlan"
                }).done(function (data) {
                    if (data !== null) {
                        $("#idPlan").val(data);
                        $("#slcPromocion").attr("disabled", false);
                        $("#btnAgregar").attr("disabled", false);
                    }
                }).fail(function () {

                });
            }
        }

        return false;
    });

    $("#btnAgregar").click(function () {
        var idPlan = $("#idPlan").val();
        var idPromocion = $("#slcPromocion").val();

        if (idPlan !== "") {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idPlan: idPlan, idPromocion: idPromocion},
                url: "../ctrPlan"
            }).done(function (data) {
                if (data !== null) {
                    //notify('bottom', 'right', '', 'success', 'animated bounceIn', 'animated bounceIn', 'Guardado ', 'Corectamente');
                    //setTimeout("location.reload()", 3000);
                    $("#lista").html(data);
                    $("#divAgregarPromocion2").show();
                     $("#btnAgregar").attr("disabled", false);
                    $("#divAgregarPromocion").hide();

                }
            }).fail(function () {

            });
        }
    });


    $("#txt_nombre").keyup(function () {
        $("#mensaje1").hide();
    });
    $("#txt_valor").keyup(function () {
        $("#mensaje2").hide();
    });
    $("#txt_descuento").keyup(function () {
        $("#mensaje3").hide();
    });
    $("#txt_dias").keyup(function () {
        $("#mensaje4").hide();
    });
    $("#txt_idtipo_plan").change(function () {
        $("#mensaje5").hide();
    });
    $("#agregarPromocion2").click(function () {
        swal({
            title: "Guardado",
            text: "Correctamente.",
            timer: 2000,
            showConfirmButton: false
        });
        setTimeout("location.reload()", 3000);
        //return false;
    });

</script>


<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>