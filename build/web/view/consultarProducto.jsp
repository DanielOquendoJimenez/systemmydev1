<%-- 
    Document   : consultarProducto
    Created on : 21-feb-2016, 16:20:53
    Author     : YEISSON
--%>
<%@page import="model.mdlProducto"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrProducto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="header.jsp"/>
<%-- 
    MODIFICAR UN PRODUCTO
--%>
<%
    String idProduto = request.getParameter("u");
    if (idProduto != null) {
        mdlProducto md = new mdlProducto();
        md.setIdProducto(Integer.parseInt(idProduto));
        ResultSet res = md.consultarPm();
        if (res.next()) {
%> 

<div class="panel panel-primary" id="form">
    <div class="panel-heading">
        <h3 class="panel-title text-right"><a href="consultarProducto.jsp">X</a></h3>
        <h3 class="panel-title text-center">Modificar</h3>
    </div>
    <div class="panel-body">
        <form action="../ctrProducto" method="post" data-parsley-validate>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >nombre </label>
                            <input type="hidden" name="txtId"  value="<%=res.getString("idproductos")%>">
                            <input type="text" id="txt_Nombre" name="txt_Nombre" class="form-control" placeholder="nombre"  value="<%=res.getString("nombre")%>"><span  id="mensaje1" hidden>Complete este campo</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Valor</label>
                            <input type="text" id="txt_valor" name="txt_valor" class="form-control" placeholder=" Valor" value="<%=res.getString("valor")%>"><span  id="mensaje2" hidden>Complete este campo</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label >Descripción </label>
                            <input type="text" id="txt_descripcion" name="txt_descripcion" class="form-control" placeholder="Descripción"  value="<%=res.getString("descripcion")%>"><span  id="mensaje3" hidden>Complete este campo</span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-md-4">
                <button type="submit" class="btn btn-primary" id="btnGuardar2" name="btnModificar">Modificar</button>
            </div>  
            <div class="col-md-4">

            </div>
            <div class="col-md-4">

            </div>
        </form>
    </div>
</div>
<%  }
    }
%>
<%-- 
    FIN__________MODIFICAR UN PRODUCTO
--%>


<%-- 
    LISTAR PRODUCTOS
--%>

<h3 class="text-center">PRODUCTOS <small></small></h3>

<div class="table-responsive">
    <table id="myTable" class="table table-bordered table-vmiddle">
        <thead>
            <tr>
                <td  data-column-id="nombre"> nombre</td>
                <td  data-column-id="Valor">Valor </td>
                <td  data-column-id="Primer apellido">Descripción </td>
                <td  data-column-id="Accion" data-formatter="commands" data-sortable="false">Accion</td>

            </tr>
        </thead>
        <tbody>
            <%
                ctrProducto per = new ctrProducto();
                ResultSet rs = per.consultarP();
                while (rs.next()) {
            %>
            <tr>
                <td><%=rs.getString("nombre")%></td>
                <td><%=rs.getString("valor")%></td>
                <td><%=rs.getString("descripcion")%></td> 
                <td>
                    <%
                        if (rs.getString("estado").equals("1")) {
                    %>
                    <button class="btn btn-success" onclick="estadop(0,<%=rs.getString("idproductos")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                        <%} else {%>
                    <button class="btn btn-danger" onclick="estadop(1,<%=rs.getString("idproductos")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                        <%}%>
                    <a href="consultarProducto.jsp?u=<%=rs.getString("idproductos")%>" class="btn btn-primary"><i class="zmdi zmdi-edit"></i></a>
                </td>
            </tr>
            <%}%>
        </tbody>
    </table>
</div>
<%-- 
  FIN___________LISTAR PRODUCTOS
--%>


<jsp:include page="footer.jsp"/>

<%-- 
  CAMBIAR ESTADO DE UN PRODUCTO
--%>


<script>
    function estadop(estado, id) {
        swal({
            title: "estas seguro?",
            text: "Esto no es reversible!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Cambiar!",
            cancelButtonText: "No, Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
           
            if (isConfirm) {

                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {estado: estado, id: id},
                    url: "../ctrProducto"

                }).done(function (data) {

                    if (data) {


                        swal("ESTADO", "Su registro ha cambiado de estado", "info");
                        setTimeout("location.reload()", 1000);

                    }
                }).fail(function () {

                });
                return true;

            } else {
                swal("Cancelado", "cancelado por el usuario :)", "error");
            }
        });



    }

</script>
<%-- 
  FIN__________ CAMBIAR ESTADO DE UN PRODUCTO
--%>


<script type="text/javascript">
    $(document).ready(function () {    
            $("#btnGuardar2").click(function () {
           
            var nom = $("#txt_Nombre").val();
            var valor = $("#txt_valor").val();
            var des = $("#txt_descripcion").val();
            if ( nom == "" || valor == "" || des == "") {               
                if (nom == "") {
                    $("#mensaje1").show();

                } else {
                    $("#mensaje1").hide();
                }
                if (valor == "") {
                    $("#mensaje2").show();
                } else {
                    $("#mensaje2").hide();
                } 
                  if (des == "") {
                    $("#mensaje3").show();
                } else {
                    $("#mensaje3").hide();
                } 
                return false;              
           }
        });

    });
</script>



