
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : consultarPersona
    Created on : 02-nov-2015, 10:35:29
    Author     : daniel
--%>


<%@page import="model.mdlPersona"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<%
    String idPersona = request.getParameter("u");
    if (idPersona != null) {
        mdlPersona md = new mdlPersona();
        md.setIdpersona(Integer.parseInt(idPersona));
        ResultSet res = md.consultarPm();
        if (res.next()) {
%> 

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title text-right"><a href="consultarPersona.jsp">X</a></h3>
        <h3 class="panel-title text-center">Modificar</h3>
    </div>
    <div class="panel-body">
        <form action="../ctrPersona" method="post" data-parsley-validate>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Primer nombre </label>
                            <input type="hidden" name="txtIdpersonaM"  value="<%=res.getString("idpersona")%>">
                            <input type="hidden" name="txtRollM"  value="<%=ht.getAttribute("roll")%>">
                            <input type="text" name="txtPrimerNombreM" class="form-control" placeholder="Primer nombre" required value="<%=res.getString("primer_nombre")%>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Segundo nombre</label>
                            <input type="text" name="txtSegundoNombreM" class="form-control" placeholder="Segundo nombre" value="<%=res.getString("segundo_nombre")%>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Primer apellido </label>
                            <input type="text" name="txtPrimerApellidoM" class="form-control" placeholder="Primer apellido" required value="<%=res.getString("primer_apellido")%>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Segundo apellido</label>
                            <input type="text" name="txtSegundoApellidoM" class="form-control"  placeholder="Segundo apellido" required value="<%=res.getString("segundoApellido")%>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Celular</label>
                            <input type="number" name="txtCelularM" class="form-control"  placeholder="Celular" required value="<%=res.getString("Celular")%>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Documento de identidad</label>
                            <input type="number" name="txtDocumentoM" class="form-control"  placeholder="Documento de identidad" required value="<%=res.getString("documento_identidad")%>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Correo</label>
                            <input type="email" name="txtCorreoM" class="form-control"  placeholder="Correo" required="@" value="<%=res.getString("correo")%>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Fecha de nacimiento</label>
                            <div class="dtp-container dropdown fg-line">
                                <input type='text' class="form-control date-picker" data-toggle="dropdown" name="txtFechaM" required value="<%=res.getString("fecha_nacimiento")%>" placeholder="Fecha Nacimiento">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Profesión</label>
                            <input type="text" name="txtProfecionM" class="form-control" placeholder="Profesión" required value="<%=res.getString("nspecialidad")%>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">telefono</label>
                            <input type="number" name="txtTelefonoM" class="form-control"  placeholder="telefono" required value="<%=res.getString("telefono")%>">
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary" name="btnModificar">Modificar</button>
            </div>  
            <div class="col-md-4">

            </div>
            <div class="col-md-4">

            </div>
        </form>
    </div>
</div>
<%  }
    }
%>
<br>
<div class="table-responsive">
    <table id="myTable" class="table table-bordered table-vmiddle">
        <thead>
            <tr>
                <td  data-column-id="Primer nombre">Primer nombre</td>
                <td  data-column-id="Segundo nombre">Segundo nombre</td>
                <td  data-column-id="Primer apellido">Primer apellido</td>
                <td  data-column-id="Segundo apellido">Segundo apellido</td>
                <td  data-column-id="Fecha de nacimiento" data-order="desc">Fecha de nacimiento</td>
                <td  data-column-id="Correo">Correo</td>
                <td  data-column-id="Telefono">Telefono</td>
                <td  data-column-id="Celular">Celular</td>
                <td  data-column-id="Documento">Documento</td>
                <td  data-column-id="Profecion">Profecion</td>
                <td  data-column-id="Tipo de persona">Tipo de persona</td>
                <td  data-column-id="sender">Estado</td>
                <td  data-column-id="Accion" data-formatter="commands" data-sortable="false">Accion</td>
            </tr>
        </thead>
        <tbody>
            <%
                ctrPersona per = new ctrPersona();
                ResultSet rs = per.consultarP();
                while (rs.next()) {
            %>
            <tr>
                <td><%=rs.getString("primer_nombre")%></td>
                <td><%=rs.getString("segundo_nombre")%></td>
                <td><%=rs.getString("primer_apellido")%></td>
                <td><%=rs.getString("segundoApellido")%></td>
                <td><%=rs.getString("fecha_nacimiento")%></td>
                <td><%=rs.getString("correo")%></td>
                <td><%=rs.getString("telefono")%></td>
                <td><%=rs.getString("Celular")%></td>
                <td><%=rs.getString("documento_identidad")%></td>
                <td><%=rs.getString("nspecialidad")%></td>
                <td>
                    <%
                        if (rs.getString("idTipo_persona").equals("1")) {
                    %>
                    Administrador
                    <%} else if (rs.getString("idTipo_persona").equals("2")) {%>
                    Instructor
                    <%} else if (rs.getString("idTipo_persona").equals("3")) {%>
                    Cliente
                    <%} else if (rs.getString("idTipo_persona").equals("4")) {%>
                    Invitado
                    <%}%>
                </td>
                <td>
                    <%
                        if (rs.getString("estado").equals("1")) {
                    %>
                    Activo
                    <%} else {%>
                    Inactivo
                    <%}%>
                </td>
                <td>
                    <a href="consultarPersona.jsp?u=<%=(rs.getString("idpersona"))%>" class="btn btn-icon btn-info command-edit"><span class="zmdi zmdi-edit"></span></a>  
                </td>
            </tr>
            <%}%>
        </tbody>
    </table>
</div>
<jsp:include page="footer.jsp"/>
<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>