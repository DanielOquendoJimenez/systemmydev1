<%-- 
    Document   : horarioXdia
    Created on : 05-ene-2016, 15:57:10
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar horario</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-wrench m-r-5"></i> HORARIO DIA</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrItems">
                                <dl class="dl-horizontal">
                                    <dt>Nombre del horario</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_nombreHorario" name="txt_nombreHorario" />
                                    </dd>
                                </dl>
                                <div class="m-t-30" id="ver1">
                                    <input type="submit" class="btn btn-success" value="Guardar" id="enviar-btn" name="guardarH" />
                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
      
    </div>
</div> 




<jsp:include page="footer.jsp"/>


<script>

    function eliminarHorario(id) {
        swal({
            title: "estas seguro?",
            text: "Esto no es reversible!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar!",
            cancelButtonText: "No, Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            swal("Eliminado!", "tu registro a sido eliminado.", "success");
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {id: id},
                    url: "../ctrHorarioXdia"
                }).done(function (tabla) {
                    if (tabla !== null) {
                        $("#tabla").html(tabla);
                    }
                }).fail(function () {

                });
                return false;
            } else {
                swal("Cancelado", "cancelado por el usuario :)", "error");
            }
        });
    }
</script>


