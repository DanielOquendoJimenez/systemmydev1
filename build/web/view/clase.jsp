
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>

<%-- 
    Document   : Clase
    Created on : 18/02/2016, 05:48:58 PM
    Author     : Maychan
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="javax.swing.DefaultComboBoxModel"%>
<%@page import="controller.ctrPlan"%>
<%@page import="controller.ctrCronograma"%>
<%@page import="controller.ctrPersona"%>
<%@page import="model.mdlCronograma"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="header.jsp"/>

<!-- registrar clase-->

<div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <header class="page-header">
            <h3>Clases<small>Aqui podras crear y consultar las clases que se asignaran al cronograma</small></h3>
        </header>

        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center"><h3>Crear Clases</h3></div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h3 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i>Clases</h3>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form method="POST" action="../ctrCronograma">
                                <dl class="dl-horizontal">
                                    <dt>Nombre de la Clase (*)</dt>
                                    <dd>
                                    <input type="text" class="form-control input-mask" id="txt_clase" name="txt_clase" ><span id="mensaje" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descripción de la Clase (*)</dt>
                                    <dd>
                                    <input type="text" class="form-control input-mask" id="txt_descripcionclase" name="txt_descripcionclase" ><span id="mensaje2" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Tipo de Plan (*)</dt>
                                    <dd>
                                        <select class="selectpicker" name="txt_idtipoplan" >
                                            <option></option>
                                            <%
                                                ctrPlan tp = new ctrPlan();
                                                ResultSet rt = tp.listarTipoPersona();
                                                while (rt.next()) {
                                            %>          
                                            <option value="<%=rt.getString("idtipo_plan")%>"><%=rt.getString("nombreTp")%></option>
                                            <%}%>

                                        </select><span id="mensaje3" hidden>Complete este campo</span>
                                    </dd>
                                </dl>                               
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary" value="Crear Clase" id="txt_guardar-btn1" name="txt_guardar-btn1">
                                    <input type="button"  id="btn_verClase" class="btn btn-success" name="btn_verClase" value="Consultar Clase">
                                </div>
                            </form> 
                        </div>
                    </div>                        
                    <div id="tabla"></div>    
                </div>
            </div>
        </div>

        <!-- cierra registrar clase-->

        <!-- Model editar clase-->

        <div class="modal fade bs-example-modal-xs" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Modificar Clase</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="../ctrCronograma">
                            <dl class="dl-horizontal">
                                <dt><center>Clase</center></dt>
                                <dd>
                                    <input type="hidden" class="input-sm form-control" name="txtclase" id="txtclase" >
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Nombre de la Clase (*)</dt>
                                <dd>
                                <input type="text" class="form-control input-mask" id="txtnombre" name="txtnombre"  ><span id="mensaje4" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Descripción de la Clase (*)</dt>
                                <dd>
                                <input type="text" class="form-control input-mask" id="txtdescripcionclase" name="txtdescripcionclase" ><span id="mensaje6" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Tipo de Plan (*)</dt>
                                <dd>
                                    <select class="selectpicker" name="txtidtipoplan" >
                                        <option></option>
                                        <%
                                            ResultSet rtr = tp.listarTipoPersona();
                                            while (rtr.next()) {
                                        %>          
                                        <option value="<%=rtr.getString("idtipo_plan")%>"><%=rtr.getString("nombreTp")%></option>
                                        <%}%>
                                    </select><span id="mensaje7" hidden>Complete este campo</span>
                                </dd>
                            </dl>                               
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary" value="Modificar Clase" id="txtmodifc" name="txtmodific">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

<!-- Cierra Model editar clase-->


<jsp:include page="footer.jsp"/>

<!-- editar clase-->
<script>
    function editarClase(id, nom, des) {
        $(document).on("click", ".open-Modal", function () {
        });
        $("#txtclase").val(id);
        $("#txtnombre").val(nom);
        $("#txtdescripcionclase").val(des);
    }
</script>  
<!-- cierra editar clase-->

<!-- ver clase-->
<script>
    $("#btn_verClase").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {listar: "si"},
            url: "../ctrCronograma"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    });
</script>
<!-- cierra ver clase-->

<!-- estado clase-->
<script>
    function estadoClase(estado, idclase) {

        swal("Cambio de estado", "Exitoso", "success");

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {estado: estado, idclase: idclase},
            url: "../ctrCronograma"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    }
</script>
<!-- cierra estado clase-->

<!-- validacion clase-->

<script>
    $(document).ready(function () {
        $("#txt_guardar-btn1").click(function () {
            var txtnombre = $("#txt_clase").val();
            var descripcion = $("#txt_descripcionclase").val();
            var txtidtipoplan = $("#txtidtipoplan").val();

            if (txtnombre == "" || descripcion == "" || txtidtipoplan == "") {
                if (txtnombre == "") {
                    $("#mensaje").show();
                } else {
                    $("#mensaje").hide();
                }
                if (descripcion == "") {
                    $("#mensaje2").show();
                } else {
                    $("#mensaje2").hide();
                }
                if (txtidtipoplan == "") {
                    $("#mensaje3").show();
                } else {
                    $("#mensaje3").hide();
                }
                return false;
            }
        });

        $("#txtmodifc").click(function () {
            var txtclase = $("#txtnombre").val();
            var txtdescripcion = $("#txtdescripcionclase").val();
            var txtidtipoplan = $("#txtidtipoplan").val();

            if (txtclase == "" || txtdescripcion == "" || txtidtipoplan == "") {
                if (txtclase == "") {
                    $("#mensaje4").show();

                } else {
                    $("#mensaje4").hide();
                }
                if (txtdescripcion == "") {
                    $("#mensaje6").show();
                } else {
                    $("#mensaje6").hide();
                }
                if (txtidtipoplan == "") {
                    $("#mensaje7").show();
                } else {
                    $("#mensaje7").hide();
                }
                return false;
            }
        });
    }
    );
</script>

<!-- cierra validacion clase-->

<%
    } else {
        response.sendRedirect("../index.jsp");
    }
%>
