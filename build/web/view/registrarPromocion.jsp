<%-- 
    Document   : registrarPromocion
    Created on : 11-feb-2016, 19:06:03
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar Promocion</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-flower-alt m-r-5"></i> PRMOCIONES</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form  >
                                <dl class="dl-horizontal">
                                    <dt>Nombre de la Promoción</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_nombrePromocion" name="txt_nombrePromocion"><span id="mensajeE" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Valor en numero de dias</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" data-mask="000"  id="txt_valorPromocion" name="txt_valorPromocion"><span id="mensajeE2" hidden style="color: red">Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descripcion de las condiciones</dt>
                                    <dd>
                                        <textarea type="text" class="form-control input-mask" id="txt_coniciones" name="txt_coniciones"></textarea>
                                    </dd>
                                </dl>
                                <div class="m-t-30" id="ver1">
                                    <button type="submit" class="btn btn-success" id="btn_guardarPromocion" name="btn_guardarPromocion">Guardar</button>
                                </div>
                                <div class="m-t-30 pull-right">
                                    <button  class="btn btn-primary" id="btn_VerPromocion" name="btn_VerPromocion">Ver <i class="zmdi zmdi-eye m-r-5"></i></button>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tabla">

        </div>
    </div>
</div> 


<jsp:include page="footer.jsp"/>
<script>
    $(document).ready(function () {

        $("#btn_guardarPromocion").click(function () {
            var nombrePromocion = $("#txt_nombrePromocion").val();
            var valor = $("#txt_valorPromocion").val();
            var descripcion = $("#txt_coniciones").val();

            if (nombrePromocion == "" || valor == "") {
                if (nombrePromocion == "") {
                    $("#mensajeE").show();
                }
                if (valor == "") {
                    $("#mensajeE2").show();
                }
            } else {

                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {nombreP: nombrePromocion, valorD: valor, descripcion: descripcion},
                    url: "../ctrPromociones"
                }).done(function (data) {
                    if (data) {
                        function notify(from, align, icon, type, animIn, animOut, title, message) {
                            $.growl({
                                icon: icon,
                                title: title || 'Bootstrap Growl ',
                                message: message || 'Turning standard Bootstrap alerts into awesome notifications',
                                url: ''
                            }, {
                                element: 'body',
                                type: type,
                                allow_dismiss: true,
                                placement: {
                                    from: from,
                                    align: align
                                },
                                offset: {
                                    x: 20,
                                    y: 85
                                },
                                spacing: 10,
                                z_index: 1031,
                                delay: 2500,
                                timer: 1000,
                                url_target: '_blank',
                                mouse_over: false,
                                animate: {
                                    enter: animIn,
                                    exit: animOut
                                },
                                icon_type: 'class',
                                template: '<div data-growl="container" class="alert" role="alert">' +
                                        '<button type="button" class="close" data-growl="dismiss">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                        '<span class="sr-only">Close</span>' +
                                        '</button>' +
                                        '<span data-growl="icon"></span>' +
                                        '<span data-growl="title"></span>' +
                                        '<span data-growl="message"></span>' +
                                        '<a href="#" data-growl="url"></a>' +
                                        '</div>'
                            });
                        }
                        ;
                        notify('bottom', 'right', '', 'success', 'animated bounceIn', 'animated bounceIn', 'Guardado ', 'Corectamente');
                        $("#txt_nombrePromocion").val("");
                        $("#txt_valorPromocion").val("");
                        $("#txt_coniciones").val("");
                    } else {

                        notify('bottom', 'right', '', 'danger', 'animated bounceIn', 'animated bounceIn', 'Error ', 'Registro No Guardado');
                    }
                }).fail(function () {
                    alert("error");
                });
            }
            return false;
        });

        $("#txt_nombrePromocion").keyup(function () {
            $("#mensajeE").hide();
        });
        $("#txt_valorPromocion").keyup(function () {
            $("#mensajeE2").hide();
        });

    });
</script>
<script>
    $("#btn_VerPromocion").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {listar: "si"},
            url: "../ctrPromociones"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
                $('#myTable').DataTable();
            }
        }).fail(function () {
            alert("error");
        });

        return false;
    });

    function eliminarPromocion(id) {
        var idPromocion = id;
        swal({
            title: "estas seguro?",
            text: "Esto no es reversible!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            swal("Eliminado!", "tu registro a sido eliminado.", "success");
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {idPromocion: idPromocion},
                    url: "../ctrPromociones"
                }).done(function (tabla) {
                    if (tabla !== null) {
                        $("#tabla").html(tabla);
                        $('#myTable').DataTable();
                    }
                }).fail(function () {

                });
                return false;
            } else {
                swal("Cancelado", "cancelado por el usuario :)", "error");
            }
        });

    }

</script>
