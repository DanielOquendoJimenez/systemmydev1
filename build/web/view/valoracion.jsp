<%@page import="controller.ctrItems"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : valoracion
    Created on : 01-mar-2016, 13:20:12
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<section>
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Seccion 1">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon glyphicon-heart-empty"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Seccion 2">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completo">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" role="tabpanel" id="step1">
                <!--registrar Valoracion Inicial-->
                <div class="pmb-block">
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Fecha</dt>
                                <dd>
                                    <div class="dtp-container dropdown fg-line">
                                        <input type='text' class="form-control date-picker" data-toggle="dropdown" id="txtFechaV" name="txtFechaV" placeholder="Fecha"><span  id="mensaje" hidden>Complete este campo</span>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Persona</dt>
                                <dd>
                                    <select  class="selectpicker" data-live-search="true" name="slcPersonaV" id="slcPersonaV">
                                        <option></option>
                                        <%
                                            ctrPersona per = new ctrPersona();
                                            ResultSet res = per.consultarP();
                                            while (res.next()) {
                                                if (res.getString("idTipo_persona").equals("3")) {

                                        %>
                                        <option value="<%=res.getString("idpersona")%>"><%=res.getString("primer_nombre")%>  <%=res.getString("primer_apellido")%></option>
                                        <%}
                                            }%>
                                    </select><span  id="mensaje2" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Objetivo</dt>
                                <dd>
                                    <textarea class="form-control" type="text" class="form-control input-mask"  id="txt_ObjetivoV" name="txt_ObjetivoV"></textarea><span  id="mensaje3" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <!-- cerra registrar Valoracion Inicial-->
                <ul class="list-inline pull-right">
                    <li><button type="button" class="btn btn-primary" value="Ga" id="siguiente">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
                </ul>
            </div>
            <div class="tab-pane" role="tabpanel" id="step2">
                <!--registrar Valoracion Inicial-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center" id="nombrePersona"><i class="zmdi zmdi-assignment-o m-r-5"></i></h2> 
                            <div class="t-body tb-padding">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Item de Valoracion</label>
                                        <select type="text" class="selectpicker" data-live-search="true" id="slcItemValoracion">
                                            <option></option> 
                                            <%
                                                ctrItems lis = new ctrItems();
                                                ResultSet resI = lis.listarItemsV();
                                                while (resI.next()) {
                                            %>
                                            <option value="<%=resI.getString("iditem_valoracion")%>"><%=resI.getString("nombre")%></option> 
                                            <%}%>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Resultado</label>
                                        <input type="text" id="idValoracion" value="" hidden="">
                                        <input type="text" id="idpersona" value="" hidden="">
                                        <input type="text" class="form-control" id="txtResultadoV" placeholder="Resultado" data-mask="0000000">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <br>
                                    <button  class="btn btn-primary btn-sm m-t-5" id="agregarValoracion">Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" id="tablaValoracion"></div>
                    </div>
                </div>                   
                <!-- cerra registrar Valoracion Inicial-->
                <ul class="list-inline pull-right">
                    <li><button type="button" class="btn btn-default prev-step"><i class="zmdi zmdi-arrow-left m-r-5"></i> Atras</button></li>
                    <li><button type="button" class="btn btn-primary next-step">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
                </ul>
            </div>
            <div class="tab-pane" role="tabpanel" id="complete">
                <h3 class="text-center">Completo</h3>
                <p class="text-center">Valoracion Registrada Corectamente</p>
                <div class="text-center">
                    <button  type="button" class="btn btn-default" id="btnOk" name="btnOk">Ok</button>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<jsp:include page="footer.jsp"/>

<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $("#siguiente").click(function (e) {
            var vlid = $("#siguiente").val();
            var fechaV = $("#txtFechaV").val();
            var objetivoV = $("#txt_ObjetivoV").val();
            var idPersona = $("#slcPersonaV").val();
            var Nombre = $("#slcPersonaV option:selected").html();
            if (fechaV == "" || objetivoV == "" || idPersona == "") {
                if (fechaV == "") {
                    $("#mensaje").show();
                }
                if (idPersona == "") {
                    $("#mensaje2").show();
                }
                if (objetivoV == "") {
                    $("#mensaje3").show();
                }

            } else {
                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {fechaV: fechaV, objetivoV: objetivoV, idPersona: idPersona},
                    url: "../ctrValoracion"
                }).done(function (data) {
                    if (data !== null) {
                        $("#idValoracion").val(data);
                        $("#nombrePersona").html(Nombre);
                        $("#idpersona").val(idPersona);
                        $("#siguiente").attr("disabled", true)
                    }
                }).fail(function () {

                });
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);

                return false;
            }
        });
        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {
            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }


    $("#agregarValoracion").click(function () {
        var resultadoV = $("#txtResultadoV").val();
        var idItem = $("#slcItemValoracion").val();
        var idValoracion = $("#idValoracion").val();
        var idPersona = $("#idpersona").val()

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {resultadoV: resultadoV, idItem: idItem, idValoracion: idValoracion, idPersona: idPersona},
            url: "../ctrValoracion"
        }).done(function (dataV) {
            $("#tablaValoracion").html(dataV);
        }).fail(function () {

        });
        return false;
    });

    $("#txt_ObjetivoV").keyup(function () {
        $("#mensaje").hide();
        $("#mensaje2").hide();
        $("#mensaje3").hide();
    });
    $("#btnOk").click(function () {
        setTimeout("location.reload()", 1000);
    });

</script>
<script>     function eliminarItem(id) {
        var idDetalleValoracion = id;
        var idPersona = $("#idpersona").val();
        var idValoracion = $("#idValoracion").val();

        if (idDetalleValoracion !== null) {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idDetalleValoracion: idDetalleValoracion, idPersona: idPersona, idValoracion: idValoracion},
                url: "../ctrValoracion"
            }).done(function (tabla) {
                if (tabla !== null) {
                    $("#tablaValoracion").html(tabla);
                }
            }).fail(function () {

            });
            return false;
        }
    }
</script>
<script>
    $("#txtFechaV").blur(function () {
        var fecha = $("#txtFechaV").val();
        var fechaActual = new Date();
        var dd = fechaActual.getDate();
        var mm = fechaActual.getMonth() + 1;
        var yy = fechaActual.getFullYear();
        var fechaActual = dd + "/" + mm + "/" + yy;

        FechaNac = fecha.split("/");
        var diaC = FechaNac[0];
        var mmC = FechaNac[1];
        var yyyyC = FechaNac[2];
//retiramos el primer cero de la izquierda
        if (mmC.substr(0, 1) == 0) {
            mmC = mmC.substring(1, 2);
        }
//retiramos el primer cero de la izquierda
        if (diaC.substr(0, 1) == 0) {
            diaC = diaC.substring(1, 2);
        }

        fecha = diaC + "/" + mmC + "/" + yyyyC;

        if (fecha == fechaActual) {
            $('#siguiente').attr("disabled", false);
            $("#txtFechaV").css({
                border: "1px green solid"
            });
        } else {
            $('#siguiente').attr("disabled", true);
            $("#txtFechaV").css({
                border: "1px red solid"
            });
        }

    });

</script>


<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>

