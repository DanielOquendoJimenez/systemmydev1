<%@page import="model.mdlPlan"%>
<%@page import="model.mdlPersona"%>
<%@page import="controller.ctrPlan"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page import="controller.ctrConsultaD"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : consultaDetalladaP
    Created on : 04-nov-2015, 12:37:15
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>

<form action="consultaDetalladaP.jsp" method="post">
    <div class="container"> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tile">
                <div class="t-header th-alt bg-blue">
                    <div class="th-title text-center">Buscar persona</div>
                </div>
                <div class="t-body tb-padding">             
                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2 class="text-center"><i class="zmdi zmdi-account m-r-5"></i> Persona</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">

                                <dl class="dl-horizontal">
                                    <dt>Documento</dt>
                                    <dd>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control autocompleto" id="txtBuscarD" placeholder="Buscar..." required name="txtBuscarD"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-success" id="btnBuscarD" name="btnBuscarD">Buscar <i class="zmdi zmdi-search m-r-5"></i></button>
                                        </div>
                                    </dd>
                                </dl>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<%
    String txtBuscarD = request.getParameter("txtBuscarD");
    if (txtBuscarD != null) {
        mdlPersona idpersona = new mdlPersona();
        mdlPlan planAc = new mdlPlan();
        idpersona.setDocumento_identidad(txtBuscarD);
        ResultSet res = idpersona.optenerIdpersona();
        if (res.next()) {
            int id = res.getInt("idpersona");
            idpersona.setIdpersona(id);
            planAc.setIdpersona(id);
            ResultSet data = idpersona.consultarPm();
            ResultSet planA = planAc.optenetFechaAc();
            if (data.next()) {

%>

<div class="container">
    <div class="tile" id="profile-main">
        <div class="pm-overview c-overflow-dark">
            <div class="pmo-pic">
                <div class="p-relative">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div id="form1" runat="server">
                                    <img id="image_upload_preview" height="259" width="259" src="../upload/<%=data.getString("foto")%>" alt="your image" class="btn-block" value="img/profile-pic.jpg" />
                                    <input class="btn btn-file" type='file' id="inputFile" name="fleFoto"  value="img/profile-pic.jpg" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="pmo-block pmo-contact hidden-xs">
                <h2>Contact</h2>
                <ul>
                    <li><i class="zmdi zmdi-phone"></i> 00971 12345678 9</li>
                    <li><i class="zmdi zmdi-email"></i> malinda-h@gmail.com</li>
                    <li><i class="zmdi zmdi-facebook-box"></i> malinda.hollaway</li>
                    <li><i class="zmdi zmdi-twitter"></i> @malinda (twitter.com/malinda)</li>
                </ul>
            </div>-->
        </div>
        <div>
            <div class="pm-body clearfix">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos Personales</a></li>
                    <li role="presentation"><a href="#planes" aria-controls="profile" role="tab" data-toggle="tab">planes adquiridos</a></li>
                    <li role="presentation"><a href="#entrenamiento" aria-controls="entrenamiento" role="tab" data-toggle="tab">Plan de entrenamiento</a></li>
                    <li role="presentation"><a href="#nutricion" aria-controls="nutricion" role="tab" data-toggle="tab">Plan nutricional</a></li>
                    <li role="presentation"><a href="#valoracion" aria-controls="valoracion" role="tab" data-toggle="tab">Valoracion</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">            
                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-account m-r-5"></i> Información Basica</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>

                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a data-pmb-action="edit" href="#">Editar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Primer nombre</dt>
                                        <dd><%=data.getString("primer_nombre")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Segundo nombre</dt>
                                        <dd><%=data.getString("segundo_nombre")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Primer apellido</dt>
                                        <dd><%=data.getString("primer_apellido")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Segundo apellido</dt>
                                        <dd><%=data.getString("segundoApellido")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Documento</dt>
                                        <dd><%=data.getString("documento_identidad")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Fecha de nacimiento</dt>
                                        <dd><%=data.getString("fecha_nacimiento")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Profeciono</dt>
                                        <dd><%=data.getString("nspecialidad")%></dd>
                                    </dl>
                                </div>
                                <div class="pmbb-edit">
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Primer nombre</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" value="<%=data.getString("primer_nombre")%>" class="form-control" placeholder="eg. Mallinda Hollaway">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Segundo nombre</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" value="<%=data.getString("segundo_nombre")%>" class="form-control" placeholder="eg. Mallinda Hollaway">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Primer apellido</dt>
                                        <dd>
                                            <div class="dtp-container dropdown fg-line">
                                                <input type='text' value="<%=data.getString("primer_apellido")%>" class="form-control date-picker" data-toggle="dropdown" placeholder="Click here...">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Segundo apellido</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text"  value="<%=data.getString("segundoApellido")%>" class="form-control" placeholder="eg. Mallinda Hollaway">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Documento</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" value="<%=data.getString("documento_identidad")%>" class="form-control" placeholder="eg. Mallinda Hollaway">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Fecha de nacimiento</dt>
                                        <dd>
                                            <div class="dtp-container dropdown fg-line">
                                                <input type='text' value="<%=data.getString("fecha_nacimiento")%>" class="form-control date-picker" data-toggle="dropdown" placeholder="Click here...">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Profecion</dt>
                                        <dd>
                                            <div class="dtp-container dropdown fg-line">
                                                <input type='text' value="<%=data.getString("nspecialidad")%>" class="form-control date-picker" data-toggle="dropdown" placeholder="Click here...">
                                            </div>
                                        </dd>
                                    </dl>

                                    <div class="m-t-30">
                                        <button class="btn btn-primary btn-sm">Guardar</button>
                                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-phone m-r-5"></i> Informació de contacto</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>

                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a data-pmb-action="edit" href="#">Editar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Celular</dt>
                                        <dd><%=data.getString("Celular")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Telefono fijo</dt>
                                        <dd><%=data.getString("telefono")%></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Correo</dt>
                                        <dd><%=data.getString("correo")%></dd>
                                    </dl>
                                </div>

                                <div class="pmbb-edit">
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Celular</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" value="<%=data.getString("Celular")%>" class="form-control" placeholder="eg. 00971 12345678 9">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Telefono Fijo</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" value="<%=data.getString("telefono")%>" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Correo</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" value="<%=data.getString("correo")%>" class="form-control" placeholder="eg. @malinda">
                                            </div>
                                        </dd>
                                    </dl>
                                    <div class="m-t-30">
                                        <button class="btn btn-primary btn-sm">Guardar</button>
                                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-lock m-r-5"></i> Accseso</h2>
                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>

                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a data-pmb-action="edit" href="#">Editar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Usuario</dt>
                                        <dd></dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Contraseña</dt>
                                        <dd></dd>
                                    </dl>
                                </div>
                                <div class="pmbb-edit">
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Usuario</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="text" class="form-control" placeholder="eg. 00971 12345678 9">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Contraseña anterior</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="email" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Nueva contraseña</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="email" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt class="p-t-10">Confirmar contraseña</dt>
                                        <dd>
                                            <div class="fg-line">
                                                <input type="email" class="form-control" placeholder="eg. malinda.h@gmail.com">
                                            </div>
                                        </dd>
                                    </dl>
                                    <div class="m-t-30">
                                        <button class="btn btn-primary btn-sm">Guardar</button>
                                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="planes">
                        <div class="container">
                            <div class="tile">
                                <div class="action-header clearfix">
                                    <h2 class="ah-label hidden-xs">Planes</h2>

                                    <div class="ah-search">
                                        <input type="text" placeholder="Start typing..." class="ahs-input">

                                        <i class="ahs-close">&times;</i>
                                    </div>

                                    <ul class="ah-actions actions">
                                        <li>
                                            <a data-toggle="modal" href="#modalDefault"><i class="zmdi zmdi-plus-circle"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="p-timeline">
                                    <div class="pt-line c-gray text-right">
                                        <span class="d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        &nbsp;
                                    </div>  
                                    <div class="pt-body">
                                        <h2 class="ptb-title">Plan Actual</h2>
                                        <div class="lightbox clearfix">
                                            <ul class="clist clist-star">
                                                <%
                                                    if (planA.next()) {
                                                %>
                                                <li><%=planA.getString("nombre")%> inicio <%=planA.getString("fecha_inicio")%> hasta <%=planA.getString("fecha_final")%></li>
                                                <li>Estado
                                                    <ul>
                                                        <%
                                                            if (planA.getString("estado").equals("1")) {%>
                                                        <li>Activo</li>
                                                            <%} else {%>
                                                        <li>Inactivo</li>
                                                            <%}%>
                                                    </ul>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-timeline">
                                    <div class="pt-line c-gray text-right">
                                        <span class="d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        &nbsp;
                                    </div>  
                                    <div class="pt-body">
                                        <h2 class="ptb-title">Adquiridos Pilates</h2>
                                        <div class="lightbox clearfix">
                                            <ul class="clist clist-star">
                                                <li>Trimestre-02/05/2015</li>                                              
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-timeline">
                                    <div class="pt-line c-gray text-right">
                                        <span class="d-block">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        &nbsp;
                                    </div>  
                                    <div class="pt-body">
                                        <h2 class="ptb-title">Adquiridos Normal</h2>
                                        <div class="lightbox clearfix">
                                            <ul class="clist clist-star">
                                                <li>Lorem ipsum dolor sit amet</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="entrenamiento">

                    </div>
                    <div role="tabpanel" class="tab-pane" id="nutricion">

                    </div>
                    <div role="tabpanel" class="tab-pane" id="valoracion">
                        
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="../ctrPlan">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Plan</h4>
                </div>
                <div class="modal-body">

                    <div class="t-body tb-padding">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                <select name="slcPlan" class="selectpicker" data-live-search="true">
                                    <option></option>
                                    <optgroup label="Normal"> 
                                        <%
                                            ctrPlan lis = new ctrPlan();
                                            ResultSet rp = lis.listarPlan();
                                            while (rp.next()) {
                                                if (rp.getString("nombreTp").equals("Normal") && rp.getString("estado").equals("1")) {

                                        %> 
                                        <option value="<%=rp.getString("idplan")%>"><%=rp.getString("nombre")%></option>

                                        <%}%>
                                        <%}%>
                                    </optgroup> 
                                    <% rp.beforeFirst();%> 
                                    <optgroup label="Pilates"> 
                                        <%
                                            while (rp.next()) {
                                                if (rp.getString("nombreTp").equals("Pilates") && rp.getString("estado").equals("1")) {
                                        %>
                                        <option value="<%=rp.getString("idplan")%>"><%=rp.getString("nombre")%></option>
                                        <%}%>                                       
                                        <%}%>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                <input type="text" hidden="" value="<%=data.getString("idpersona")%>" name="txtIdPersona"/>
                                <div class="dtp-container dropdown fg-line">
                                    <input type='text' class="form-control date-picker" data-toggle="dropdown" name="txtFechaPlan" placeholder="Fecha Inicio del Plan">
                                </div>
                            </div>
                        </div>
                    </div>         
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary" name="btnGuardarPlan" id="btnGuardarPlan">Guardar</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>                   
        </div>
    </div>
</div>
<%
            }
        }
    }
%>
<jsp:include page="footer.jsp"/>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
</script>
<script>
    if ($('.autocompleto')[0]) {
    var statesArray = [
    <%
        ctrPersona doc = new ctrPersona();
        ResultSet res = doc.consultarP();
        while (res.next()) {
    %>
    '<%=res.getString("documento_identidad")%>',
    <%}%>
    ];
            var states = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: statesArray
            });
            $('.autocompleto').typeahead({
    hint: true,
            highlight: true,
            minLength: 1
    },
    {
    name: 'states',
            source: states
    });
    }
</script>
<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>