<%-- 
Document   : registrarEncuesta
Created on : 8/02/2016, 06:22:33 PM
Author     : YEISSON
--%>

<%@page import="controller.ctrPersona"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPregunta"%>
<%@page import="controller.ctrEncuesta"%>

<%
HttpSession ht = request.getSession();
if (ht.getAttribute("nombre") != null) {
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>


<%-- 
REGISTRAR ENCUESTA
--%>
<section>
    <div class="wizard">
        <div class="wizard-inner">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Seccion 1">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon glyphicon-heart-empty"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Seccion 2">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation" class="disabled">
                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completo">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" role="tabpanel" id="step1">
                <!--registrar Valoracion Inicial-->
                <div class="pmb-block">
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Fecha</dt>
                                <dd>
                                    <div class="dtp-container dropdown fg-line">
                                        <input type='text' class="form-control date-picker" data-toggle="dropdown" id="txtFecha" name="txtFecha" placeholder="Fecha"><span  id="mensaje" hidden>Complete este campo</span>
                                    </div>
                                </dd>
                            </dl>

                            <dl class="dl-horizontal">
                                <dt>observación</dt>
                                <dd>
                                    <textarea  class="form-control" type="text" class="form-control input-mask"  id="txt_pronostico" name="txt_pronostico"></textarea><span  id="mensaje1" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Cliente</dt>
                                <dd>
                                    <select  class="selectpicker" data-live-search="true" name="cliente" id="cliente">
                                        <option></option>
                                        <%
                                        ctrPersona per1 = new ctrPersona();
                                        ResultSet res1 = per1.consultarP();
                                        while (res1.next()) {
                                        if (res1.getString("idTipo_persona").equals("3")) {

                                        %>
                                        <option value="<%=res1.getString("idpersona")%>"><%=res1.getString("primer_nombre")%>  <%=res1.getString("primer_apellido")%></option>
                                        <%}
                                    }%>
                                </select><span  id="mensaje2" hidden>Complete este campo</span>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <!-- cerra registrar Valoracion Inicial-->
            <ul class="list-inline pull-right">
                <li><button type="button" class="btn btn-primary next" id="siguiente">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
            </ul>
        </div>
        <div class="tab-pane" role="tabpanel" id="step2">
            <!--registrar Valoracion Inicial-->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <dt>Nombre del Cliente:</dt>
                                </div>
                                <div class="col-md-4">
                                    <dt  id="nombreCliente"></dt> 
                                </div>
                            </div>
                        </div>

                        <div class="t-body tb-padding">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Pregunta</label>
                                    <select type="text" class="selectpicker" data-live-search="true" id="slcPregunta">
                                        <option></option> 
                                        <%
                                        ctrPregunta lis = new ctrPregunta();
                                        ResultSet resI = lis.consultarPreguntas();
                                        while (resI.next()) {
                                        %>
                                        <option value="<%=resI.getString("idpreguntas")%>"><%=resI.getString("pregunta")%></option> 
                                        <%}%>
                                    </select><span  id="mensaje3" hidden>Complete este campo</span>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Resultado</label>
                                    <select type="text" class="selectpicker" data-live-search="true" id="slcRespuesta" >
                                        <option></option>                                           
                                        <option value="1">Si</option> 
                                        <option value="2">No</option> 
                                    </select><span  id="mensaje4" hidden>Complete este campo</span>
                                </div>
                            </div>

                            <div class="col-sm-4"> 
                                <input hidden type="text" id="idEncuesta" value="" >
                                <input hidden type="text" id="idPersona" value="" >

                                <br>
                                <button  class="btn btn-primary btn-sm m-t-5" id="agregarValor">Agregar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" id="tablaEncuesta"></div>
                </div>
            </div>                   
            <!-- cerra registrar -->
            <ul class="list-inline pull-right">
                <li><button type="button" class="btn btn-default prev-step"><i class="zmdi zmdi-arrow-left m-r-5"></i> Atras</button></li>
                <li><button type="button" class="btn btn-primary next-step" id="next-step">Guardar  <i class="zmdi zmdi-arrow-right m-r-5"></i></button></li>
            </ul>
        </div>
        <div class="tab-pane" role="tabpanel" id="complete">
            <h3 class="text-center">Completo</h3>
            <p class="text-center">Encuesta Registrada Corectamente</p>
            <div class="text-center">
                <a href="registrarEncuesta.jsp" class="btn btn-default">Ok</a>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
</div>
</section>
<%-- 
FIN________REGISTRAR ENCUESTA
--%>

<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-3">
       <div class="m-t-30">
           <a href="consultarEncuesta.jsp">
               <button type="button" class="btn btn-info">Consultar Encuesta</button>
           </a>
       </div>
   </div>
   <div class="col-md-4"></div>
</div>

<jsp:include page="footer.jsp"/>
<script>
    $(document).ready(function () {
//Initialize tooltips
$('.nav-tabs > li a[title]').tooltip();

//Wizard
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    var $target = $(e.target);

    if ($target.parent().hasClass('disabled')) {
        return false;
    }
});

$("#siguiente").click(function (e) {
    var fechaV = $("#txtFecha").val();
    var pronostico = $("#txt_pronostico").val();
    var idPersona = $("#cliente").val();


    var nombreCliente = $("#cliente option:selected").html();


    if (fechaV == "" || pronostico == "" || idPersona == "") {
        if (fechaV == "") {
            $("#mensaje").show();
        } else {
            $("#mensaje").hide();
        }

        if (pronostico == "") {
            $("#mensaje1").show();
        } else {
            $("#mensaje1").hide();
        }
        if (idPersona == "") {
            $("#mensaje2").show();
        } else {
            $("#mensaje2").hide();
        }

    } else {
        $.ajax({type: 'POST', dataType: 'text',
            data: {fechaV: fechaV, pronostico: pronostico, idPersona: idPersona, nombreCliente: nombreCliente}, url: "../ctrEncuesta"
        }).done(function (data) {
            if (data !== null) {
                $("#idEncuesta").val(data);
                $("#nombreCliente").html(nombreCliente);
                $("#idPersona").val(idPersona);
                $("#siguiente").attr("disabled", true)
            }
        }).fail(function () {

        });
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

        return false;
    }
});
$(".next-step").click(function (e) {
    var $active = $('.wizard .nav-tabs li.active');
    $active.next().removeClass('disabled');
    nextTab($active);

});
$(".prev-step").click(function (e) {
    var $active = $('.wizard .nav-tabs li.active');
    prevTab($active);

});

});
    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }


    $("#agregarValor").click(function () {

        var resultadoV = $("#slcRespuesta").val();
        var idPregunta = $("#slcPregunta").val();
        var idEncuesta = $("#idEncuesta").val();
        var idPersona = $("#idPersona").val()

        if (resultadoV == "" || idPregunta == "") {
            if (idPregunta == "") {
                $("#mensaje3").show();
            } else {
                $("#mensaje3").hide();
            }

            if (resultadoV == "") {
                $("#mensaje4").show();
            } else {
                $("#mensaje4").hide();
            }

        } else {
            $("#mensaje3").hide();
            $("#mensaje4").hide();
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {resultadoV: resultadoV, idPregunta: idPregunta, idEncuesta: idEncuesta, idPersona: idPersona},
                url: "../ctrEncuesta"
            }).done(function (data) {
                $("#tablaEncuesta").html(data);
            }).fail(function () {
            });
            return false;
        }
    });

</script>
<script>
    function eliminarItem(id) {
        var idDetalleEncuesta = id;
        var idPersona = $("#idPersona").val();
        var idValoracion = $("#idEncuesta").val();

        if (idDetalleEncuesta !== null) {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {idDetalleEncuesta: idDetalleEncuesta, idPersona: idPersona, idValoracion: idValoracion},
                url: "../ctrEncuesta"
            }).done(function (tabla) {
                if (tabla !== null) {
                    $("#tablaEncuesta").html(tabla);
                }
            }).fail(function () {

            });
            return false;
        }
    }
</script>
<%    } else {
response.sendRedirect("../index.jsp");
}
%>