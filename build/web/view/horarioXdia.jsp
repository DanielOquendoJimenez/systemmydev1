
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>

<%-- 
    Document   : horarioXdia
    Created on : 05-ene-2016, 15:57:10
    Author     : daniel / yeisson / Mayerly
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="javax.swing.DefaultComboBoxModel"%>
<%@page import="model.mdlItems"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<!-- Registrar Horario x dia-->

<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar Horario</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-wrench m-r-5"></i> HORARIO DÍA</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrItems">
                                <dl class="dl-horizontal">
                                    <dt>Nombre Horario por día(*)</dt>
                                    <dd>
                                    <input type="text" class="form-control input-mask" id="txt_nombreHorario" name="txt_nombreHorario" ><span id="mensaje" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>du Horario por día(*)</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txtdu" name="txtdu" >
                                    </dd>
                                </dl>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success" value="Registrar" id="enviar-btn" name="guardarH" >
                                    <input type="button" class="btn btn-primary" id="btn_verHorario" name="btn_verHorario" value="Consultar Horario">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla"></div>
        </div>

        <!-- Cierra registrar Horario x dia-->

        <!-- Modal editar Horario x dia-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Horario por día</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="../ctrItems">
                            <dl class="dl-horizontal">
                                <dt hidden>Id Horario por día (*)</dt>
                                <dd>
                                <input type="hidden" id="txtidhx" name="txtidhx"  class="form-control input-mask" ><span id="mensaje1" hidden>Complete este campo</span>
                                </dd>
                            </dl>  
                            <dl class="dl-horizontal">
                                <dt>Nombre Horario por día (*)</dt>
                                <dd>
                                    <input type="text" id="txtnombho" name="txtnombho" class="input-sm form-control" >
                                </dd>
                            </dl>          
                            <dl class="dl-horizontal">
                                <dt>du Horario por día (*)</dt>
                                <dd>
                                    <input type="text" id="txtduu" name="txtduu" class="input-sm form-control" >
                                </dd>
                            </dl>  
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary" value="Modificar Horarioxdia" id="txtmodi" name="txtmodi">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>    
                            </div>
                            <div class="row">
                                 <div class="col-md-3"></div>
                                <div class="col-md-5">
                                    <div class="m-t-30">                                       
                                        <a href="consultarHorarioXdia.jsp">
                                            <button type="button" class="btn btn-primary">Consultar Horario por día</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 


<jsp:include page="footer.jsp"/>

<script>
    $("#txtdu").timepicki();
</script>

<script>
    $("#txtduu").timepicki();
</script>

<!-- editar Horario x dia-->
<script>
    function editarHorarioxdia(id, nom,dur) {
        $(document).on("click", ".open-Modal", function () {
        });
        $("#txtidhx").val(id);
        $("#txtnombho").val(nom);        
        $("#txtduu").val(dur);
    }
</script>
<!-- cierra editar Horario x dia-->

<!-- Ver Horario x dia-->
<script>
    $("#btn_verHorario").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {listaree: "si"},
            url: "../ctrItems"

        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    });
</script>
<!-- Cierra ver Horario x dia-->

<!-- Estado Horario x dia-->
<script>
    function estadoHorarioxdia(estado, idhorarioxdia) {

        swal("Cambio de estado", "Exitoso", "success");

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {estado: estado, idhorarioxdia: idhorarioxdia},
            url: "../ctrItems"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    }
</script>
<!-- Cierra estado Horario x dia-->

<!-- validacion Horario x dia-->
<script>
    $(document).ready(function () {
        $("#enviar-btn").click(function () {
            var txtnomb = $("#txt_nombreHorario").val();

            if (txtnomb == "") {
                if (txtnomb == "") {
                    $("#mensaje").show();
                } else {
                    $("#mensaje").hide();
                }
                return false;
            }
        });

        $("#txtmodi").click(function () {
            var txtnomb = $("#txtnomb").val();
            if (txtnomb == "") {
                if (txtnomb == "") {
                    $("#mensaje1").show();
                } else {
                    $("#mensaje1").hide();
                }
                return false;
            }
        });
    }
    );
</script>
<!-- cierra validacion Horario x dia-->

<%
    } else {
        response.sendRedirect("../index.jsp");
    }
%>

