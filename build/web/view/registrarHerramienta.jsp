<%-- 
Document   : registrarHerramienta
Created on : 05-ene-2016, 11:00:10
Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar Herramienta</div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-wrench m-r-5"></i> HERRAMIENTAS</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrHerramientas">
                                <dl class="dl-horizontal">
                                    <dt>Nombre de la herramienta</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_nombreHerramienta" name="txt_nombreHerramienta" />
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Serial</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_Serial" name="txt_Serial"/>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                <dt>Descripción</dt>
                                    <dd>
                                        <textarea class="form-control" id="message-text" type="text" class="form-control input-mask"  id="txt_Descripcion" name="txt_Descripcion" >                                            
                                        </textarea>
                                    </dd>
                                </dl>                                                        
                                <div class="row">
                                 <div class="col-md-3"> 
                                    <div class="m-t-30">                                       
                                        <a href="consultarHerramientas.jsp">
                                            <button type="button" class="btn btn-primary">Consultar Herramientas</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-5"></div>
                                <div class="col-md-4">
                                    <div class="m-t-30" id="ver1">
                                        <input type="submit" class="btn btn-success" value="Guardar" id="enviar-btn" name="enviar-btn" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> 
<jsp:include page="footer.jsp"/>