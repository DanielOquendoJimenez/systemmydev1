
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>

<%-- 
    Document   : alimento
    Created on : 25/02/2016, 04:27:32 PM
    Author     : Maychan
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="javax.swing.DefaultComboBoxModel"%>
<%@page import="controller.ctrAlimento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="header.jsp"/>

<!-- Crear Alimento -->

<div class="container c-boxed">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <header class="page-header">
            <h3>Alimento<small>xxxxxxxxxxxxxxxxxxxxxxxx</small></h3>
        </header>

        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center"><h3>Crear Alimento</h3></div>
            </div>

            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h3 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i>Alimento</h3>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form method="POST" action="../ctrAlimento">
                                <dl class="dl-horizontal">
                                    <dt>Nombre Alimento (*)</dt>
                                    <dd>
                                    <input type="text" class="input-sm form-control" id="txt_horai" name="txt_nombrea" ><span id="mensaje" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descripcion Alimento (*)</dt>
                                    <dd>
                                    <input type="text" class="input-sm form-control" id="txt_horaf" name="txt_descripciona"  ><span id="mensaje1" hidden>Complete este campo</span>
                                    </dd>
                                </dl>                               
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary" value="Crear Alimento" id="txt_guardar2" name="txt_guardar2">
                                    <input type="button" id="btn_verAlimento" class="btn btn-success" name="btn_verAlimento" value="Consultar Alimento">
                                </div>
                            </form> 
                        </div>
                    </div>                        
                </div>
            </div> 
            <div id="tabla"></div>    
        </div>

        <!-- Cierra crear Alimento -->

        <!-- modal editar Alimento -->

        <div class="modal fade bs-example-modal-xs" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Modificar Alimento</h4>
                    </div>
                    <div class="modal-body">
                        <form  method="POST" action="../ctrAlimento">
                            <dl class="dl-horizontal">
                                <dt><center>Alimento</center></dt>
                                <dd>
                                    <input type="hidden" name="txtId" id="txtId" class="input-sm form-control">  
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Nombre Alimento (*)</dt>
                                <dd>
                                <input type="text" name="txt_nomb" id="txt_nom" class="input-sm form-control" ><span id="mensaje2" hidden>Complete este campo</span>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Descripcion Alimento (*)</dt>
                                <dd>
                                <input type="text" id="txt_des" name="txt_des" class="input-sm form-control" ><span id="mensaje3" hidden>Complete este campo</span>
                                </dd>
                            </dl>                               
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary" value="Modificar Alimento" id="txt_modifi" name="txt_modifi">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- cierra modal editar Alimento -->


<jsp:include page="footer.jsp"/>

<!-- Editar Alimento -->
<script>
    function editarAlimento(id, nom, des) {
        $(document).on("click", ".open-Modal", function () {
        });
        $("#txtId").val(id);
        $("#txt_nom").val(nom);
        $("#txt_des").val(des);
    }
</script>  
<!-- Cierra editar Alimento -->

<!-- Ver Alimento -->
<script>
    $("#btn_verAlimento").click(function () {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {listarr: "si"},
            url: "../ctrAlimento"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    });
</script>
<!-- Cierra ver Alimento -->

<!-- Estado Alimento -->
<script>
    function estadoAlimento(estado, idalimento) {

        swal("Cambio de estado", "Exitoso", "success");

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {estado: estado, idalimento: idalimento},
            url: "../ctrAlimento"
        }).done(function (tabla) {
            if (tabla !== null) {
                $("#tabla").html(tabla);
            }
        }).fail(function () {
            alert("mierda");
        });
        return false;
    }
</script>
<!-- Cierra estado Alimento -->

<!-- validacion  Alimento -->
<script>
    $(document).ready(function () {
        $("#txt_guardar2").click(function () {
            var txt_horai = $("#txt_horai").val();
            var txt_horaf = $("#txt_horaf").val();

            if (txt_horai == "" || txt_horaf == "") {
                if (txt_horai == "") {
                    $("#mensaje").show();
                } else {
                    $("#mensaje").hide();
                }
                if (txt_horaf == "") {
                    $("#mensaje1").show();
                } else {
                    $("#mensaje1").hide();
                }
                return false;
            }
        });

        $("#txt_modif").click(function () {
            var txt_nom = $("#txt_nom").val();
            var txt_des = $("#txt_des").val();

            if (txt_nom == "" || txt_des == "") {
                if (txt_nom == "") {
                    $("#mensaje2").show();
                } else {
                    $("#mensaje2").hide();
                }
                if (txt_des == "") {
                    $("#mensaje3").show();
                } else {
                    $("#mensaje3").hide();
                }
                return false;
            }
        });
    }
    );
</script>
<!-- cierra validacion  Alimento -->

<%
    } else {
        response.sendRedirect("../index.jsp");
    }
%>

