<%@page import="controller.ctrPlan"%>
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>
<%-- 
    Document   : registrarPersona
    Created on : 14-oct-2015, 13:44:40
    Author     : daniel
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container"> 
    <form action="../ctrPersona" method="post" enctype="multipart/form-data" data-parsley-validate>
        <div class="tile" id="profile-main">
            <div class="pm-overview c-overflow-dark">
                <div class="pmo-pic">
                    <div class="p-relative">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div id="form1" runat="server">
                                        <img id="image_upload_preview" height="259" width="259" src="img/profile-pic.jpg" alt="your image" class="btn-block" value="img/profile-pic.jpg" />
                                        <input class="btn btn-file" type='file' id="inputFile" name="fleFoto"  value="C:/Users/daniel/Desktop/Proycto sena/SystemMYDE/Aplicacion/SystemMYDEv1/web/upload/profile-pic.jpg" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-center"><i class="zmdi zmdi-account m-r-5"></i> Tipo de persona</p>
                        <select data-live-search="true" class="selectpicker" name="slcTipoPersona" onchange="val(this.value)" required="">
                            <option></option>
                            <%
                                ctrPersona ct = new ctrPersona();
                                ResultSet rs = ct.listarTP();
                                while (rs.next()) {
                                    if (ht.getAttribute("roll").equals("2") && rs.getString("idtipo_persona").equals("1") || ht.getAttribute("roll").equals("2") && rs.getString("idtipo_persona").equals("2")) {
                            %>
                            <%} else {%>
                            <option value="<%=rs.getString("idtipo_persona")%>"><%=rs.getString("descripcion")%></option>
                            <%}%>
                            <%}%>
                        </select>
                    </div>
                </div>                      
            </div>
            <div class="pm-body clearfix">
                <ul class="tab-nav tn-justified">
                    <li class="active"><a>Registro de Personas</a></li>
                </ul>
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-account m-r-5"></i> Información básica</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Primer nombre (*)</dt>
                                <dd><input type="text" name="txtPrimerNombre" class="form-control" placeholder="Primer nombre" onkeypress="return soloLetras(event)" required=""></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Segundo nombre</dt>
                                <dd><input type="text" name="txtSegundoNombre" class="form-control" placeholder="Segundo nombre" onkeypress="return soloLetras(event)"></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Primer apellido (*)</dt>
                                <dd><input type="text" name="txtPrimerApellido" class="form-control" placeholder="Primer apellido" onkeypress="return soloLetras(event)" required=""></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Segundo apellido</dt>
                                <dd><input type="text" name="txtSegundoApellido" class="form-control"  placeholder="Segundo apellido" onkeypress="return soloLetras(event)"></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Documento (*)</dt>
                                <dd><input type="text" id="txtDoc" name="txtDocumento" class="form-control"  placeholder="Documento de identidad" value="" onchange="validarDoc(this.value)" data-mask="00000000000" required=""></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Fecha Nacimiento (*)</dt>
                                <dd>
                                    <div class="dtp-container dropdown fg-line">
                                        <input type='text' class="form-control date-picker" data-toggle="dropdown" name="txtFecha" id="txtFecha" required="required" placeholder="Fecha Nacimiento" data-mask="00/00/0000" onblur="calcular_edad(this.value)">
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Profesión (*)</dt>
                                <dd><input type="text" name="txtProfecion" class="form-control" placeholder="Profesión" onkeypress="return soloLetras(event)" required=""></dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-phone m-r-5"></i> Información del contacto</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Celular (*)</dt>
                                <dd><input type="text" name="txtCelular" class="form-control"  placeholder="Celular" data-mask="0000000000" required=""></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Correo</dt>
                                <dd><input type="email" name="txtCorreo" class="form-control"  placeholder="Correo" required="@"></dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Teléfono</dt>
                                <dd><input type="text" name="txtTelefono" class="form-control"  placeholder="telefono" data-mask="0000000"></dd>
                            </dl>
                            <div class="m-t-30" id="ver1">
                                <button type="submit" class="btn btn-success btn-block" id="btnGuardar">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pmb-block" id="ver" hidden>
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-assignment-o m-r-5"></i> PLan</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Fecha Inicio del plan</dt>
                                <dd>
                                    <div class="dtp-container dropdown fg-line">
                                        <input type='text' class="form-control date-picker" data-toggle="dropdown" name="txtFecha" placeholder="Fecha Inicio del Plan">
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Plan</dt>
                                <dd>
                                    <select name="slcPlan" class="selectpicker" data-live-search="true">
                                        <option></option>
                                        <optgroup label="Normal"> 
                                            <%
                                                ctrPlan lis = new ctrPlan();
                                                ResultSet rp = lis.listarPlan();
                                                while (rp.next()) {
                                                    if (rp.getString("nombreTp").equals("Normal") && rp.getString("estado").equals("1")) {

                                            %> 
                                            <option value="<%=rp.getString("idplan")%>"><%=rp.getString("nombre")%></option>
                                            <%}%>
                                            <%}%>
                                        </optgroup> 
                                        <% rp.beforeFirst();%> 
                                        <optgroup label="Pilates"> 
                                            <%
                                                while (rp.next()) {
                                                    if (rp.getString("nombreTp").equals("Pilates") && rp.getString("estado").equals("1")) {
                                            %>
                                            <option value="<%=rp.getString("idplan")%>"><%=rp.getString("nombre")%></option>
                                            <%}%>                                       
                                            <%}%>
                                        </optgroup>
                                    </select>
                                </dd>
                            </dl>
                            <div class="m-t-30">
                                <button type="submit" class="btn btn-success btn-block" id="btnGuardar2">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<jsp:include page="footer.jsp"/>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputFile").change(function () {
        readURL(this);
    });
</script>
<script>
    function validarDoc(val) {

        if (val !== "") {
            $.ajax({
                type: 'POST',
                dataType: 'text',
                data: {valor: val},
                url: '../ctrPersona'
            }).done(function (s) {
                if (s != "") {
                    var input = $("#txtDoc").css({
                        border: "1px red solid"
                    });
                    $('#btnGuardar').attr("disabled", true);
                    $('#btnGuardar2').attr("disabled", true);
                } else {
                    var input = $("#txtDoc").css({
                        border: "1px green solid"
                    });
                    $('#btnGuardar').attr("disabled", false);
                    $('#btnGuardar2').attr("disabled", false);
                }

            }).fail(function () {
                alert("no");
            });
        }
    }
</script>
<script>
    function val(valor) {
        if (valor == 3 || valor == 4) {
            $("#ver").first().show("fast", function showNext() {
                $(this).next("#ver").show("fast", showNext);
            });
            $("#ver1").hide(1000);
        } else {
            $("#ver1").first().show("fast", function showNext() {
                $(this).next("#ver1").show("fast", showNext);
            });
            $("#ver").hide(1000);
        }

    }
</script>
<script>
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = "8-37-39-46";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }
</script>
<script type="text/javascript">

    /*----------Funcion para obtener la edad------------*/
    function calcular_edad(fecha) {
        var fechaActual = new Date()
        var diaActual = fechaActual.getDate();
        var mmActual = fechaActual.getMonth() + 1;
        var yyyyActual = fechaActual.getFullYear();
        FechaNac = fecha.split("/");
        var diaCumple = FechaNac[0];
        var mmCumple = FechaNac[1];
        var yyyyCumple = FechaNac[2];
        //retiramos el primer cero de la izquierda
        if (mmCumple.substr(0, 1) == 0) {
            mmCumple = mmCumple.substring(1, 2);
        }
        //retiramos el primer cero de la izquierda
        if (diaCumple.substr(0, 1) == 0) {
            diaCumple = diaCumple.substring(1, 2);
        }
        var edad = yyyyActual - yyyyCumple;

        //validamos si el mes de cumpleaños es menor al actual
        //o si el mes de cumpleaños es igual al actual
        //y el dia actual es menor al del nacimiento
        //De ser asi, se resta un año
        if ((mmActual < mmCumple) || (mmActual == mmCumple && diaActual < diaCumple)) {
            edad--;
        }
        if (edad >= 14) {
            $('#btnGuardar').attr("disabled", false);
            $('#btnGuardar2').attr("disabled", false);
            $("#txtFecha").css({
                border: "1px green solid"
            });
        } else {
            $('#btnGuardar').attr("disabled", true);
            $('#btnGuardar2').attr("disabled", true);
            $("#txtFecha").css({
                border: "1px red solid"
            });
        }
    };
</script>
<%    } else {
        response.sendRedirect("../index.jsp");
    }
%>
