<%-- 
Document   : eliminarPlan
Created on : 08-dic-2015, 11:04:54
Author     : daniel
--%>

<%@page import="model.mdlPlan"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrPlan"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<%

String modificar = request.getParameter("accion");

if (modificar != null) {
mdlPlan mod = new mdlPlan();
mod.setIdplan(Integer.parseInt(modificar));
ResultSet res = mod.listarPlanId();
if (res.next()) {

%>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Registrar Plan <div class="pull-right"><a href="consultarPlan.jsp">X</a></div></div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i> Planes</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrPlan">
                                <dl class="dl-horizontal">
                                    <dt>Nombre del Plan</dt>
                                    <dd>
                                        <input type="hidden" hidden class="form-control input-mask" id="txt_idPlan" name="txt_idPlan" value="<%=res.getString("idplan")%>" />
                                        <input type="text" class="form-control input-mask" id="txt_nombre" name="txt_nombre" value="<%=res.getString("nombre")%>" /><span  id="mensaje" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Valor</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_valor" name="txt_valor" value="<%=res.getString("valor")%>"/><span  id="mensaje1" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descuento</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_descuento" name="txt_descuento" value="<%=res.getString("descuento")%>"/><span  id="mensaje2" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Dias</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask" id="txt_dias" name="txt_dias" value="<%=res.getString("dias_cliente")%>" /><span  id="mensaje3" hidden>Complete este campo</span>
                                    </dd>
                                </dl>                               
                                <div class="m-t-30" id="ver1">
                                    <input type="submit" class="btn btn-success" value="Modificar" id="enviar-btn" name="btnModificar" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<%}%>
<%}%>
<table id="myTable" class="table table-bordered table-vmiddle">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Valor</th>
            <th>Descuento</th>
            <th>Días</th>
            <th>Tipo</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        <%
        ctrPlan cp = new ctrPlan();
        ResultSet plan = cp.listarPlan();
        while (plan.next()) {
        %>
        <tr>
            <td><%=plan.getString("nombre")%></td>
            <td><%=plan.getString("valor")%></td>
            <td><%=plan.getString("descuento")%></td>
            <td><%=plan.getString("dias_cliente")%></td>
            <td><%=plan.getString("nombreTp")%></td>
            <td>
                <%
                if (plan.getString("estado").equals("1")) {
                %>
                <button class="btn btn-success" onclick="estadoPlan(0,<%=plan.getString("idplan")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                <%} else {%>
                <button class="btn btn-danger" onclick="estadoPlan(1,<%=plan.getString("idplan")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                <%}%>
                <a href="consultarPlan.jsp?accion=<%=plan.getString("idplan")%>" class="btn btn-primary"><i class="zmdi zmdi-edit"></i></a>
            </td>
        </tr>  
        <%}%>
    </tbody>
</table>

<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-4">
        <a href="plan.jsp">
            <button type="button" class="btn btn-success">Registrar Plan</button>
        </a>
    </div>
    <div class="col-md-3"></div>
</div>


<jsp:include page="footer.jsp"/>
<script>
    function estadoPlan(estado, idPlan) {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: {estado: estado, idPlan: idPlan},
            url: "../ctrPlan"
        }).done(function (data) {
            if (data) {
                setTimeout("location.reload()", 1);
            }
        }).fail(function () {

        });

    }
</script>
<script>
    function prueba(id) {
        var idP = id;
        alert("hola" + idP);

    }
</script>

<script>
    $(document).ready(function () {
        $("#enviar-btn").click(function () {
            var nombre = $("#txt_nombre").val();
            var valor = $("#txt_valor").val();
            var descuento = $("#txt_descuento").val();
            var dias = $("#txt_dias").val();
            if (valor == "" || descuento == "" || dias == "") {
                if (nombre == "") {
                    $("#mensaje").show();
                } else {
                    $("#mensaje").hide();
                } 
                if (valor == "") {
                    $("#mensaje1").show();

                } else {
                    $("#mensaje1").hide();
                }
                if (descuento == "") {
                    $("#mensaje2").show();

                } else {
                    $("#mensaje2").hide();
                }
                if (dias == "") {
                    $("#mensaje3").show();
                } else {
                    $("#mensaje3").hide();
                }                
                return false;              
            }
        });
    }
    );
</script>
