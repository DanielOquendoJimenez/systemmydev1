<%-- 
Document   : consultarHerramientas
Created on : 13/02/2016, 11:53:46 AM
Author     : YEISSON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.mdlHerramienta"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="controller.ctrHerramientas"%>

<jsp:include page="header.jsp"/>
<%-- 
MODIFICAR UNA HERRAMIENTA
--%>
<%

String modificar = request.getParameter("accion");

if (modificar != null) {
mdlHerramienta mod = new mdlHerramienta();
mod.setIdherramienta(Integer.parseInt(modificar));
ResultSet res = mod.listarHerramientaId();
if (res.next()) {

%>
<div class="container"> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="title">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center">Modificar Herramienta <div class="pull-right"><a href="consultarHerramientas.jsp" style="color: black">X</a></div></div>
            </div>
            <div class="t-body tb-padding">             
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2 class="text-center"><i class="zmdi zmdi-assignment-o m-r-5"></i>Herramienta</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <form name="register" method="post" action="../ctrHerramientas">
                                <dl class="dl-horizontal">
                                    <dt>Nombre de la herramienta</dt>
                                    <dd>
                                        <input type="hidden" hidden class="form-control input-mask" id="txt_idHerramienta" name="txt_idHerramienta" value="<%=res.getString("idherramienta")%>" />
                                        <input type="text" class="form-control input-mask" id="txt_nombre" name="txt_nombre" value="<%=res.getString("nombre")%>" /><span  id="mensaje" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Código</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_codigo" name="txt_codigo" value="<%=res.getString("codigo")%>"/><span  id="mensaje1" hidden>Complete este campo</span>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Descripción</dt>
                                    <dd>
                                        <input type="text" class="form-control input-mask"  id="txt_descripcion" name="txt_descripcion" value="<%=res.getString("descripcion")%>"/><span  id="mensaje2" hidden>Complete este campo</span>
                                    </dd>
                                </dl>

                                <div class="m-t-30" >
                                    <input type="submit" class="btn btn-success" value="Modificar" id="btnModificar" name="btnModificar" />
                                </div>
                            </form>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div> 
<%}%>
<%}%>
<%-- 
FIN_________MODIFICAR UNA HERRAMIENTA
--%>

<!--
LISTO EN UNA TABLA TODAS LAS HERRAMIENTAS
-->

<h3 class="text-center">HERRAMIENTAS <small></small></h3>
<table id="myTable" class="table table-bordered table-vmiddle">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Código</th>
            <th>Descripción</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        <%

        ctrHerramientas ch = new ctrHerramientas();
        ResultSet her = ch.listarHerramientas();

        while (her.next()) {
        %>
        <tr>
            <td><%=her.getString("nombre")%></td>
            <td><%=her.getString("codigo")%></td>
            <td><%=her.getString("descripcion")%></td>
            <td>
                <%
                if (her.getString("estado").equals("1")) {
                %>
                <button class="btn btn-success" onclick="estadop(0,<%=her.getString("idherramienta")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                <%} else {%>
                <button class="btn btn-danger" onclick="estadop(1,<%=her.getString("idherramienta")%>)"><i class="zmdi zmdi-refresh-sync"></i></button>
                <%}%>
                <a href="consultarHerramientas.jsp?accion=<%=her.getString("idherramienta")%>" class="btn btn-primary"><i class="zmdi zmdi-edit"></i></a>
            </td>
        </tr>  
        <%}%>
    </tbody>
</table>

<div class="row">
   <div class="col-md-5"></div>
   <div class="col-md-4">
    <a href="registrarHerramienta.jsp">
    <button type="button" class="btn btn-success">Registrar Herramientas</button>
    </a>
</div>
<div class="col-md-3"></div>
</div>

<!--
FIN DEL LISTADO DE LAS HERRAMIENTAS
-->
<jsp:include page="footer.jsp"/>
<script>
    //CAMBIAR ESTADO DE LA HERRAMIENTA 
    function estadop(estado, id) {
        swal({
            title: "estas seguro?",
            text: "Esto no es reversible!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Cambiar!",
            cancelButtonText: "No, Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            // swal("Eliminado!", "tu registro a sido cambiado.", "success");
            if (isConfirm) {
                $.ajax({
                    type: 'POST',
                    dataType: 'text',
                    data: {estado: estado, id: id},
                    url: "../ctrHerramientas"
                }).done(function (data) {
                    if (data) {
                        swal("ESTADO", "Su registro ha cambiado de estado", "info");
                        setTimeout("location.reload()", 3000);
                    }
                }).fail(function () {

                });
                return true;

            } else {
                swal("Cancelado", "cancelado por el usuario :)", "error");
            }
        });
    }

    //FIN CAMBIO DE ESTADO DE LA HERRAMIENTA
</script>

<script>
    $(document).ready(function () {
        $("#btnModificar").click(function () {
            var nombre = $("#txt_nombre").val();
            var serial = $("#txt_codigo").val();
            var des = $("#txt_descripcion").val();
            if (nombre == "" || serial == "" || des == "") {
                if (nombre == "") {
                    $("#mensaje").show();

                } else {
                    $("#mensaje").hide();
                }
                if (serial == "") {
                    $("#mensaje1").show();

                } else {
                    $("#mensaje1").hide();
                }
                if (des == "") {
                    $("#mensaje2").show();
                } else {
                    $("#mensaje2").hide();
                }                
                return false;              
            }
        });
    }
    );
</script>