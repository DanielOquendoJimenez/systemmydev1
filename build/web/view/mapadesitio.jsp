
<%
    HttpSession ht = request.getSession();
    if (ht.getAttribute("nombre") != null) {
%>

<%-- 
    Document   : mapadesitio
    Created on : 11/03/2016, 05:36:30 PM
    Author     : Maychan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="header.jsp"/>

<!-- Mostrar Mapa de sitio -->

<div class="container c-boxed">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="tile">
            <div class="t-header th-alt bg-blue">
                <div class="th-title text-center"><h3>Mapa de Sitio</h3></div>
            </div>
        </div>

        <div class="t-body tb-padding">             
            <div class="pmb-block">
                <div class="pmbb-body p-l-30">
                    <div class="pmbb-view">
                        <div class="container">                  
                            <div class="row">
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Personas </h2>
                                    <ul>
                                        <li><a href="registrarPersona.jsp">Registrar Personas</a></li>
                                        <li><a href="consultarPersona.jsp">Listar Personas</a></li>
                                        <li><a href="consultaDetalladaP.jsp">Consultar Personas</a></li>
                                    </ul>                                        
                                </div><!--/.col-xs-6.col-lg-4-->
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Plan </h2>
                                    <ul>
                                        <li><a href="plan.jsp">Registrar Plan</a></li>
                                        <li><a href="consultarPlan.jsp">Ver planes</a></li>
                                        <li><a href="registrarPromocion.jsp">Promociones</a></li>
                                    </ul>                                        
                                </div><!--/.col-xs-6.col-lg-4-->
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Cronograma </h2>
                                    <ul>
                                        <li><a href="cronograma.jsp">Cronograma</a></li>
                                        <li><a href="clase.jsp">Clases</a></li>
                                        <li><a href="horario.jsp">Horario</a></li>
                                    </ul>                                        
                                </div><!--/.col-xs-6.col-lg-4-->
                            </div>
                            <div class="row">                                        
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Encuesta </h2>
                                    <ul>
                                        <li><a href="registrarEncuesta.jsp"> Registrar Encuesta </a></li>
                                        <li><a href="preguntas.jsp"> Registrar Pregunta </a></li>
                                        <li><a href="consultarPreguntas.jsp"> Ver Pregunta </a></li>
                                    </ul>                                     
                                </div><!--/.col-xs-6.col-lg-4-->
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Mantenimiento </h2>
                                    <ul>
                                        <li><a href="registrarMantenimiento.jsp">Registrar Mantenimiento</a></li>
                                        <li><a href="consultarMantenimiento.jsp">Ver Mantenimientos</a></li>
                                    </ul>                                     
                                </div><!--/.col-xs-6.col-lg-4-->
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Herramientas </h2>
                                    <ul>
                                        <li><a href="registrarHerramienta.jsp"> Registrar Herramientas </a></li>
                                        <li><a href="consultarHerramientas.jsp"> Ver Herramientas </a></li>
                                    </ul>                                       
                                </div><!--/.col-xs-6.col-lg-4-->
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Valoración </h2>
                                    <ul>
                                        <li><a href="registrarItemValoracion.jsp"> Registrar Item Valoración </a></li>
                                        <li><a href="consultarItemValoracion.jsp"> Ver Valoración </a></li>
                                        <li><a href="my-address.html"> Registrar Valoración</a></li>  
                                        <li><a href="horarioXdia.jsp"> Horario por dia </a></li>
                                    </ul>                                        
                                </div><!--/.col-xs-6.col-lg-4-->
                                <div class="col-xs-6 col-lg-4">
                                    <h2> Plan Nutricional </h2>
                                    <ul>
                                        <li><a href="alimento.jsp">Alimento</a></li>
                                        <li><a href="registrarProducto.jsp">Registrar Productos</a></li>
                                        <li><a href="consultarProducto.jsp">Lista Productos</a></li>     
                                    </ul>                                        
                                </div><!--/.col-xs-6.col-lg-4-->
                                <div class="col-xs-6 col-lg-4">
                                    <h2>  Plan Entrenamiento </h2>
                                    <ul>
                                        <li><a href="xxxx.jsp"> Registrar Plan Nutricional </a></li>
                                        <li><a href="xxxx"> Ver Plan Nutricional </a></li>
                                    </ul>                                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Cierra Mapa de sitio -->

    </div>
</div>

<jsp:include page="footer.jsp"/>

<%
    } else {
        response.sendRedirect("../index.jsp");
    }
%>


