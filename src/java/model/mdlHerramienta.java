package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author yeisson
 */
public class mdlHerramienta {

    private String nombreH;
    private String codigoH;
    private String descripcionH;
    private int estadoH;
    private int idherramienta;
    private Connection con;

    public mdlHerramienta() throws ClassNotFoundException, SQLException {
        conexion conn = new conexion();
        this.con = conn.conectar();
    }

    public String getNombreH() {
        return nombreH;
    }

    public void setNombreH(String nombreH) {
        this.nombreH = nombreH;
    }

    public String getCodigoH() {
        return codigoH;
    }

    public void setCodigoH(String codigoH) {
        this.codigoH = codigoH;
    }

    public String getDescripcionH() {
        return descripcionH;
    }

    public void setDescripcionH(String descripcionH) {
        this.descripcionH = descripcionH;
    }

    public int getEstadoH() {
        return estadoH;
    }

    public void setEstadoH(int estadoH) {
        this.estadoH = estadoH;
    }

    public int getIdherramienta() {
        return idherramienta;
    }

    public void setIdherramienta(int idherramienta) {
        this.idherramienta = idherramienta;
    }

    public int insertarHerramienta() throws SQLException {
        String query = "CALL sp_registrar_herramienta(?,?,?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setString(1, this.getNombreH());
        cal.setString(2, this.getCodigoH());
        cal.setString(3, this.getDescripcionH());
        cal.setInt(4, this.getEstadoH());
        int resi = cal.executeUpdate();
        return resi;
    }
 // yeisson

    public ResultSet listarHerramienta() throws SQLException {
        String query = "CALL sp_listar_herramienta()";
        CallableStatement cal = this.con.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int cambiarEstado() throws SQLException {
        String query = "CALL sp_cambiar_estado_herramienta(?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setInt(1, this.getEstadoH());
        cal.setInt(2, this.getIdherramienta());
        int reso = cal.executeUpdate();
        return reso;
    }

    public int modificarHerramienta() throws SQLException {
        String query = "CALL sp_modificar_herramienta(?,?,?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setString(1, this.getNombreH());
        cal.setString(2, this.getCodigoH());
        cal.setString(3, this.getDescripcionH());
        cal.setInt(4, this.getIdherramienta());
        int resu = cal.executeUpdate();
        return resu;
    }

    public ResultSet listarHerramientaId() throws SQLException {
        String query = "CALL sp_listar_herramienta_id(?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setInt(1, this.getIdherramienta());
        ResultSet rs = cal.executeQuery();
        return rs;
    }

}
