package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author mayerly
 */
public class mdlItems {

    private String nombreI;
    private int estadoh;
    private int idHorario;
    private String du;
    private Connection conn;

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public mdlItems() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();
    }

    public String getNombreI() {
        return nombreI;
    }

    public void setNombreI(String nombreI) {
        this.nombreI = nombreI;
    }

    public int getEstadoh() {
        return estadoh;
    }

    public void setEstadoh(int estadoh) {
        this.estadoh = estadoh;
    }

    //    Registrar  horarioxdia
    public int insertarHorario() throws SQLException {
        String query = "CALL sp_insertar_horarioXdia(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombreI());
        cal.setInt(2, this.getEstadoh());
        int ris = cal.executeUpdate();
        return ris;
    }
    //    Cierra registrar horarioxdia

    //    Listar  horarioxdia
    public ResultSet listarHorario() throws SQLException {
        String query = "CALL sp_consultar_horario_x_dia()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }
    //    Cierra listar  horarioxdia

//    cambiar estado horarioxdia
    public int cambiarHxd() throws SQLException {
        String query = "Call sp_cambiar_estado_hxdia(?,?)";
        CallableStatement cl = this.conn.prepareCall(query);
        cl.setInt(2, this.getIdHorario());
        cl.setInt(1, this.getEstadoh());
        int el = cl.executeUpdate();
        return el;
    }
    //    cierra cambiar estado horarioxdia

//    modificar horariosdia
    public int modificarHorarioxdia() throws SQLException {
        String query = "Call sp_modificar_horarioxdia(?,?)";
        CallableStatement hr = this.conn.prepareCall(query);
        hr.setString(1, this.getNombreI());
        hr.setInt(2, this.getIdHorario());
        int eh = hr.executeUpdate();
        return eh;
    }
//    cierra modificar horrioxdia
    
    public ResultSet listaritems() throws SQLException {
        String query = "CALL sp_consultar_items_valoracion()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }
}
