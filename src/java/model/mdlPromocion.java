/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author daniel
 */
public class mdlPromocion {

    private int idpromociones;
    private String nombreP;
    private int valorP;
    private String restriccionP;
    private Connection conn;

    public mdlPromocion() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();

    }

    public int getIdpromociones() {
        return idpromociones;
    }

    public void setIdpromociones(int idpromociones) {
        this.idpromociones = idpromociones;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public int getValorP() {
        return valorP;
    }

    public void setValorP(int valorP) {
        this.valorP = valorP;
    }

    public String getRestriccionP() {
        return restriccionP;
    }

    public void setRestriccionP(String restriccionP) {
        this.restriccionP = restriccionP;
    }

    public int insertarPromocion() throws SQLException {
        String query = "CALL sp_insertar_promocion(?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombreP());
        cal.setInt(2, this.getValorP());
        cal.setString(3, this.getRestriccionP());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet consultarPromocion() throws SQLException {
        String query = "CALL sp_consultar_promocion()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int eliminarPromocion() throws SQLException {
        String query = "CALL sp_eliminar_promocion(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpromociones());
        int res = cal.executeUpdate();
        return res;
    }

}
