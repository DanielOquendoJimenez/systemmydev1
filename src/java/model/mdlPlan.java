/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.ResultSet;
import java.sql.CallableStatement;

/**
 *
 * @author daniel
 */
public class mdlPlan {

    private int idtipo_plan;
    private String nombreTp;
    private int idplan;
    private String nombre;
    private double valor;
    private double descuento;
    private int dias_cliente;
    private int idpersona;
    private String fecha_inicio;
    private String fecha_final;
    private int idpromociones;
    private int estado;

    private Connection conn;

    public mdlPlan() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();
    }

    public String getNombreTp() {
        return nombreTp;
    }

    public void setNombreTp(String nombreTp) {
        this.nombreTp = nombreTp;
    }

    public int getIdplan() {
        return idplan;
    }

    public void setIdplan(int idplan) {
        this.idplan = idplan;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public int getDias_cliente() {
        return dias_cliente;
    }

    public void setDias_cliente(int dias_cliente) {
        this.dias_cliente = dias_cliente;
    }

    public int getIdtipo_plan() {
        return idtipo_plan;
    }

    public void setIdtipo_plan(int idtipo_plan) {
        this.idtipo_plan = idtipo_plan;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getIdpromociones() {
        return idpromociones;
    }

    public void setIdpromociones(int idpromociones) {
        this.idpromociones = idpromociones;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    public ResultSet listarTp() throws SQLException {
        String query = "CALL sp_consultar_tipo_plan()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet rs = cal.executeQuery();
        return rs;
    }

    public int registrarPlan() throws SQLException {
        String query = "CALL sp_insertar_plan(?,?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombre());
        cal.setDouble(2, this.getValor());
        cal.setDouble(3, this.getDescuento());
        cal.setInt(4, this.getDias_cliente());
        cal.setInt(5, this.getIdtipo_plan());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet listarPlan() throws SQLException {
        String query = "CALL sp_listar_plan()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int registrarPlanPersona() throws SQLException {
        String query = "CALL sp_insertar_plan_persona(?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getFecha_inicio());
        cal.setInt(2, this.getIdplan());
        cal.setInt(3, this.getIdpersona());
        cal.setString(4, this.getFecha_final());
        int res = cal.executeUpdate();
        return res;
    }

    public int cambiarEstado() throws SQLException {
        String query = "CALL sp_cambiar_estado_plan(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getEstado());
        cal.setInt(2, this.getIdplan());
        int res = cal.executeUpdate();
        return res;
    }

    public int modificarPlan() throws SQLException {
        String query = "CALL sp_modificar_plan(?,?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombre());
        cal.setDouble(2, this.getValor());
        cal.setDouble(3, this.getDescuento());
        cal.setInt(4, this.getDias_cliente());
        cal.setInt(5, this.getIdplan());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet listarPlanId() throws SQLException {
        String query = "CALL sp_listar_plan_id(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdplan());
        ResultSet rs = cal.executeQuery();
        return rs;
    }

    public ResultSet optenetFechaAc() throws SQLException {
        String query = "CALL sp_optener_plan_actual(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpersona());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public ResultSet optenerUltimoId() throws SQLException {
        String query = "CALL sp_optener_ultimo_id_plan()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;

    }

    public int insertarPlanPromocion() throws SQLException {
        String query = "CALL sp_agregar_promocion_plan(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpromociones());
        cal.setInt(2, this.getIdplan());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet listarDetallePromocion() throws SQLException {
        String query = "CALL sp_consultar_detalle_promocion(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdplan());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public ResultSet consultarPlanesPromociones() throws SQLException {
        String query = "CALL sp_consultar_plan_promociones(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdplan());
        ResultSet res = cal.executeQuery();
        return res;
    } 

    public ResultSet consultarFechaFinal() throws SQLException {
        String query = "CALL sp_consultar_plan_promociones(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdplan());
        ResultSet res = cal.executeQuery();
        return res;
    }
}
