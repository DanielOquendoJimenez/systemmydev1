/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.ResultSet;
import java.sql.CallableStatement;

/**
 *
 * @author Maychan
 */
public class mdlAlimento {

    private int idalimento;
    private String nombreAlimento;
    private String DescripAlimen;
    private int estado;
    private Connection coni;

    public mdlAlimento() throws ClassNotFoundException, SQLException {
        conexion conii = new conexion();
        this.coni = conii.conectar();
    }

    public int getIdalimento() {
        return idalimento;
    }

    public void setIdalimento(int idalimento) {
        this.idalimento = idalimento;
    }

    public String getNombreAlimento() {
        return nombreAlimento;
    }

    public void setNombreAlimento(String nombreAlimento) {
        this.nombreAlimento = nombreAlimento;
    }

    public String getDescripAlimen() {
        return DescripAlimen;
    }

    public void setDescripAlimen(String DescripAlimen) {
        this.DescripAlimen = DescripAlimen;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

//    Listar Alimento    
    public ResultSet listarAlimento() throws SQLException {
        String query = "CALL sp_consultar_alimento()";
        CallableStatement cal = this.coni.prepareCall(query);
        ResultSet rsrt = cal.executeQuery();
        return rsrt;
    }
    //    Cierra listar Alimento

    //    Editar Alimento
    public ResultSet editarConsultarAlimento() throws SQLException {
        String query = "CALL sp_consultar_editar_alimento(?)";
        CallableStatement cal = this.coni.prepareCall(query);
        cal.setInt(1, this.getIdalimento());
        ResultSet rsrt = cal.executeQuery();
        return rsrt;
    }
    //    Cierra editar Alimento

    //    Registar Alimento
    public int insertarAlimento() throws SQLException {
        String query = "CALL sp_registrar_alimento(?,?,?)";
        CallableStatement call = this.coni.prepareCall(query);
        call.setString(1, this.getNombreAlimento());
        call.setString(2, this.getDescripAlimen());
        call.setInt(3, this.getEstado());
        int resuel = call.executeUpdate();
        return resuel;
    }
    //    Cierra registrar Alimento

    //    Modificar Alimento
    public int modificarAlimento() throws SQLException {
        String query = "Call sp_modificar_alimento(?,?,?)";
        CallableStatement hr = this.coni.prepareCall(query);
        hr.setString(1, this.getNombreAlimento());
        hr.setString(2, this.getDescripAlimen());
        hr.setInt(3, this.getIdalimento());
        int eh = hr.executeUpdate();
        return eh;
    }
//    Cierra modificar Alimento

    //    Cambiar Alimento
    public int cambiarAlimento() throws SQLException {
        String query = "Call sp_cambiar_estado_alimento(?,?)";
        CallableStatement cl = this.coni.prepareCall(query);
        cl.setInt(2, this.getIdalimento());
        cl.setInt(1, this.getEstado());
        int ely = cl.executeUpdate();
        return ely;
    }
    //    Cierra cambiar Alimento

}
