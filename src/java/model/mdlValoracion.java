/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import library.conexion;

/**
 *
 * @author daniel
 */
public class mdlValoracion {

    private int idvaloracion;
    private String fecha_valoracion;
    private String objetivo;
    private int idpersona;
    private int iditem_valoracion;
    private int iditem_detalle_valoracion;
    private String resultado_item;
    private Connection conn;

    public mdlValoracion() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();
    }

    public int getIdvaloracion() {
        return idvaloracion;
    }

    public void setIdvaloracion(int idvaloracion) {
        this.idvaloracion = idvaloracion;
    }

    public String getFecha_valoracion() {
        return fecha_valoracion;
    }

    public void setFecha_valoracion(String fecha_valoracion) {
        this.fecha_valoracion = fecha_valoracion;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public int getIditem_valoracion() {
        return iditem_valoracion;
    }

    public void setIditem_valoracion(int iditem_valoracion) {
        this.iditem_valoracion = iditem_valoracion;
    }

    public String getResultado_item() {
        return resultado_item;
    }

    public void setResultado_item(String resultado_item) {
        this.resultado_item = resultado_item;
    }

    public int getIditem_detalle_valoracion() {
        return iditem_detalle_valoracion;
    }

    public void setIditem_detalle_valoracion(int iditem_detalle_valoracion) {
        this.iditem_detalle_valoracion = iditem_detalle_valoracion;
    }

    public ResultSet graficaPeso() throws SQLException {
        String query = "CALL sp_consultar_valoracion(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpersona());
        ResultSet res = cal.executeQuery();
        return res;
    }
    
     public ResultSet graficaMasa() throws SQLException {
        String query = "CALL sp_consultar_valoracion(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpersona());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int insertarValoracion() throws SQLException {
        String query = "CALL sp_insertar_valoracion(?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getFecha_valoracion());
        cal.setString(2, this.getObjetivo());
        cal.setInt(3, this.getIdpersona());
        int res = cal.executeUpdate();
        return res;
    }

    public int insertarValoracionPersona() throws SQLException {// insertar la valoracion
        String query = "CALL sp_insertar_valoracion_persona(?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getResultado_item());
        cal.setInt(2, this.getIdvaloracion());
        cal.setInt(3, this.getIditem_valoracion());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet consultarUltimoId() throws SQLException {//optener el ultimo id de la valoracion
        String query = "CALL sp_consultar_ultimo_id_valoracion()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int insertarValoracionDetalle() throws SQLException {// insertar la valoracion en el detalle
        String query = "CALL sp_insertar_valoracion_persona(?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getResultado_item());
        cal.setInt(2, this.getIdvaloracion());
        cal.setInt(3, this.getIditem_valoracion());
        int res = cal.executeUpdate();
        return res;

    }

    public ResultSet consultarValoracionPersona() throws SQLException {//listar tabla con item
        String query = "CALL sp_consultar_detalle_valoracion(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpersona());
        cal.setInt(2, this.getIdvaloracion());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int eliminarItemidValoracio() throws SQLException {//eliminar item de la tabla
        String query = "CALL sp_eliminar_item_detalle_valoracion(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIditem_detalle_valoracion());
        int res = cal.executeUpdate();
        return res;
    }

}
