package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.ResultSet;
import java.sql.CallableStatement;
/**
 *
 * @author daniel
 */
public class mdlLogin {

    private int idusuario;
    private String usuario;
    private String clave;
    private int idpersona;
    private int idtipo_persona;
    private int idroll;
    private String descripcion;
  

    private Connection con;

    public mdlLogin() throws ClassNotFoundException, SQLException {
        conexion conn = new conexion();
        this.con = conn.conectar();
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public int getIdtipo_persona() {
        return idtipo_persona;
    }

    public void setIdtipo_persona(int idtipo_persona) {
        this.idtipo_persona = idtipo_persona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdroll() {
        return idroll;
    }

    public void setIdroll(int idroll) {
        this.idroll = idroll;
    }
    
    
    public ResultSet validarUsuario() throws SQLException{
        String query ="CALL sp_validar_usuario(?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setString(1, this.getUsuario());
        ResultSet rs = cal.executeQuery();
        return rs;
    }
    
    public int registrarUsuario() throws SQLException{
    String query ="CALL sp_crear_login(?,?,?,?)";
    CallableStatement lo = this.con.prepareCall(query);
    lo.setString(1, this.getUsuario());
    lo.setString(2, this.getClave());
    lo.setInt(3, this.getIdpersona());
    lo.setInt(4, this.getIdroll());
    int resultado = lo.executeUpdate();
    return resultado;
    }
    
    public int modificarUsuario() throws SQLException{
    String query = "CALL sp_modifiacar_usuario(?,?)";
    CallableStatement cal = this.con.prepareCall(query);
    cal.setInt(1, this.getIdusuario());
    cal.setString(2, this.getClave());
    int res = cal.executeUpdate();
    return res;
    }
    

}
