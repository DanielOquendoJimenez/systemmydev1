/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import library.conexion;

/**
 *
 * @author Admiinistrador
 */
public class mdlHorarioXdia {

    private String nombreI;
    private int estado;
    private int idHorario;
    private Connection conn;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public mdlHorarioXdia() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();
    }

    public String getNombreI() {
        return nombreI;
    }

    public void setNombreI(String nombreI) {
        this.nombreI = nombreI;
    }

    public int insertarHorario() throws SQLException {
        String query = "CALL sp_insertar_horarioXdia(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombreI());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet listarHorario() throws SQLException {
        String query = "CALL sp_consultar_horario_x_dia()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int eliminarHorario() throws SQLException {
        String query = "Call sp_eliminar_horario(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdHorario());
        int eli = cal.executeUpdate();
        return eli;
    }

    public int cambiarEstado() throws SQLException {
        String query = "CALL sp_cambiar_estado_horario(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getEstado());
        cal.setInt(2, this.getIdHorario());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet listarHorarioId() throws SQLException {
        String query = "CALL sp_listar_horario_id(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdHorario());
        ResultSet rs = cal.executeQuery();
        return rs;
    }

    public int modificarHorario() throws SQLException {
        String query = "CALL sp_modificar_Horario(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombreI());       
        cal.setInt(2, this.getIdHorario());
        int res = cal.executeUpdate();
        return res;
    }

}
