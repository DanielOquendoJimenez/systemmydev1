package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.ResultSet;
import java.sql.CallableStatement;

/**
 *
 * @author daniel
 */
public class mdlPlanNutricional {

    private String fecha_plan_nutricional;
    private String porcion;
    private int idplan_nutricional;
    private int idvaloracion;
    private int idpersona;
    private int idHorarioDia;
    private int idAlimento;
    private int idDetallePlanNutricional;
    private Connection conn;

    public mdlPlanNutricional() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();
    }

    public String getFecha_plan_nutricional() {
        return fecha_plan_nutricional;
    }

    public void setFecha_plan_nutricional(String fecha_plan_nutricional) {
        this.fecha_plan_nutricional = fecha_plan_nutricional;
    }

    public int getIdplan_nutricional() {
        return idplan_nutricional;
    }

    public void setIdplan_nutricional(int idplan_nutricional) {
        this.idplan_nutricional = idplan_nutricional;
    }

    public int getIdvaloracion() {
        return idvaloracion;
    }

    public void setIdvaloracion(int idvaloracion) {
        this.idvaloracion = idvaloracion;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public String getPorcion() {
        return porcion;
    }

    public void setPorcion(String porcion) {
        this.porcion = porcion;
    }

    public int getIdHorarioDia() {
        return idHorarioDia;
    }

    public void setIdHorarioDia(int idHorarioDia) {
        this.idHorarioDia = idHorarioDia;
    }

    public int getIdAlimento() {
        return idAlimento;
    }

    public void setIdAlimento(int idAlimento) {
        this.idAlimento = idAlimento;
    }

    public int getIdDetallePlanNutricional() {
        return idDetallePlanNutricional;
    }

    public void setIdDetallePlanNutricional(int idDetallePlanNutricional) {
        this.idDetallePlanNutricional = idDetallePlanNutricional;
    }

    public ResultSet consultarValoraciones() throws SQLException {
        String query = "CALL sp_consultar_valoracion_dieta(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpersona());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public ResultSet consultarValoracion() throws SQLException {
        String query = "CALL sp_consultar_valoracion_plan_nutricional(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdvaloracion());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int insertarPlanNutricional() throws SQLException {
        String query = "CALL sp_insertar_plan_nutricional(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getFecha_plan_nutricional());
        cal.setInt(2, this.getIdvaloracion());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet consultarUltimoPlanN() throws SQLException {
        String query = "CALL sp_consultar_ultimo_id_Plan_nutricional()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public ResultSet consultarAlimento() throws SQLException {
        String query = "CALL sp_consultar_alimento()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public ResultSet ConsultarHorarioDia() throws SQLException {
        String query = "CALL sp_consultar_horario_x_dia()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int insertarDetallePlanNutricional() throws SQLException {
        String query = "CALL sp_insertar_detalle_plan_nutricional(?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getPorcion());
        cal.setInt(2, this.getIdplan_nutricional());
        cal.setInt(3, this.getIdAlimento());
        cal.setInt(4, this.getIdHorarioDia());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet cosultarDetallePlanNutricional() throws SQLException {
        String query = "CALL sp_consultar_detalle_plan_nutricional(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdplan_nutricional());
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int eliminarItemPlanNutricional() throws SQLException {
        String query = "CALL sp_eliminar_detalle_plan_nutricional(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdDetallePlanNutricional());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet planNutricionalCliente() throws SQLException {
        String query = "CALL sp_consultar_plan_nutricional_cliente(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdpersona());
        ResultSet res = cal.executeQuery();
        return res;
    }

}
