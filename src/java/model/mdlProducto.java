/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import library.conexion;

/**
 *
 * @author YEISSON
 */
public class mdlProducto {

    private String nombre;
    private String valor;
    private String descripcion;
    private String foto;
    private int idProducto;
    private Connection conn;
    private int estado;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public mdlProducto() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();

    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int insertarProducto() throws SQLException {
        String query = "CALL sp_insertar_producto(?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombre());
        cal.setString(2, this.getValor());
        cal.setString(3, this.getDescripcion());
        cal.setString(4, this.getFoto());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet listarProducto() throws SQLException {
        String query = "CALL sp_consultar_producto()";
        CallableStatement cal = this.conn.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int eliminarProducto() throws SQLException {
        String query = "Call sp_eliminar_producto(?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getIdProducto());
        int eli = cal.executeUpdate();
        return eli;
    }

    public ResultSet consultarPm() throws SQLException {
        String query = "CALL sp_consultar_idproducto_modificar(?)";
        CallableStatement im = this.conn.prepareCall(query);
        im.setInt(1, this.getIdProducto());
        ResultSet rs = im.executeQuery();
        return rs;
    }

    public ResultSet consultarP() throws SQLException {
        String query = "CALL sp_consultar_producto()";
        CallableStatement cp = this.conn.prepareCall(query);
        ResultSet rsl = cp.executeQuery();
        return rsl;
    }

    public int modificarP() throws SQLException {
        String query = "CALL sp_modificar_producto(?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getNombre());
        cal.setString(2, this.getValor());
        cal.setString(3, this.getDescripcion());
        cal.setInt(4, this.getIdProducto());
        int md = cal.executeUpdate();
        return md;
    }

    public int cambiarEstado() throws SQLException {
        String query = "CALL sp_cambiar_estado_producto(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setInt(1, this.getEstado());
        cal.setInt(2, this.getIdProducto());
        int res = cal.executeUpdate();
        return res;
    }

}
