/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author daniel
 */
public class mdlPlanEntrenamiento {

    private String objetivo;
    private String fechaPlanEntrenamieno;
    private int idPersona;
    private String fechaInicio;
    private String fechaFinal;
    private int idPlanEntrenamiento;
    private int idCronograma;
    private Connection con;

    public mdlPlanEntrenamiento() throws ClassNotFoundException, SQLException {
        conexion conn = new conexion();
        this.con = conn.conectar();
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getFechaPlanEntrenamieno() {
        return fechaPlanEntrenamieno;
    }

    public void setFechaPlanEntrenamieno(String fechaPlanEntrenamieno) {
        this.fechaPlanEntrenamieno = fechaPlanEntrenamieno;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public int getIdPlanEntrenamiento() {
        return idPlanEntrenamiento;
    }

    public void setIdPlanEntrenamiento(int idPlanEntrenamiento) {
        this.idPlanEntrenamiento = idPlanEntrenamiento;
    }

    public int getIdCronograma() {
        return idCronograma;
    }

    public void setIdCronograma(int idCronograma) {
        this.idCronograma = idCronograma;
    }

    public ResultSet insertarPlanEntrenamiento() throws SQLException {
        String query = "CALL sp_insertar_plan_entrenamiento(?,?,?)";
        CallableStatement insert = this.con.prepareCall(query);
        insert.setString(1, this.getFechaPlanEntrenamieno());
        insert.setString(2, this.getObjetivo());
        insert.setInt(3, this.getIdPersona());
        ResultSet execucion = insert.executeQuery();
        return execucion;
    }

    public int insertarDetalleEntrenamiento() throws SQLException {
        String query = "CALL sp_insertar_detalle_plan_entrenamiento(?,?,?,?)";
        CallableStatement insert = this.con.prepareCall(query);
        insert.setString(1, this.getFechaInicio());
        insert.setString(2, this.getFechaFinal());
        insert.setInt(3, this.getIdPlanEntrenamiento());
        insert.setInt(4, this.getIdCronograma());
        int execute = insert.executeUpdate();
        return execute;
    }

    public ResultSet consultarPlanEntrenamientoCliente() throws SQLException {
        String query = "CALL sp_consultar_plan_entrenamiento(?)";
        CallableStatement find = this.con.prepareCall(query);
        find.setInt(1, this.getIdPersona());
        ResultSet result = find.executeQuery();
        return result;
    }
}
