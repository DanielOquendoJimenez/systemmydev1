/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author daniel
 */
public class mdlExtras {

    public mdlExtras() {

    }
    private String fecha;
    private String email;
    private String usuarioEmail;
    private String claveEmail;
    private int diasFecha;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuarioEmail() {
        return usuarioEmail;
    }

    public void setUsuarioEmail(String usuarioEmail) {
        this.usuarioEmail = usuarioEmail;
    }

    public String getClaveEmail() {
        return claveEmail;
    }

    public void setClaveEmail(String claveEmail) {
        this.claveEmail = claveEmail;
    }

    public int getDiasFecha() {
        return diasFecha;
    }

    public void setDiasFecha(int diasFecha) {
        this.diasFecha = diasFecha;
    }

    public String convertirFecha() {//convertir String a fecha
        String res = "";
        SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            res = myFormat.format(fromUser.parse(this.getFecha()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String revertirFecha() {//convertir String a fecha
        String res = "";
        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            res = myFormat.format(fromUser.parse(this.getFecha()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }

    public String enviarEmails() {// enviar email
        String result;
        //destinatario de correo electrónico.
        String to = this.getEmail();

        // remitente de correo electrónico
        String from = "djoquendo@misena.edu.co";

        // Suponiendo que usted está enviando correo desde localhost
        String host = "ec2-35-160-158-243.us-west-2.compute.amazonaws.com";

        // Recibe las propiedades del sistema objeto
        Properties properties = System.getProperties();

        // Configuración del servidor de correo
        properties.setProperty("mail.smtp.host", host);

        // Obtener el objeto Session defecto
        Session mailSession = Session.getDefaultInstance(properties);

        try {
            // Crear un objeto MimeMessage defecto.
            MimeMessage message = new MimeMessage(mailSession);
            // Conjunto De: campo de cabecera de la cabecera.
            message.setFrom(new InternetAddress(from));
            // Para: campo de cabecera de la cabecera.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
            if (this.getUsuarioEmail() != null) {
                // Set Asunto: campo de cabecera
                message.setSubject("Bienvenido a Ser Athletic");
                // Ahora configure el mensaje real
                message.setText("su usuario:" + this.getUsuarioEmail() + "\n"
                        + "su contraseña: " + this.getClaveEmail());
            } else {
                // Set Asunto: campo de cabecera
                message.setSubject("Cambio de Clave Ser Athletic");
                // Ahora configure el mensaje real
                message.setText("su contraseña: " + this.getClaveEmail());

            }
            // Enviar mensaje
            Transport.send(message);
            result = "Correo enviado";
        } catch (MessagingException mex) {
            mex.printStackTrace();
            result = "Error: Correo no enviado" + mex.getMessage();
        }
        return result;

    }

    public String enviarEmail() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.mailgun.org");// smtp.gmail.com
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("postmaster@sandbox4a1f99e37b7f40ca9183d14bb8049836.mailgun.org", "eb5b061df6bbbd4ef14fd21f0991e715");//"daniel.oquendo18@gmail.com", "daniel1995"
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("not-reply@serathletic.co"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(this.getEmail()));
            if (this.getUsuarioEmail() != null) {
                // Set Asunto: campo de cabecera
                message.setSubject("Bienvenido a Ser Athletic");
                // Ahora configure el mensaje real
                String contenido = "<!DOCTYPE html>\n"
                        + "<html>\n"
                        + "\n"
                        + "<head>\n"
                        + "    <meta name=\"viewport\" content=\"width=device-width\" />\n"
                        + "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                        + "	<title>Actionable emails e.g. reset password</title>\n"
                        + "	<style>\n"
                        + "		{\n"
                        + "			margin: 0;\n"
                        + "			padding: 0;\n"
                        + "			font-family: \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n"
                        + "			box-sizing: border-box;\n"
                        + "			font-size: 14px;\n"
                        + "		}\n"
                        + "		img {\n"
                        + "			max-width: 100%;\n"
                        + "		}\n"
                        + "		body {\n"
                        + "			-webkit-font-smoothing: antialiased;\n"
                        + "			-webkit-text-size-adjust: none;\n"
                        + "			width: 100% !important;\n"
                        + "			height: 100%;\n"
                        + "			line-height: 1.6;\n"
                        + "		}\n"
                        + "		table td {\n"
                        + "			vertical-align: top;\n"
                        + "		}\n"
                        + "		body {\n"
                        + "			background-color: #f6f6f6;\n"
                        + "		}\n"
                        + "		.body-wrap {\n"
                        + "			background-color: #f6f6f6;\n"
                        + "			width: 100%;\n"
                        + "		}\n"
                        + "		.container {\n"
                        + "			display: block !important;\n"
                        + "			max-width: 600px !important;\n"
                        + "			margin: 0 auto !important;\n"
                        + "			clear: both !important;\n"
                        + "		}\n"
                        + "		.content {\n"
                        + "			max-width: 600px;\n"
                        + "			margin: 0 auto;\n"
                        + "			display: block;\n"
                        + "			padding: 20px;\n"
                        + "		}\n"
                        + "		.main {\n"
                        + "			background: #fff;\n"
                        + "			border: 1px solid #e9e9e9;\n"
                        + "			border-radius: 3px;\n"
                        + "		}\n"
                        + "		.content-wrap {\n"
                        + "			padding: 20px;\n"
                        + "		}\n"
                        + "		.content-block {\n"
                        + "			padding: 0 0 20px;\n"
                        + "		}\n"
                        + "		.header {\n"
                        + "			width: 100%;\n"
                        + "			margin-bottom: 20px;\n"
                        + "		}\n"
                        + "		.footer {\n"
                        + "			width: 100%;\n"
                        + "			clear: both;\n"
                        + "			color: #999;\n"
                        + "			padding: 20px;\n"
                        + "		}\n"
                        + "		.footer a {\n"
                        + "			color: #999;\n"
                        + "		}\n"
                        + "		.footer p,\n"
                        + "		.footer a,\n"
                        + "		.footer unsubscribe,\n"
                        + "		.footer td {\n"
                        + "			font-size: 12px;\n"
                        + "		}\n"
                        + "		.column-left {\n"
                        + "			float: left;\n"
                        + "			width: 50%;\n"
                        + "		}\n"
                        + "		.column-right {\n"
                        + "			float: left;\n"
                        + "			width: 50%;\n"
                        + "		}\n"
                        + "		h1,\n"
                        + "		h2,\n"
                        + "		h3 {\n"
                        + "			font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n"
                        + "			color: #000;\n"
                        + "			margin: 40px 0 0;\n"
                        + "			line-height: 1.2;\n"
                        + "			font-weight: 400;\n"
                        + "		}\n"
                        + "		h1 {\n"
                        + "			font-size: 32px;\n"
                        + "			font-weight: 500;\n"
                        + "		}\n"
                        + "		h2 {\n"
                        + "			font-size: 24px;\n"
                        + "		}\n"
                        + "		h3 {\n"
                        + "			font-size: 18px;\n"
                        + "		}\n"
                        + "		h4 {\n"
                        + "			font-size: 14px;\n"
                        + "			font-weight: 600;\n"
                        + "		}\n"
                        + "		p,\n"
                        + "		ul,\n"
                        + "		ol {\n"
                        + "			margin-bottom: 10px;\n"
                        + "			font-weight: normal;\n"
                        + "		}\n"
                        + "		p li,\n"
                        + "		ul li,\n"
                        + "		ol li {\n"
                        + "			margin-left: 5px;\n"
                        + "			list-style-position: inside;\n"
                        + "		}\n"
                        + "		a {\n"
                        + "			color: #348eda;\n"
                        + "			text-decoration: underline;\n"
                        + "		}\n"
                        + "		.btn-primary {\n"
                        + "			text-decoration: none;\n"
                        + "			color: #FFF;\n"
                        + "			background-color: #348eda;\n"
                        + "			border: solid #348eda;\n"
                        + "			border-width: 10px 20px;\n"
                        + "			line-height: 2;\n"
                        + "			font-weight: bold;\n"
                        + "			text-align: center;\n"
                        + "			cursor: pointer;\n"
                        + "			display: inline-block;\n"
                        + "			border-radius: 5px;\n"
                        + "			text-transform: capitalize;\n"
                        + "		}\n"
                        + "		.last {\n"
                        + "			margin-bottom: 0;\n"
                        + "		}\n"
                        + "		.first {\n"
                        + "			margin-top: 0;\n"
                        + "		}\n"
                        + "		.padding {\n"
                        + "			padding: 10px 0;\n"
                        + "		}\n"
                        + "		.aligncenter {\n"
                        + "			text-align: center;\n"
                        + "		}\n"
                        + "		.alignright {\n"
                        + "			text-align: right;\n"
                        + "		}\n"
                        + "		.alignleft {\n"
                        + "			text-align: left;\n"
                        + "		}\n"
                        + "		.clear {\n"
                        + "			clear: both;\n"
                        + "		}\n"
                        + "		.alert {\n"
                        + "			font-size: 16px;\n"
                        + "			color: #fff;\n"
                        + "			font-weight: 500;\n"
                        + "			padding: 20px;\n"
                        + "			text-align: center;\n"
                        + "			border-radius: 3px 3px 0 0;\n"
                        + "		}\n"
                        + "		.alert a {\n"
                        + "			color: #fff;\n"
                        + "			text-decoration: none;\n"
                        + "			font-weight: 500;\n"
                        + "			font-size: 16px;\n"
                        + "		}\n"
                        + "		.alert.alert-warning {\n"
                        + "			background: #ff9f00;\n"
                        + "		}\n"
                        + "		.alert.alert-bad {\n"
                        + "			background: #d0021b;\n"
                        + "		}\n"
                        + "		.alert.alert-good {\n"
                        + "			background: #68b90f;\n"
                        + "		}\n"
                        + "		.invoice {\n"
                        + "			margin: 40px auto;\n"
                        + "			text-align: left;\n"
                        + "			width: 80%;\n"
                        + "		}\n"
                        + "		.invoice td {\n"
                        + "			padding: 5px 0;\n"
                        + "		}\n"
                        + "		.invoice .invoice-items {\n"
                        + "			width: 100%;\n"
                        + "		}\n"
                        + "		.invoice .invoice-items td {\n"
                        + "			border-top: #eee 1px solid;\n"
                        + "		}\n"
                        + "		.invoice .invoice-items .total td {\n"
                        + "			border-top: 2px solid #333;\n"
                        + "			border-bottom: 2px solid #333;\n"
                        + "			font-weight: 700;\n"
                        + "		}\n"
                        + "		@media only screen and (max-width: 640px) {\n"
                        + "			h1, h2, h3, h4 {\n"
                        + "				font-weight: 600 !important;\n"
                        + "				margin: 20px 0 5px !important;\n"
                        + "			}\n"
                        + "			h1 {\n"
                        + "				font-size: 22px !important;\n"
                        + "			}\n"
                        + "			h2 {\n"
                        + "				font-size: 18px !important;\n"
                        + "			}\n"
                        + "			h3 {\n"
                        + "				font-size: 16px !important;\n"
                        + "			}\n"
                        + "			.container {\n"
                        + "				width: 100% !important;\n"
                        + "			}\n"
                        + "			.content,\n"
                        + "			.content-wrapper {\n"
                        + "				padding: 10px !important;\n"
                        + "			}\n"
                        + "			h3 {\n"
                        + "				font-size: 32px!important;\n"
                        + "				font-weight: bold!important;\n"
                        + "				margin-top: 0px!important;\n"
                        + "			}\n"
                        + "			.invoice {\n"
                        + "				width: 100% !important;\n"
                        + "			}\n"
                        + "		}\n"
                        + "	</style>\n"
                        + "</head>\n"
                        + "\n"
                        + "<body>\n"
                        + "	<table class=\"body-wrap\">\n"
                        + "		<tr>\n"
                        + "			<td></td>\n"
                        + "			<td class=\"container\" width=\"600\">\n"
                        + "				<div class=\"content\">\n"
                        + "					<table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                        + "						<tr>\n"
                        + "							<td class=\"content-wrap\">\n"
                        + "								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                        + "									<tr>\n"
                        + "										<td class=\"content-block\" style=\"text-align:center;\">\n"
                        + "											<img src=\"http://static.wixstatic.com/media/f132ad_e2ee21e4ec30e6094818c36e1051531d.gif_1024\" width=\"200px\">\n"
                        + "											<h3>Bienvenido a Ser Athletic</h3> \n"
                        + "										</td>\n"
                        + "									</tr>\n"
                        + "									<tr style=\"text-align:center\">\n"
                        + "										<td class=\"content-block\">Bienvenido estos son sus credenciales para ingresar al sistema</td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "            							<td class=\"content-block\">Usuario: " + this.getUsuarioEmail() + "</td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "    									<td class=\"content-block\">Contraseña: " + this.getClaveEmail() + "</td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "        								<td class=\"content-block\"></td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "            							<td class=\"content-block\"></td>\n"
                        + "									</tr>\n"
                        + "									<tr style=\"text-align:center;\">\n"
                        + "										<td class=\"content-block\"> <a class=\"btn-primary\" href=\"http://ec2-35-160-158-243.us-west-2.compute.amazonaws.com:8080/SystemMYDEv1/\">No responda este mensaje</a> \n"
                        + "										</td>\n"
                        + "									</tr>\n"
                        + "									<tr>\n"
                        + "									</tr>\n"
                        + "								</table>\n"
                        + "							</td>\n"
                        + "						</tr>\n"
                        + "					</table>\n"
                        + "					<div class=\"footer\">\n"
                        + "						<table width=\"100%\">\n"
                        + "							<tr>\n"
                        + "								<td class=\"aligncenter content-block\"></td>\n"
                        + "							</tr>\n"
                        + "						</table>\n"
                        + "					</div>\n"
                        + "				</div>\n"
                        + "			</td>\n"
                        + "			<td></td>\n"
                        + "		</tr>\n"
                        + "	</table>\n"
                        + "</body>\n"
                        + "\n"
                        + "</html>";
                message.setContent(contenido, "text/html; charset=utf-8");
            } else {
                // Set Asunto: campo de cabecera
                message.setSubject("Cambio de Clave Ser Athletic");
                // Ahora configure el mensaje real
                String contenido = "<!DOCTYPE html>\n"
                        + "<html>\n"
                        + "\n"
                        + "<head>\n"
                        + "    <meta name=\"viewport\" content=\"width=device-width\" />\n"
                        + "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                        + "	<title>Actionable emails e.g. reset password</title>\n"
                        + "	<style>\n"
                        + "		{\n"
                        + "			margin: 0;\n"
                        + "			padding: 0;\n"
                        + "			font-family: \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n"
                        + "			box-sizing: border-box;\n"
                        + "			font-size: 14px;\n"
                        + "		}\n"
                        + "		img {\n"
                        + "			max-width: 100%;\n"
                        + "		}\n"
                        + "		body {\n"
                        + "			-webkit-font-smoothing: antialiased;\n"
                        + "			-webkit-text-size-adjust: none;\n"
                        + "			width: 100% !important;\n"
                        + "			height: 100%;\n"
                        + "			line-height: 1.6;\n"
                        + "		}\n"
                        + "		table td {\n"
                        + "			vertical-align: top;\n"
                        + "		}\n"
                        + "		body {\n"
                        + "			background-color: #f6f6f6;\n"
                        + "		}\n"
                        + "		.body-wrap {\n"
                        + "			background-color: #f6f6f6;\n"
                        + "			width: 100%;\n"
                        + "		}\n"
                        + "		.container {\n"
                        + "			display: block !important;\n"
                        + "			max-width: 600px !important;\n"
                        + "			margin: 0 auto !important;\n"
                        + "			clear: both !important;\n"
                        + "		}\n"
                        + "		.content {\n"
                        + "			max-width: 600px;\n"
                        + "			margin: 0 auto;\n"
                        + "			display: block;\n"
                        + "			padding: 20px;\n"
                        + "		}\n"
                        + "		.main {\n"
                        + "			background: #fff;\n"
                        + "			border: 1px solid #e9e9e9;\n"
                        + "			border-radius: 3px;\n"
                        + "		}\n"
                        + "		.content-wrap {\n"
                        + "			padding: 20px;\n"
                        + "		}\n"
                        + "		.content-block {\n"
                        + "			padding: 0 0 20px;\n"
                        + "		}\n"
                        + "		.header {\n"
                        + "			width: 100%;\n"
                        + "			margin-bottom: 20px;\n"
                        + "		}\n"
                        + "		.footer {\n"
                        + "			width: 100%;\n"
                        + "			clear: both;\n"
                        + "			color: #999;\n"
                        + "			padding: 20px;\n"
                        + "		}\n"
                        + "		.footer a {\n"
                        + "			color: #999;\n"
                        + "		}\n"
                        + "		.footer p,\n"
                        + "		.footer a,\n"
                        + "		.footer unsubscribe,\n"
                        + "		.footer td {\n"
                        + "			font-size: 12px;\n"
                        + "		}\n"
                        + "		.column-left {\n"
                        + "			float: left;\n"
                        + "			width: 50%;\n"
                        + "		}\n"
                        + "		.column-right {\n"
                        + "			float: left;\n"
                        + "			width: 50%;\n"
                        + "		}\n"
                        + "		h1,\n"
                        + "		h2,\n"
                        + "		h3 {\n"
                        + "			font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n"
                        + "			color: #000;\n"
                        + "			margin: 40px 0 0;\n"
                        + "			line-height: 1.2;\n"
                        + "			font-weight: 400;\n"
                        + "		}\n"
                        + "		h1 {\n"
                        + "			font-size: 32px;\n"
                        + "			font-weight: 500;\n"
                        + "		}\n"
                        + "		h2 {\n"
                        + "			font-size: 24px;\n"
                        + "		}\n"
                        + "		h3 {\n"
                        + "			font-size: 18px;\n"
                        + "		}\n"
                        + "		h4 {\n"
                        + "			font-size: 14px;\n"
                        + "			font-weight: 600;\n"
                        + "		}\n"
                        + "		p,\n"
                        + "		ul,\n"
                        + "		ol {\n"
                        + "			margin-bottom: 10px;\n"
                        + "			font-weight: normal;\n"
                        + "		}\n"
                        + "		p li,\n"
                        + "		ul li,\n"
                        + "		ol li {\n"
                        + "			margin-left: 5px;\n"
                        + "			list-style-position: inside;\n"
                        + "		}\n"
                        + "		a {\n"
                        + "			color: #348eda;\n"
                        + "			text-decoration: underline;\n"
                        + "		}\n"
                        + "		.btn-primary {\n"
                        + "			text-decoration: none;\n"
                        + "			color: #FFF;\n"
                        + "			background-color: #348eda;\n"
                        + "			border: solid #348eda;\n"
                        + "			border-width: 10px 20px;\n"
                        + "			line-height: 2;\n"
                        + "			font-weight: bold;\n"
                        + "			text-align: center;\n"
                        + "			cursor: pointer;\n"
                        + "			display: inline-block;\n"
                        + "			border-radius: 5px;\n"
                        + "			text-transform: capitalize;\n"
                        + "		}\n"
                        + "		.last {\n"
                        + "			margin-bottom: 0;\n"
                        + "		}\n"
                        + "		.first {\n"
                        + "			margin-top: 0;\n"
                        + "		}\n"
                        + "		.padding {\n"
                        + "			padding: 10px 0;\n"
                        + "		}\n"
                        + "		.aligncenter {\n"
                        + "			text-align: center;\n"
                        + "		}\n"
                        + "		.alignright {\n"
                        + "			text-align: right;\n"
                        + "		}\n"
                        + "		.alignleft {\n"
                        + "			text-align: left;\n"
                        + "		}\n"
                        + "		.clear {\n"
                        + "			clear: both;\n"
                        + "		}\n"
                        + "		.alert {\n"
                        + "			font-size: 16px;\n"
                        + "			color: #fff;\n"
                        + "			font-weight: 500;\n"
                        + "			padding: 20px;\n"
                        + "			text-align: center;\n"
                        + "			border-radius: 3px 3px 0 0;\n"
                        + "		}\n"
                        + "		.alert a {\n"
                        + "			color: #fff;\n"
                        + "			text-decoration: none;\n"
                        + "			font-weight: 500;\n"
                        + "			font-size: 16px;\n"
                        + "		}\n"
                        + "		.alert.alert-warning {\n"
                        + "			background: #ff9f00;\n"
                        + "		}\n"
                        + "		.alert.alert-bad {\n"
                        + "			background: #d0021b;\n"
                        + "		}\n"
                        + "		.alert.alert-good {\n"
                        + "			background: #68b90f;\n"
                        + "		}\n"
                        + "		.invoice {\n"
                        + "			margin: 40px auto;\n"
                        + "			text-align: left;\n"
                        + "			width: 80%;\n"
                        + "		}\n"
                        + "		.invoice td {\n"
                        + "			padding: 5px 0;\n"
                        + "		}\n"
                        + "		.invoice .invoice-items {\n"
                        + "			width: 100%;\n"
                        + "		}\n"
                        + "		.invoice .invoice-items td {\n"
                        + "			border-top: #eee 1px solid;\n"
                        + "		}\n"
                        + "		.invoice .invoice-items .total td {\n"
                        + "			border-top: 2px solid #333;\n"
                        + "			border-bottom: 2px solid #333;\n"
                        + "			font-weight: 700;\n"
                        + "		}\n"
                        + "		@media only screen and (max-width: 640px) {\n"
                        + "			h1, h2, h3, h4 {\n"
                        + "				font-weight: 600 !important;\n"
                        + "				margin: 20px 0 5px !important;\n"
                        + "			}\n"
                        + "			h1 {\n"
                        + "				font-size: 22px !important;\n"
                        + "			}\n"
                        + "			h2 {\n"
                        + "				font-size: 18px !important;\n"
                        + "			}\n"
                        + "			h3 {\n"
                        + "				font-size: 16px !important;\n"
                        + "			}\n"
                        + "			.container {\n"
                        + "				width: 100% !important;\n"
                        + "			}\n"
                        + "			.content,\n"
                        + "			.content-wrapper {\n"
                        + "				padding: 10px !important;\n"
                        + "			}\n"
                        + "			h3 {\n"
                        + "				font-size: 32px!important;\n"
                        + "				font-weight: bold!important;\n"
                        + "				margin-top: 0px!important;\n"
                        + "			}\n"
                        + "			.invoice {\n"
                        + "				width: 100% !important;\n"
                        + "			}\n"
                        + "		}\n"
                        + "	</style>\n"
                        + "</head>\n"
                        + "\n"
                        + "<body>\n"
                        + "	<table class=\"body-wrap\">\n"
                        + "		<tr>\n"
                        + "			<td></td>\n"
                        + "			<td class=\"container\" width=\"600\">\n"
                        + "				<div class=\"content\">\n"
                        + "					<table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                        + "						<tr>\n"
                        + "							<td class=\"content-wrap\">\n"
                        + "								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                        + "									<tr>\n"
                        + "										<td class=\"content-block\" style=\"text-align:center;\">\n"
                        + "											<img src=\"http://www.freeiconspng.com/uploads/forgot-password-icon-14.png\" width=\"90px\">\n"
                        + "											<h3>Recuperar contraseña Ser Athletic</h3> \n"
                        + "										</td>\n"
                        + "									</tr>\n"
                        + "									<tr style=\"text-align:center\">\n"
                        + "										<td class=\"content-block\"></td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "            							<td class=\"content-block\"></td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "    									<td class=\"content-block\">Nueva Contraseña: " + this.getClaveEmail() + "</td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "        								<td class=\"content-block\"></td>\n"
                        + "									</tr>\n"
                        + "                                    <tr style=\"text-align:center\">\n"
                        + "            							<td class=\"content-block\"></td>\n"
                        + "									</tr>\n"
                        + "									<tr style=\"text-align:center;\">\n"
                        + "										<td class=\"content-block\"> <a class=\"btn-primary\" href=\"http://ec2-35-160-158-243.us-west-2.compute.amazonaws.com:8080/SystemMYDEv1/\">No responda este mensage</a> \n"
                        + "										</td>\n"
                        + "									</tr>\n"
                        + "									<tr>\n"
                        + "									</tr>\n"
                        + "								</table>\n"
                        + "							</td>\n"
                        + "						</tr>\n"
                        + "					</table>\n"
                        + "					<div class=\"footer\">\n"
                        + "						<table width=\"100%\">\n"
                        + "							<tr>\n"
                        + "								<td class=\"aligncenter content-block\"></td>\n"
                        + "							</tr>\n"
                        + "						</table>\n"
                        + "					</div>\n"
                        + "				</div>\n"
                        + "			</td>\n"
                        + "			<td></td>\n"
                        + "		</tr>\n"
                        + "	</table>\n"
                        + "</body>\n"
                        + "\n"
                        + "</html>";
                message.setContent(contenido, "text/html; charset=utf-8");

            }
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        return "enviado";
    }

    public String agregarDiasF() throws ParseException {
        Calendar fecha = Calendar.getInstance();
        DateFormat dias = new SimpleDateFormat("yyyy-MM-dd");
        String fechaP = this.getFecha();
        Date fechaF;
        fechaF = dias.parse(fechaP);
        fecha.setTime(fechaF);
        fecha.add(Calendar.DATE, this.getDiasFecha());
        fecha.getTime();
        return dias.format(fecha.getTime());
    }

}
