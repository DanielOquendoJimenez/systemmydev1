package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author daniel
 */
public class mdlPersona {

    private int idpersona;
    private String primer_nombre;
    private String segundo_nombre;
    private String primer_apellido;
    private String segundoApellido;
    private String fecha_nacimiento;
    private String correo;
    private String telefono;
    private String Celular;
    private String documento_identidad;
    private String nspecialidad;
    private int estado;
    private int idTipo_persona;
    private String descripcion;
    private String foto;
    private Connection conn;

    public mdlPersona() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.conn = con.conectar();
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public String getPrimer_nombre() {
        return primer_nombre;
    }

    public void setPrimer_nombre(String primer_nombre) {
        this.primer_nombre = primer_nombre;
    }

    public String getSegundo_nombre() {
        return segundo_nombre;
    }

    public void setSegundo_nombre(String segundo_nombre) {
        this.segundo_nombre = segundo_nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String Celular) {
        this.Celular = Celular;
    }

    public String getDocumento_identidad() {
        return documento_identidad;
    }

    public void setDocumento_identidad(String documento_identidad) {
        this.documento_identidad = documento_identidad;
    }

    public String getNspecialidad() {
        return nspecialidad;
    }

    public void setNspecialidad(String nspecialidad) {
        this.nspecialidad = nspecialidad;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getIdTipo_persona() {
        return idTipo_persona;
    }

    public void setIdTipo_persona(int idTipo_persona) {
        this.idTipo_persona = idTipo_persona;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int insertarPersona() throws SQLException {
        String query = "CALL sp_insertar_persona(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getPrimer_nombre());
        cal.setString(2, this.getSegundo_nombre());
        cal.setString(3, this.getPrimer_apellido());
        cal.setString(4, this.getSegundoApellido());
        cal.setString(5, this.getFecha_nacimiento());
        cal.setString(6, this.getCorreo());
        cal.setString(7, this.getTelefono());
        cal.setString(8, this.getCelular());
        cal.setString(9, this.getDocumento_identidad());
        cal.setString(10, this.getNspecialidad());
        cal.setInt(11, this.getEstado());
        cal.setInt(12, this.getIdTipo_persona());
        cal.setString(13, this.getFoto());
        int insert = cal.executeUpdate();
        return insert;

    }

    public ResultSet listarTipoPersona() throws SQLException {
        String query = "CALL sp_listar_tipo_persona()";
        CallableStatement lip = this.conn.prepareCall(query);
        ResultSet rs = lip.executeQuery();
        return rs;
    }

    public ResultSet optenerIdpersona() throws SQLException {
        String query = "CALL sp_optener_idpersona(?)";
        CallableStatement op = this.conn.prepareCall(query);
        op.setString(1, this.getDocumento_identidad());
        ResultSet rs = op.executeQuery();
        return rs;

    }

    public ResultSet consultarP() throws SQLException {
        String query = "CALL sp_consultar_persona()";
        CallableStatement cp = this.conn.prepareCall(query);
        ResultSet rsl = cp.executeQuery();
        return rsl;
    }

    public int modificarP() throws SQLException {
        String query = "CALL sp_modificar_persona(?,?,?,?,?,?,?,?,?,?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getPrimer_nombre());
        cal.setString(2, this.getSegundo_nombre());
        cal.setString(3, this.getPrimer_apellido());
        cal.setString(4, this.getSegundoApellido());
        cal.setString(5, this.getCelular());
        cal.setString(6, this.getDocumento_identidad());
        cal.setString(7, this.getCorreo());
        cal.setString(8, this.getFecha_nacimiento());
        cal.setString(9, this.getNspecialidad());
        cal.setString(10, this.getTelefono());
        cal.setInt(11, this.getIdpersona());
        int md = cal.executeUpdate();
        return md;
    }

    public ResultSet consultarPm() throws SQLException {
        String query = "CALL sp_consultar_idpersona_modificar(?)";
        CallableStatement im = this.conn.prepareCall(query);
        im.setInt(1, this.getIdpersona());
        ResultSet rs = im.executeQuery();
        return rs;
    }

    public ResultSet ValidarDocumento() throws SQLException {
        String query = "CALL sp_validar_documento(?)";
        CallableStatement vd = this.conn.prepareCall(query);
        vd.setString(1, this.getDocumento_identidad());
        ResultSet rs = vd.executeQuery();
        return rs;
    }

    public int cambiarFoto() throws SQLException {
        String query = "CALL sp_cambiar_foto(?,?)";
        CallableStatement cal = this.conn.prepareCall(query);
        cal.setString(1, this.getFoto());
        cal.setInt(2, this.getIdpersona());
        int res = cal.executeUpdate();
        return res;
    }

    // yeisson
    public ResultSet optenerpersona() throws SQLException {
        String query = "CALL sp_optener_persona(?)";
        CallableStatement op = this.conn.prepareCall(query);
        op.setString(1, this.getDocumento_identidad());
        ResultSet rs = op.executeQuery();
        return rs;

    }

    public ResultSet consultarPersonaTecnico() throws SQLException {
        String query = "CALL sp_consultar_persona_tecnico()";
        CallableStatement cp = this.conn.prepareCall(query);
        ResultSet rsl = cp.executeQuery();
        return rsl;
    }

}
