/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import library.conexion;

/**
 *
 * @author YEISSON
 */
public class mdlMantenimiento {
    

    private String fechaMantenimiento  ;
    private String fechaMantenimientoFin  ;
    private String descripcionMante ;
    private int herramienta ;
    private int tecnico ;
    private int idMantenimiento;
    private int idDetalleMantenimiento;
    
    private Connection con;
    private int estado;
 

    public mdlMantenimiento() throws ClassNotFoundException, SQLException {
        conexion conn = new conexion();
        this.con = conn.conectar();
    }

    public String getFechaMantenimientoFin() {
        return fechaMantenimientoFin;
    }

    public void setFechaMantenimientoFin(String fechaMantenimientoFin) {
        this.fechaMantenimientoFin = fechaMantenimientoFin;
    }

  

    

    public int getHerramienta() {
        return herramienta;
    }

    public void setHerramienta(int herramienta) {
        this.herramienta = herramienta;
    }

    public int getTecnico() {
        return tecnico;
    }

    public void setTecnico(int tecnico) {
        this.tecnico = tecnico;
    }

  

  

    public String getFechaMantenimiento() {
        return fechaMantenimiento;
    }

    public void setFechaMantenimiento(String fechaMantenimiento) {
        this.fechaMantenimiento = fechaMantenimiento;
    }

    public String getDescripcionMante() {
        return descripcionMante;
    }

    public void setDescripcionMante(String descripcionMante) {
        this.descripcionMante = descripcionMante;
    }

    public int getIdMantenimiento() {
        return idMantenimiento;
    }

    public void setIdMantenimiento(int idMantenimiento) {
        this.idMantenimiento = idMantenimiento;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
        
    }

    public int getIdDetalleMantenimiento() {
        return idDetalleMantenimiento;
    }

    public void setIdDetalleMantenimiento(int idDetalleMantenimiento) {
        this.idDetalleMantenimiento = idDetalleMantenimiento;
    }
    
    
    
      public int insertar() throws SQLException {
        String query = "CALL sp_insertar_mantenimiento(?,?,?,?,?,?)";
        CallableStatement calable = this.con.prepareCall(query);
        
        calable.setString(2, this.getDescripcionMante());        
        calable.setString(3, this.getFechaMantenimiento());        
        calable.setInt(4, this.getHerramienta());        
        calable.setInt(5, this.getIdMantenimiento());
        calable.setInt(6, this.getTecnico());
        int res = calable.executeUpdate();
        return res;

    }
      
        public int insertarMantenimiento() throws SQLException {
        String query = "CALL sp_insertar_mantenimiento(?,?)";
        CallableStatement calable = this.con.prepareCall(query);        
        calable.setString(1,this.getFechaMantenimiento() );        
        calable.setString(2,this.getDescripcionMante() );       
      
        int res = calable.executeUpdate();
        return res;

    }

    public ResultSet consultarUltimoId() throws SQLException {//optener el ultimo id del mantenimiento
        String query = "CALL sp_consultar_ultimo_id_mantenimiento()";
        CallableStatement cal = this.con.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }
    
    
     public int insertarDetalleMantenimiento() throws SQLException {
        String query = "CALL sp_insertar_detalle_mantenimiento(?,?,?)";
        CallableStatement calable = this.con.prepareCall(query);
        calable.setInt(1, this.getHerramienta());
        calable.setInt(2, this.getIdMantenimiento());
        calable.setInt(3, this.getTecnico());
        int res = calable.executeUpdate();
        return res;
    }
     
     public ResultSet consultarDetalleMantenimiento() throws SQLException {
        String query = "CALL sp_consultar_detalle_mantenimiento()";
        CallableStatement calable = this.con.prepareCall(query);
        ResultSet res = calable.executeQuery();
        return res;

    }
     
      public ResultSet consultarReportesMantenimiento() throws SQLException {
        String query = "CALL sp_reportes_mantenimientos()";
        CallableStatement calable = this.con.prepareCall(query);
        ResultSet res = calable.executeQuery();
        return res;

    }
     
       public int eliminarHerramienta() throws SQLException {//eliminar item de la tabla
        String query = "CALL sp_eliminar_herramienta_detalle_encuesta(?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setInt(1, this.getIdDetalleMantenimiento());
        int res = cal.executeUpdate();
        return res;
    }
    
    
}
