package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.ResultSet;
import java.sql.CallableStatement;

/**
 *
 * @author Mayerly
 */
public class mdlCronograma {

    private int idhorario;
    private int idcronograma;
    private String objetivo;
    private int iddia;
    private int idpersona;
    private String horaini;
    private String horafin;
    private int estadoHora;
    private int idclase;
    private String nombre;
    private String descripcion;
    private int idtipoplan;
    private int estado;
    private Connection cone;

    public mdlCronograma() throws ClassNotFoundException, SQLException {
        conexion con = new conexion();
        this.cone = con.conectar();
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public int getIddia() {
        return iddia;
    }

    public void setIddia(int iddia) {
        this.iddia = iddia;
    }

    public int getIdcronograma() {
        return idcronograma;
    }

    public void setIdcronograma(int idcronograma) {
        this.idcronograma = idcronograma;
    }

    public int getIdhorario() {
        return idhorario;
    }

    public void setIdhorario(int idhorario) {
        this.idhorario = idhorario;
    }

    public String getHorafin() {
        return horafin;
    }

    public void setHorafin(String horafin) {
        this.horafin = horafin;
    }

    public String getHoraini() {
        return horaini;
    }

    public void setHoraini(String horaini) {
        this.horaini = horaini;
    }

    public int getEstadoHora() {
        return estadoHora;
    }

    public void setEstadoHora(int estadoHora) {
        this.estadoHora = estadoHora;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public int getIdclase() {
        return idclase;
    }

    public void setIdclase(int idclase) {
        this.idclase = idclase;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdtipoplan() {
        return idtipoplan;
    }

    public void setIdtipoplan(int idtipoplan) {
        this.idtipoplan = idtipoplan;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

// consultar horario
    public ResultSet listarHi() throws SQLException {
        String query = "CALL sp_consultar_horario()";
        CallableStatement cal = this.cone.prepareCall(query);
        ResultSet rst = cal.executeQuery();
        return rst;
    }
//    cierra consultar horario

//    consultar clase
    public ResultSet listarClase() throws SQLException {
        String query = "CALL sp_consultar_clase()";
        CallableStatement calte = this.cone.prepareCall(query);
        ResultSet rws = calte.executeQuery();
        return rws;
    }
//    cierra consultar clase

//    consultar dia
    public ResultSet listarDia() throws SQLException {
        String query = "CALL sp_consultar_dia()";
        CallableStatement ce = this.cone.prepareCall(query);
        ResultSet rw = ce.executeQuery();
        return rw;
    }
    //    cierra consultar dia

//    consultar objetivo
    public ResultSet listarObjetivo() throws SQLException {
        String query = "CALL sp_consultar_objetivo()";
        CallableStatement cl = this.cone.prepareCall(query);
        ResultSet rwl = cl.executeQuery();
        return rwl;
    }
    //    cierra consultar objetivo

//    cambiar estado clase
    public int cambiarEstado() throws SQLException {
        String query = "Call sp_cambiar_estado_clase(?,?)";
        CallableStatement cl = this.cone.prepareCall(query);
        cl.setInt(2, this.getIdclase());
        cl.setInt(1, this.getEstado());
        int el = cl.executeUpdate();
        return el;
    }
    //    cierra cambiar estado clase

//    cambiar estado horario
    public int cambiarHora() throws SQLException {
        String query = "Call sp_cambiar_estado_horario(?,?)";
        CallableStatement crl = this.cone.prepareCall(query);
        crl.setInt(1, this.getEstadoHora());
        crl.setInt(2, this.getIdhorario());
        int elimi = crl.executeUpdate();
        return elimi;
    }
    //    cierra cambiar estado horario

//    modificar horario
    public int modificarHorario() throws SQLException {
        String query = "Call sp_modificar_horario(?,?,?)";
        CallableStatement hyr = this.cone.prepareCall(query);
        hyr.setString(1, this.getHoraini());
        hyr.setString(2, this.getHorafin());
        hyr.setInt(3, this.getIdhorario());
        int ehy = hyr.executeUpdate();
        return ehy;
    }
    //    modificar horario

//    modificar clase
    public int modificarClase() throws SQLException {
        String query = "Call sp_modificar_clases(?,?,?,?)";
        CallableStatement hr = this.cone.prepareCall(query);
        hr.setString(1, this.getNombre());
        hr.setString(2, this.getDescripcion());
        hr.setInt(3, this.getIdtipoplan());
        hr.setInt(4, this.getIdclase());
        int eh = hr.executeUpdate();
        return eh;
    }
    //    cierra modificar clase

//    registrar horario
    public int registrarHorario() throws SQLException {
        String query = "CALL sp_insertar_horario(?,?,?)";
        CallableStatement call = this.cone.prepareCall(query);
        call.setString(1, this.getHoraini());
        call.setString(2, this.getHorafin());
        call.setInt(3, this.getEstadoHora());
        int resul = call.executeUpdate();
        return resul;
    }
    //    cierra modificar clase

//    registrar clase
    public int insertarClases() throws SQLException {
        String query = "CALL sp_insertar_clases(?,?,?,?)";
        CallableStatement c = this.cone.prepareCall(query);
        c.setString(1, this.getNombre());
        c.setInt(2, this.getEstado());
        c.setString(3, this.getDescripcion());
        c.setInt(4, this.getIdtipoplan());
        int e = c.executeUpdate();
        return e;
    }
    //    cierra registrar clase

//    registrar objetivo
    public int registrarObjetivo() throws SQLException {
        String query = "CALL sp_insertar_objetivo(?)";
        CallableStatement o = this.cone.prepareCall(query);
        o.setString(1, this.getObjetivo());
        int co = o.executeUpdate();
        return co;
    }
    //    cierra registrar objetivo

//    registrar cronograma
    public int registrarCronograma() throws SQLException {
        String query = "CALL sp_registrar_cronograma(?,?,?,?)";
        CallableStatement c = this.cone.prepareCall(query);
        c.setInt(1, this.getIdclase());
        c.setInt(2, this.getIdpersona());
        c.setInt(3, this.getIdhorario());
        c.setInt(4, this.getIddia());
        int ec = c.executeUpdate();
        return ec;
    }
    //    cierra registrar cronograma

    //listar Cronograma
    public ResultSet consultarCronograma() throws SQLException {
        String query = "CALL sp_consultar_cronograma()";
        CallableStatement cal = this.cone.prepareCall(query);
        ResultSet data = cal.executeQuery();
        return data;
    }
    //Cerro: listar Cronograma

    //Eliminar Cronograma
    public int elimiarCronograma() throws SQLException {
        String query = "CALL sp_eliminar_cronograma(?)";
        CallableStatement cal = this.cone.prepareCall(query);
        cal.setInt(1, this.getIdcronograma());
        int data = cal.executeUpdate();
        return data;
    }
    //Cerro: Eliminar Cronograma
}
