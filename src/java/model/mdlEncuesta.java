/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import library.conexion;

/**
 *
 * @author YEISSON
 */
public class mdlEncuesta {

    private String fecha_encuesta;
    private String pronostico;
    private String recomendacion;
    private String respuesta;
    private int idencuesta;
    private int idPersona;
    private int idPregunta;
    private Connection con;

    public mdlEncuesta() throws ClassNotFoundException, SQLException {
        conexion conn = new conexion();
        this.con = conn.conectar();
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getFecha_encuesta() {
        return fecha_encuesta;
    }

    public void setFecha_encuesta(String fecha_encuesta) {
        this.fecha_encuesta = fecha_encuesta;
    }

    public String getPronostico() {
        return pronostico;
    }

    public void setPronostico(String pronostico) {
        this.pronostico = pronostico;
    }

    public String getRecomendacion() {
        return recomendacion;
    }

    public void setRecomendacion(String recomendacion) {
        this.recomendacion = recomendacion;
    }

    public int getIdencuesta() {
        return idencuesta;
    }

    public void setIdencuesta(int idencuesta) {
        this.idencuesta = idencuesta;
    }

    public int insertarEncuesta() throws SQLException {
        String query = "CALL sp_insertar_encuesta(?,?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setString(1, this.getFecha_encuesta());
        cal.setString(2, this.getPronostico());
        cal.setInt(3, this.getIdPersona());

        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet consultarDetalleEncuesta() throws SQLException {
        String query = "CALL sp_consultar_detalle_encuesta()";
        CallableStatement calable = this.con.prepareCall(query);
        ResultSet res = calable.executeQuery();
        return res;

    }

    public ResultSet consultarUltimoId() throws SQLException {//optener el ultimo id de la encuesta
        String query = "CALL sp_consultar_ultimo_id_encuesta()";
        CallableStatement cal = this.con.prepareCall(query);
        ResultSet res = cal.executeQuery();
        return res;
    }

    public int insertarDetalleEncuesta() throws SQLException {
        String query = "CALL sp_insertar_detalle_encuesta(?,?,?)";
        CallableStatement calable = this.con.prepareCall(query);
        calable.setString(1, this.getRespuesta());
        calable.setInt(2, this.getIdPregunta());
        calable.setInt(3, this.getIdencuesta());
        int res = calable.executeUpdate();
        return res;
    }

    public int eliminarItem() throws SQLException {//eliminar item de la tabla
        String query = "CALL sp_eliminar_pregunta_detalle_encuesta(?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setInt(1, this.getIdencuesta());
        int res = cal.executeUpdate();
        return res;
    }

    public ResultSet consultarDetalleEncuestaId() throws SQLException {
        String query = "CALL sp_consultar_detalle_encuesta_id(?)";
        CallableStatement calable = this.con.prepareCall(query);
        calable.setInt(1, getIdencuesta());
        ResultSet res = calable.executeQuery();
        return res;

    }

    public ResultSet consultarDetalleEncuestaPersona() throws SQLException {
        String query = "CALL sp_consultar_detalle_encuesta_persona()";
        CallableStatement calable = this.con.prepareCall(query);
        ResultSet res = calable.executeQuery();
        return res;

    }

    public ResultSet consultarDetalleEncuestaPersonaId() throws SQLException {
        String query = "CALL sp_consultar_detalle_encuesta_persona_id(?)";
        CallableStatement calable = this.con.prepareCall(query);
        calable.setInt(1, this.getIdencuesta());
        ResultSet res = calable.executeQuery();
        return res;

    }

}
