/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.SQLException;
import library.conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author daniel
 */
public class mdlPregunta {

    private String pregunta;
    private int idPregunta;
    private int estado;
    private Connection con;

    public mdlPregunta() throws ClassNotFoundException, SQLException {
        conexion conn = new conexion();
        this.con = conn.conectar();
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunt(String pregunt) {
        this.pregunta = pregunt;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int insertarPregunta() throws SQLException {
        String query = "CALL sp_insertar_pregunta(?)";
        CallableStatement calable = this.con.prepareCall(query);
        calable.setString(1, this.getPregunta());
        int res = calable.executeUpdate();                
        return res;

    }
    
    // yeisson
    
    //CONSULTAR LAS PREGUNTAS 
    public ResultSet consultarPreguntas() throws SQLException {
        String query = "CALL sp_consultar_preguntas()";
        CallableStatement calable = this.con.prepareCall(query);
        ResultSet res = calable.executeQuery();
        return res;

    }
    //FIN_______CONSULTAR LAS PREGUNTAS 

    //ELIMINAR LAS PREGUNTAS
    public int eliminarPregunta() throws SQLException {
        String query = "CALL sp_elimina_pregunta(?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        //cal.setInt(1, this.getEstadoH());
        //cal.setInt(2, this.getIdherramienta());
        int res = cal.executeUpdate();
        return res;
    }
    //FIN______ELIMINAR LAS PREGUNTAS
    
    
    //MODIFICAR UNA PREGUNTA

    public int modificarPregunta() throws SQLException {
        String query = "CALL sp_modificar_Pregunta(?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setString(1, this.getPregunta());
        cal.setInt(2, this.getIdPregunta());
        int res = cal.executeUpdate();
        return res;
    }
    //FIN_____MODIFICAR UNA PREGUNTA
    
    
    
//OBTENER UNA PREGUNTA ESPECIFICA DE ACUERDO AL ID DE LA PREGUNTA
    public ResultSet listarPreguntaId() throws SQLException {
        String query = "CALL sp_listar_pregunta_id(?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setInt(1, this.getIdPregunta());
        ResultSet rs = cal.executeQuery();
        return rs;
    }
//FIN_______OBTENER UNA PREGUNTA ESPECIFICA DE ACUERDO AL ID DE LA PREGUNTA

    //CAMBIAR EL ESTADO A UNA PREGUNTA
    public int cambiarEstado() throws SQLException {
        String query = "CALL sp_cambiar_estado_pregunta(?,?)";
        CallableStatement cal = this.con.prepareCall(query);
        cal.setInt(1, this.getEstado());
        cal.setInt(2, this.getIdPregunta());
        int res = cal.executeUpdate();
        return res;
    }
      //FIN________CAMBIAR EL ESTADO A UNA PREGUNTA
    
    

}
