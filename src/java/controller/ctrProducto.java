/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import library.StringEncrypt;
import model.mdlExtras;
import model.mdlLogin;
import model.mdlPersona;
import model.mdlPlan;
import model.mdlProducto;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author YEISSON
 */
@WebServlet(name = "ctrProducto", urlPatterns = {"/ctrProducto"})
public class ctrProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            //REGISTRAR UN PRODUCTO Y AGREGAR IMAGEN AL PROYECTO
            //IMAGEN
            DiskFileItemFactory factory = new DiskFileItemFactory();
            // sets temporary location to store files
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
            ServletFileUpload upload = new ServletFileUpload(factory);
            // constructs the directory path to store upload file
            // this path is relative to application's directory
            String uploadPath = getServletContext().getRealPath("")
                    + File.separator + "upload";
            // creates the directory if it does not exist
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            //IMAGEN

            String nombreH = request.getParameter("txt_nombreProducto");
            String guardar = request.getParameter("guardarI");

            try {
                List<FileItem> formItems = upload.parseRequest(request); //ARRAY
                if (formItems != null && formItems.size() > 0) {
                    // iterates over form's fields
                    for (FileItem item : formItems) {
                        // processes only fields that are not form fields
                        if (!item.isFormField()) {
                            String fileName = new File(item.getName()).getName();
                            String filePath = uploadPath + File.separator + fileName;
                            File storeFile = new File(filePath);
                            item.write(storeFile);

                            String nombre = formItems.get(1).getString();//RECORRE LA ARRAY
                            String valor = formItems.get(2).getString();
                            String des = formItems.get(3).getString();

                            mdlProducto per = new mdlProducto();//envia a modelo
                            per.setNombre(nombre);
                            per.setValor(valor);
                            per.setDescripcion(des);

                            per.setFoto(fileName);

                            if (per.insertarProducto() == 1) {

                                out.println("<script>\n"
                                        + "alert(\"Guardado\");\n"
                                        + "document.location=('/SystemMYDEv1/view/registrarProducto.jsp');\n"
                                        + "</script>");
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                request.setAttribute("message",
                        "There was an error: " + ex.getMessage());
            }

            //FIN REGISTRAR PRODUCTO Y FOTO
              // CAMBIAR ESTADO A UN PRODUCTO
            String estado = request.getParameter("estado");
            String idProducto = request.getParameter("id");

            if (estado != null && idProducto != null) {
                mdlProducto es = new mdlProducto();
                es.setEstado(Integer.parseInt(estado));
                es.setIdProducto(Integer.parseInt(idProducto));
                int res = es.cambiarEstado();
                out.println(true);
            }
              //FIN___________CAMBIAR ESTADO A UN PRODUCTO

               // MODIFICAR UN PRODUCTO
            String nombre = request.getParameter("txt_Nombre");
            String valor = request.getParameter("txt_valor");
            String des = request.getParameter("txt_descripcion");
            String id = request.getParameter("txtId");
            String btnModificar = request.getParameter("btnModificar");
            if (btnModificar != null) {
                mdlProducto mdf = new mdlProducto();
                mdf.setNombre(nombre);
                mdf.setValor(valor);
                mdf.setDescripcion(des);

                mdf.setIdProducto(Integer.parseInt(id));
                if (mdf.modificarP() == 1) {
                    out.println("<script>\n"
                            + "alert(\"modificado\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarProducto.jsp');\n"
                            + "</script>");
                } else {
                    out.println("<script>\n"
                            + "alert(\"Error\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarProducto.jsp');\n"
                            + "</script>");
                }
            }

              // FIN___________MODIFICAR UN PRODUCTO
        }
    }

    public ResultSet consultarP() throws ClassNotFoundException, SQLException {
        mdlProducto per = new mdlProducto();
        return per.consultarP();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrProducto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrProducto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
