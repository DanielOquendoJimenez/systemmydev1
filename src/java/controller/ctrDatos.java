/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import library.StringEncrypt;
import model.mdlPersona;
import model.mdlExtras;
import model.mdlLogin;

/**
 *
 * @author Maychan
 */
@WebServlet(name = "ctrDatos", urlPatterns = {"/ctrDatos"})
public class ctrDatos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //MODIFICAR PERSONA----------------------------------------------------------------
            String primer_nombreM = request.getParameter("txtPrimerNombreM");
            String segundo_nombreM = request.getParameter("txtSegundoNombreM");
            String primer_apellidoM = request.getParameter("txtPrimerApellidoM");
            String segundoApellidoM = request.getParameter("txtSegundoApellidoM");
            String celularM = request.getParameter("txtCelularM");
            String documentoM = request.getParameter("txtDocumentoM");
            String correoM = request.getParameter("txtCorreoM");
            String fechaNacimientoM = request.getParameter("txtFechaM");
            String profecionM = request.getParameter("txtProfecionM");
            String telefonoM = request.getParameter("txtTelefonoM");
            String idPersona = request.getParameter("txtIdpersonaM");
            String btnModificar = request.getParameter("btnModificar");
            String roll = request.getParameter("txtRollM");
            if (btnModificar != null) {
                mdlExtras fecha = new mdlExtras();
                fecha.setFecha(fechaNacimientoM);
                mdlPersona mdf = new mdlPersona();
                mdf.setPrimer_nombre(primer_nombreM);
                mdf.setSegundo_nombre(segundo_nombreM);
                mdf.setPrimer_apellido(primer_apellidoM);
                mdf.setSegundoApellido(segundoApellidoM);
                mdf.setCelular(celularM);
                mdf.setDocumento_identidad(documentoM);
                mdf.setCorreo(correoM);
                mdf.setFecha_nacimiento(fechaNacimientoM = fecha.convertirFecha());
                mdf.setNspecialidad(profecionM);
                mdf.setTelefono(telefonoM);
                mdf.setIdpersona(Integer.parseInt(idPersona));
                if (mdf.modificarP() == 1) {
                    if (roll.equals("1") || roll.equals("2")) {
                        out.println("<script>\n"
                                + "alert(\"modificado\");\n"
                                + "document.location=('/SystemMYDEv1/view/inicio.jsp');\n"
                                + "</script>");
                    } else {
                        out.println("<script>\n"
                                + "alert(\"modificado\");\n"
                                + "document.location=('/SystemMYDEv1/view/DatosModificados.jsp');\n"
                                + "</script>");

                    }

                } else {
                    out.println("<script>\n"
                            + "alert(\"Error\");\n"
                            + "document.location=('/SystemMYDEv1/view/inicio.jsp');\n"
                            + "</script>");
                }
            }

            //CERRAR MODIFICAR PERSONA----------------------------------------------------------------
            //validar que la contraseña anerior sea igual---------------------------------------------
            String usuarioC = request.getParameter("usuario");
            String claveC = request.getParameter("clave");

            if (usuarioC != null && claveC != null) {
                mdlLogin con = new mdlLogin();
                con.setUsuario(usuarioC);
                ResultSet res = con.validarUsuario();
                if (res.next()) {
                    if (StringEncrypt.decrypt(res.getString("clave")).equals(claveC)) {

                    } else {
                        out.println("0");
                    }
                } else {
                    out.println("Error Al recorrer");

                }
            }
            //cerro validar que la contraseña anerior sea igual---------------------------------------------
            //cambiar contraseña----------------------------------------------------------------------------
            String nuevaClave = request.getParameter("txtNuevaClaveC");
            String nuevaClaveC = request.getParameter("txtConfirmaClaveC");
            String idPersonaC = request.getParameter("txtId");

            if (nuevaClave.equals("") && nuevaClaveC.equals("")) {
                out.println("<script>\n"
                        + "alert(\"compruebe los campos \");\n"
                        + "document.location=('/SystemMYDEv1/view/DatosModificados.jsp');\n"
                        + "</script>");
            } else if (nuevaClave.equals(nuevaClaveC)) {
                mdlLogin cam = new mdlLogin();
                cam.setIdusuario(Integer.parseInt(idPersonaC));
                cam.setClave(StringEncrypt.encrypt(nuevaClave));
                if (cam.modificarUsuario() == 1) {
                    out.println("<script>\n"
                            + "alert(\"guardado\");\n"
                            + "document.location=('/SystemMYDEv1/view/DatosModificados.jsp');\n"
                            + "</script>");
                }
            } else {
                out.println("<script>\n"
                        + "alert(\"compruebe los campos \");\n"
                        + "document.location=('/SystemMYDEv1/view/DatosModificados.jsp');\n"
                        + "</script>");
            }
            //cerro cambiar contraseña----------------------------------------------------------------------------

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ctrDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ctrDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
