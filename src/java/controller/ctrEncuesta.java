/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlEncuesta;
import model.mdlExtras;
import model.mdlPersona;

/**
 *
 * @author YEISSON
 */
@WebServlet(name = "ctrEncuesta", urlPatterns = {"/ctrEncuesta"})
public class ctrEncuesta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        try (PrintWriter out = response.getWriter()) {

            // insertar encuesta Inicial
            String fechaE = request.getParameter("fechaV");
            String pronostico = request.getParameter("pronostico");
            String idPersona = request.getParameter("idPersona");

            if (fechaE != null && idPersona != null) {
                mdlEncuesta ins = new mdlEncuesta();
                mdlExtras fecha = new mdlExtras();
                fecha.setFecha(fechaE);

                ins.setFecha_encuesta(fechaE = fecha.convertirFecha());
                ins.setPronostico(pronostico);
                ins.setIdPersona(Integer.parseInt(idPersona));

                if (ins.insertarEncuesta() == 1) {
                    ResultSet idr = ins.consultarUltimoId();
                    if (idr.next()) {

                        int idEncuesta = Integer.parseInt(idr.getString("idencuesta"));
                        out.println(idEncuesta);
                    }
                }

            }

            // cerro insetar Valoracion Inicial
            // insertar respuesta pregunta
            String resultado = request.getParameter("resultadoV");
            String idItem = request.getParameter("idPregunta");
            String idEncuesta = request.getParameter("idEncuesta");
            String idPersona1 = request.getParameter("idPersona");

            if (idItem != null && idEncuesta != null) {
                // listar tabla con items
                mdlEncuesta insD = new mdlEncuesta();
                insD.setRespuesta(resultado);
                insD.setIdPregunta(Integer.parseInt(idItem));
                insD.setIdencuesta(Integer.parseInt(idEncuesta));

                if (insD.insertarDetalleEncuesta() == 1) {
                    insD.setIdPersona(Integer.parseInt(idPersona1));
                    ResultSet tabla = insD.consultarDetalleEncuesta();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                    <thead>\n"
                            + "                                        <tr>\n"
                            + "                                            <th>Nombre</th>\n"
                            + "                                            <th>Resultado</th>\n"
                            + "                                            <th>Accion</th>\n"
                            + "                                        </tr>\n"
                            + "                                    </thead>\n"
                            + "                                    <tbody>");

                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                            <td>" + tabla.getString("preguntas") + "</td>\n");

                        if (tabla.getString("respuesta_pregunta").equals("1")) {

                            out.println("<td>si</td>\n");

                        } else {
                            out.println("<td>no</td>\n");
                        }

                        out.println("   <td><button class=\"btn btn-danger\" onclick=\"eliminarItem(" + tabla.getString("iddetalle_encuesta") + ")\"><i class=\"zmdi zmdi-delete\"></i></button></td>\n"
                                + "                                        </tr>");

                    }
                    out.println("  </tbody>\n"
                            + " </table>");
                }

            }

            //cerro insertar 
            //elimina item de la tabla 
            String idDetalle = request.getParameter("idDetalleEncuesta");

            if (idDetalle != null) {
                mdlEncuesta eliminar = new mdlEncuesta();
                eliminar.setIdencuesta(Integer.parseInt(idDetalle));
                if (eliminar.eliminarItem() == 1) {
                    // actualizar tabla despues de eliminado un registro
                    eliminar.setIdencuesta(Integer.parseInt(idDetalle));
                    eliminar.setIdPersona(Integer.parseInt(idPersona1));
                    ResultSet tabla = eliminar.consultarDetalleEncuesta();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                    <thead>\n"
                            + "                                        <tr>\n"
                            + "                                            <th>Nombre</th>\n"
                            + "                                            <th>Resultado</th>\n"
                            + "                                            <th>Accion</th>\n"
                            + "                                        </tr>\n"
                            + "                                    </thead>\n"
                            + "                                    <tbody>");

                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                            <td>" + tabla.getString("preguntas") + "</td>\n");

                        if (tabla.getString("respuesta_pregunta").equals("1")) {

                            out.println("<td>si</td>\n");

                        } else {
                            out.println("<td>no</td>\n");
                        }

                        out.println("   <td><button class=\"btn btn-danger\" onclick=\"eliminarItem(" + tabla.getString("iddetalle_encuesta") + ")\"><i class=\"zmdi zmdi-delete\"></i></button></td>\n"
                                + "                                        </tr>");

                    }
                    out.println("  </tbody>\n"
                            + " </table>");
                }
            }
            // cerro elimina item de la tabla 

            String tabla = request.getParameter("listar");
            String id = request.getParameter("id");
            if (tabla != "") {
                mdlPersona md = new mdlPersona();
                md.setDocumento_identidad(id);
                ResultSet res = md.optenerpersona();
                while (res.next()) {
                    out.println(res.getString("primer_nombre") + " " + res.getString("primer_apellido"));

                }

            } else {
                out.println("Fallo");
            }

            // FIN OBTENER PERSONA DE ACUERDO AL ID
            String tabla1 = request.getParameter("listar1");
            if (tabla1 != null) {
                mdlEncuesta md = new mdlEncuesta();
                ResultSet res = md.consultarDetalleEncuesta();
                out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                        + "    <thead>\n"
                        + "        <tr>\n"
                        + "            <th hidden>id</th>\n"
                        + "            <th>Pregunta</th>\n"
                        + "            <th>Respuesta</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (res.next()) {
                    out.println("<tr>\n"
                            + "            <td hidden>" + res.getString("iddetalle_encuesta") + "</td>\n"
                            + "            <td>" + res.getString("respuesta_pregunta") + "</td>\n"
                            + "            <td>" + res.getString("idpreguntas") + "</td>\n"
                            + "            <td><button class=\"btn btn-primary\" id=\"btn_EditarHorario\" onclick=\"editarHorario(" + res.getString("iddetalle_encuesta") + ")\" name=\"btn_EditarHorario\"><i class=\"zmdi zmdi-edit m-r-5\"></i> </button>"
                            + "            <button class=\"btn btn-danger\" id=\"btn_eliminarHorario\" onclick=\"eliminarHorario(" + res.getString("iddetalle_encuesta") + ")\" name=\"btn_eliminarHorario\"><i class=\"zmdi zmdi-delete m-r-5\"></i> </button>"
                            + "</td>\n"
                            + "        </tr>");
                }
                out.println(" </tbody>\n"
                        + "</table>");
            }

        }
    }

    public ResultSet consultarDetalleEncuestaPersona() throws ClassNotFoundException, SQLException {
        mdlEncuesta listP = new mdlEncuesta();
        return listP.consultarDetalleEncuestaPersona();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrEncuesta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
