/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlItems;
import java.sql.ResultSet;

/**
 *
 * @author daniel
 */
@WebServlet(name = "ctrItems", urlPatterns = {"/ctrItems"})
public class ctrItems extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            // listar tabla horario por dia 

            String tabla = request.getParameter("listaree");
            if (tabla != null) {
                mdlItems md = new mdlItems();
                ResultSet rs = md.listarHorario();
                out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                        + "    <thead>\n"
                        + "        <tr>\n"
                        + "            <th hidden>id</th>\n"
                        + "            <th>nombre</th>\n"
                        + "            <th>estado</th>\n"
                        + "            <th>accion</th>\n"
                        + "            <th>editar</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (rs.next()) {
                    out.println("<tr>\n"
                            + "            <td hidden>" + rs.getString("idhorarioxdia") + "</td>\n"
                            + "            <td>" + rs.getString("nombre") + "</td>\n"
                            + "            <td>\n");

                    if (rs.getString("estado").equals("1")) {
                        out.println("Activo" + "</td>");
                    } else {
                        out.println("Inactivo" + "</td>");
                    }

                    if (rs.getString("estado").equals("1")) {
                        out.println("<td><button class=\"btn btn-success\" onclick=\"estadoHorarioxdia(0," + rs.getString("idhorarioxdia") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    } else {
                        out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoHorarioxdia(1," + rs.getString("idhorarioxdia") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    }
                    out.println("<td><button class=\"btn btn-primary\" onclick=\"editarHorarioxdia(id=" + rs.getString("idhorarioxdia") + ",nom='" + rs.getString("nombre") + "')\"  data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                            + "</tr>");
                }
                out.println(" </tbody>\n"
                        + "</table>");
            }
            //cerrado listar tabla horario por dia

            // eliminar horario pordia 
            String id = request.getParameter("idhorarioxdia");
            String est = request.getParameter("estado");

            if (id != null) {
                mdlItems mdr = new mdlItems();
                mdr.setIdHorario(Integer.parseInt(id));
                mdr.setEstadoh(Integer.parseInt(est));

                if (mdr.cambiarHxd() == 1) {
                    mdlItems md = new mdlItems();
                    ResultSet res = md.listarHorario();
                    out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                            + "    <thead>\n"
                            + "        <tr>\n"
                            + "            <th hidden>id</th>\n"
                            + "            <th>nombre</th>\n"
                            + "            <th>estado</th>\n"
                            + "            <th>accion</th>\n"
                            + "            <th>editar</th>\n"
                            + "        </tr>\n"
                            + "    </thead>\n"
                            + "    <tbody>");
                    while (res.next()) {
                        out.println("<tr>\n"
                                + "            <td hidden>" + res.getString("idhorarioxdia") + "</td>\n"
                                + "            <td>" + res.getString("nombre") + "</td>\n"
                                + "            <td>\n");

                        if (res.getString("estado").equals("1")) {
                            out.println("Activo" + "</td>");
                        } else {
                            out.println("Inactivo" + "</td>");
                        }

                        if (res.getString("estado").equals("1")) {
                            out.println("<td><button class=\"btn btn-success\" onclick=\"estadoHorarioxdia(0," + res.getString("idhorarioxdia") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        } else {
                            out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoHorarioxdia(1," + res.getString("idhorarioxdia") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        }
                        out.println("<td><button class=\"btn btn-primary\" onclick=\"editarHorarioxdia(id=" + res.getString("idhorarioxdia") + ",nom='" + res.getString("nombre") + "')\"  data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                                + "</tr>");
                    }
                    out.println(" </tbody>\n"
                            + "</table>");
                }
            }
            // cerrar eliminar horario por dia 

            // registrar horario
            String nombreH = request.getParameter("txt_nombreHorario");
            String guardar = request.getParameter("guardarH");
            String estadeo = "1";

            if (guardar != null) {
                mdlItems hor = new mdlItems();
                hor.setNombreI(nombreH);
                hor.setEstadoh(Integer.parseInt(estadeo));
                if (hor.insertarHorario() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/horarioXdia.jsp');\n"
                            + "</script>");
                }
            }
            //            modificar horarioxdia
            String modif = request.getParameter("txtmodi");
            String nom = request.getParameter("txtnombho");
            String idd = request.getParameter("txtidhx");

            if (modif != null) {
                mdlItems ero = new mdlItems();
                ero.setNombreI(nom);
                ero.setIdHorario(Integer.parseInt(idd));

                if (ero.modificarHorarioxdia() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/horarioXdia.jsp');\n"
                            + "</script>");
                }
            }

//            cierra modificar horarioxdia
        }
    }

    public ResultSet listarItems() throws ClassNotFoundException, SQLException {
        mdlItems lis = new mdlItems();
        return lis.listarHorario();
    }
    
     public ResultSet listarItemsV() throws ClassNotFoundException, SQLException {
        mdlItems lis = new mdlItems();
        return lis.listaritems();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrItems.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrItems.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrItems.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrItems.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
