/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlHorarioXdia;

/**
 *
 * @author Admiinistrador
 */
@WebServlet(name = "ctrHorarioXdia", urlPatterns = {"/ctrHorarioXdia"})
public class ctrHorarioXdia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            // listar tabla horario por dia 
            String tabla = request.getParameter("listar");
            if (tabla != null) {
                mdlHorarioXdia md = new mdlHorarioXdia();
                ResultSet res = md.listarHorario();
                out.println("<div class=\"t-header th-alt bg-blue\">\n"
                        + "                <div class=\"th-title text-center\">Horarios</div>\n"
                        + "            </div><table class=\"table  table-bordered table-vmiddle\">\n"
                        + "    <thead>\n"
                        + "        <tr>\n"
                        + "            <th hidden>id</th>\n"
                        + "            <th>nombre</th>\n"
                        + "            <th>accion</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (res.next()) {
                    out.println("<tr>\n"
                            + "            <td hidden>" + res.getString("idhorarioxdia") + "</td>\n"
                            + "            <td>" + res.getString("nombre") + "</td>\n"
                            + "            <td><button class=\"btn btn-primary\" id=\"btn_EditarHorario\" onclick=\"editarHorario(" + res.getString("idhorarioxdia") + ")\" name=\"btn_EditarHorario\"><i class=\"zmdi zmdi-edit m-r-5\"></i> </button>"
                            + "            <button class=\"btn btn-danger\" id=\"btn_eliminarHorario\" onclick=\"eliminarHorario(" + res.getString("idhorarioxdia") + ")\" name=\"btn_eliminarHorario\"><i class=\"zmdi zmdi-delete m-r-5\"></i> </button>"
                            + "</td>\n"
                            + "        </tr>");
                }
                out.println(" </tbody>\n"
                        + "</table>");
            }
            //cerrado listar tabla horario por dia

            // CAMBIAR EL ESTADO A UN HORARIO POR DIA
            String estado = request.getParameter("estado");
            String idHorario = request.getParameter("id");

            if (estado != null && idHorario != null) {
                mdlHorarioXdia es = new mdlHorarioXdia();
                es.setEstado(Integer.parseInt(estado));
                es.setIdHorario(Integer.parseInt(idHorario));
                int res = es.cambiarEstado();
                out.println(true);
            }

            //FIN______CAMBIAR ESTADO A UN HORARIO POR DIA
            // registrar horario
            String nombreH = request.getParameter("txt_nombreHorario");
            String guardar = request.getParameter("guardarH");

            if (guardar != null) {
                mdlHorarioXdia hor = new mdlHorarioXdia();
                hor.setNombreI(nombreH);
                if (hor.insertarHorario() == 1) {
                    out.println("<script>\n"
                            + "alert(\"Guardado\");\n"
                            + "document.location=('/SystemMYDEv1/view/registrarHorarioXdia.jsp');\n"
                            + "</script>");
                }
            }

            //MODIFICAR UN HORARIO
            String idHorarioM = request.getParameter("txt_idHorario");
            String modificar = request.getParameter("btnModificar");
            String nombre = request.getParameter("txt_nombre");
            if (modificar != null) {
                mdlHorarioXdia mod = new mdlHorarioXdia();
                mod.setNombreI(nombre);

                mod.setIdHorario(Integer.parseInt(idHorarioM));
                if (mod.modificarHorario() == 1) {
                    out.println("<script>\n"
                            + "alert(\"modificado\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarHorarioXdia.jsp');\n"
                            + "</script>");
                } else {
                    out.println("<script>\n"
                            + "alert(\"error\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarHorarioXdia.jsp');\n"
                            + "</script>");
                }
            }

            //FIN_______MODIFICAR UN HORARIO
        }
    }

    //MUESTRA TODAS LOS HORARIOS EN LA VISTA
    public ResultSet consultarHerramientas() throws ClassNotFoundException, SQLException {
        mdlHorarioXdia listP = new mdlHorarioXdia();
        return listP.listarHorario();
    }

    //FIN LISTAR HORARIOS
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrHorarioXdia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrHorarioXdia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrHorarioXdia.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrHorarioXdia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
