/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlHerramienta;

/**
 *
 * @author daniel
 */
@WebServlet(name = "ctrHerramientas", urlPatterns = {"/ctrHerramientas"})
public class ctrHerramientas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String nombre = request.getParameter("txt_nombreHerramienta");
            String codigo = request.getParameter("txt_Serial");
            String descripcion = request.getParameter("txt_Descripcion");
            String guardar = request.getParameter("enviar-btn");
            int estado = 1;

            if (guardar != null) {
                mdlHerramienta her = new mdlHerramienta();
                her.setNombreH(nombre);
                her.setCodigoH(codigo);
                her.setDescripcionH(descripcion);
                her.setEstadoH(estado);
                if (her.insertarHerramienta() == 1) {
                    out.println("<script>\n"
                            + "alert(\"Guardado\");\n"
                            + "document.location=('/SystemMYDEv1/view/registrarHerramienta.jsp');\n"
                            + "</script>");
                }
            }

            // CAMBIAR EL ESTADO A UNA HERRAMIENTA
            String estadoH = request.getParameter("estado");
            String idHerramienta = request.getParameter("id");

            if (estadoH != null && idHerramienta != null) {
                mdlHerramienta es = new mdlHerramienta();
                es.setEstadoH(Integer.parseInt(estadoH));
                es.setIdherramienta(Integer.parseInt(idHerramienta));
                int res = es.cambiarEstado();
                out.println(true);
            }

            //FIN______CAMBIAR ESTADO A UNA HERRAMIENTA
            //MODIFICAR UNA HERRAMIENTA
            String nombreM = request.getParameter("txt_nombre");
            String codigoM = request.getParameter("txt_codigo");
            String descripcionM = request.getParameter("txt_descripcion");
            String modificar = request.getParameter("btnModificar");
            String idHerramienta1 = request.getParameter("txt_idHerramienta");
            if (modificar != null) {
                mdlHerramienta mod = new mdlHerramienta();
                mod.setNombreH(nombreM);
                mod.setCodigoH(codigoM);
                mod.setDescripcionH(descripcionM);
                mod.setIdherramienta(Integer.parseInt(idHerramienta1));
                if (mod.modificarHerramienta() == 1) {
                    out.println("<script>\n"
                            + "alert(\"modificado\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarHerramientas.jsp');\n"
                            + "</script>");
                } else {
                    out.println("<script>\n"
                            + "alert(\"error\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarHerramientas.jsp');\n"
                            + "</script>");
                }
            }

            //FIN_______MODIFICAR UNA HERRAMIENTA
        }
    }

    public ResultSet listarHerramientas() throws ClassNotFoundException, SQLException {
        mdlHerramienta listP = new mdlHerramienta();
        return listP.listarHerramienta();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrHerramientas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrHerramientas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrHerramientas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrHerramientas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
