/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlExtras;
import model.mdlValoracion;

/**
 *
 * @author daniel
 */
public class ctrValoracion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            // insetar Valoracion Inicial
            String fechaV = request.getParameter("fechaV");
            String objetivoV = request.getParameter("objetivoV");
            String idPersonaV = request.getParameter("idPersona");

            if (fechaV != null && idPersonaV != null) {
                mdlValoracion ins = new mdlValoracion();
                mdlExtras fecha = new mdlExtras();
                fecha.setFecha(fechaV);
                ins.setFecha_valoracion(fechaV = fecha.convertirFecha());
                ins.setObjetivo(objetivoV);
                ins.setIdpersona(Integer.parseInt(idPersonaV));
                if (ins.insertarValoracion() == 1) {
                    ResultSet idr = ins.consultarUltimoId();
                    if (idr.next()) {
                        String idValoracion = idr.getString("idValoracion");
                        out.println(idValoracion);
                    }
                }
            }
            // cerro insetar Valoracion Inicial

            // insertar valoracion por item
            String resultadoV = request.getParameter("resultadoV");
            String idItem = request.getParameter("idItem");
            String idValoracion = request.getParameter("idValoracion");
            String idPersona = request.getParameter("idPersona");
            if (idItem != null && idValoracion != null) {
                // listar tabla con items
                mdlValoracion insD = new mdlValoracion();
                insD.setResultado_item(resultadoV);
                insD.setIditem_valoracion(Integer.parseInt(idItem));
                insD.setIdvaloracion(Integer.parseInt(idValoracion));
                if (insD.insertarValoracionDetalle() == 1) {
                    insD.setIdpersona(Integer.parseInt(idPersona));
                    ResultSet tabla = insD.consultarValoracionPersona();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                    <thead>\n"
                            + "                                        <tr>\n"
                            + "                                            <th>Nombre</th>\n"
                            + "                                            <th>Resultado</th>\n"
                            + "                                            <th>Accion</th>\n"
                            + "                                        </tr>\n"
                            + "                                    </thead>\n"
                            + "                                    <tbody>");

                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                            <td>" + tabla.getString("nombre") + "</td>\n"
                                + "                                            <td>" + tabla.getString("resultado_item") + "</td>\n"
                                + "                                            <td><button class=\"btn btn-danger\" onclick=\"eliminarItem(" + tabla.getString("iddetalle_valoracion") + ")\"><i class=\"zmdi zmdi-delete\"></i></button></td>\n"
                                + "                                        </tr>");
                    }
                    out.println("  </tbody>\n"
                            + " </table>");
                }

            }
            //cerro insertar valoracion por item

            //elimina item de la tabla 
            String idDetalleValoracion = request.getParameter("idDetalleValoracion");

            if (idDetalleValoracion != null) {
                mdlValoracion eliminar = new mdlValoracion();
                eliminar.setIditem_detalle_valoracion(Integer.parseInt(idDetalleValoracion));
                if (eliminar.eliminarItemidValoracio() == 1) {
                    // actualizar tabla despues de eliminado un registro
                    eliminar.setIdvaloracion(Integer.parseInt(idValoracion));
                    eliminar.setIdpersona(Integer.parseInt(idPersona));
                    ResultSet tabla = eliminar.consultarValoracionPersona();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                    <thead>\n"
                            + "                                        <tr>\n"
                            + "                                            <th>Nombre</th>\n"
                            + "                                            <th>Resultado</th>\n"
                            + "                                            <th>Accion</th>\n"
                            + "                                        </tr>\n"
                            + "                                    </thead>\n"
                            + "                                    <tbody>");

                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                            <td>" + tabla.getString("nombre") + "</td>\n"
                                + "                                            <td>" + tabla.getString("resultado_item") + "</td>\n"
                                + "                                            <td><button class=\"btn btn-danger\" name=\"btnEliminar\" id=\"btnEliminar\" onclick=\"eliminarItem(" + tabla.getString("iddetalle_valoracion") + ")\"><i class=\"zmdi zmdi-delete\"></i></button></td>\n"
                                + "                                        </tr>");
                    }
                    out.println("  </tbody>\n"
                            + " </table>");
                }
            }
            // cerro elimina item de la tabla 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrValoracion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrValoracion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrValoracion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrValoracion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
