/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlExtras;
import model.mdlHerramienta;
import model.mdlMantenimiento;
import model.mdlPersona;

/**
 *
 * @author YEISSON
 */
@WebServlet(name = "ctrMantenimiento", urlPatterns = {"/ctrMantenimiento"})
public class ctrMantenimiento extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            
            
            
            // insetar mantenimiento Inicial
            String fechaM = request.getParameter("fechaV");
            String des = request.getParameter("des");
          
            if (fechaM != null) {
                mdlMantenimiento ins = new mdlMantenimiento();
                mdlExtras fecha = new mdlExtras();
                fecha.setFecha(fechaM);

                ins.setFechaMantenimiento(fechaM = fecha.convertirFecha());
                ins.setDescripcionMante(des);
               
                if (ins.insertarMantenimiento()== 1) {
                    ResultSet idr = ins.consultarUltimoId();
                    if (idr.next()) {
                        int idMantenimiento = Integer.parseInt(idr.getString("idmantenimiento"));
                        out.println(idMantenimiento);
                    }
                }
            }
            // cerro insetar mantenimiento Inicial

            // insertar respuesta pregunta
           
            String idHerramienta = request.getParameter("idHerramienta");
            String idMantenimiento = request.getParameter("idMantenimiento");
            String idPersona1 = request.getParameter("idPersona");

            if (idHerramienta != null && idMantenimiento != null) {
                // listar tabla con items
                mdlMantenimiento insD = new mdlMantenimiento();
                
                
                insD.setHerramienta(Integer.parseInt(idHerramienta));
                insD.setIdMantenimiento(Integer.parseInt(idMantenimiento));
                insD.setTecnico(Integer.parseInt(idPersona1));
                if (insD.insertarDetalleMantenimiento()== 1) {
                    
                    ResultSet tabla = insD.consultarDetalleMantenimiento();
                     out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                    <thead>\n"
                            + "                                        <tr>\n"
                            + "                                            <th>Nombre</th>\n"
                          
                            + "                                            <th>Accion</th>\n"
                            + "                                        </tr>\n"
                            + "                                    </thead>\n"
                            + "                                    <tbody>");

                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                            <td>" + tabla.getString("herramienta") + "</td>\n"
                               
                                + "                                            <td><button class=\"btn btn-danger\" onclick=\"eliminarItem(" + tabla.getString("iddetalle_mantenimiento") + ")\"><i class=\"zmdi zmdi-delete\"></i></button></td>\n"
                                + "                                        </tr>");
                    }
                    out.println("  </tbody>\n"
                            + " </table>");
                }

            }
            //cerro insertar 
            
            

            //elimina item de la tabla 
            String idDetalle = request.getParameter("idDetalle");

            if (idDetalle != null) {
                mdlMantenimiento eliminar = new mdlMantenimiento();
                eliminar.setIdDetalleMantenimiento(Integer.parseInt(idDetalle));
                if (eliminar.eliminarHerramienta()== 1) {
                    // actualizar tabla despues de eliminado un registro
                    eliminar.setIdMantenimiento(Integer.parseInt(idDetalle));
                    eliminar.setTecnico(Integer.parseInt(idPersona1));
                    ResultSet tabla = eliminar.consultarDetalleMantenimiento();
                   out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                    <thead>\n"
                            + "                                        <tr>\n"
                            + "                                            <th>Nombre</th>\n"
                          
                            + "                                            <th>Accion</th>\n"
                            + "                                        </tr>\n"
                            + "                                    </thead>\n"
                            + "                                    <tbody>");

                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                            <td>" + tabla.getString("herramienta") + "</td>\n"
                               
                                + "                                            <td><button class=\"btn btn-danger\" onclick=\"eliminarItem(" + tabla.getString("iddetalle_mantenimiento") + ")\"><i class=\"zmdi zmdi-delete\"></i></button></td>\n"
                                + "                                        </tr>");
                    }
                    out.println("  </tbody>\n"
                            + " </table>");
                }
            }
            // cerro elimina item de la tabla 
            
         

          
            

        }
    }
    
    
    
 //MUESTRA TODAS LAS HERRAMIENTAS EN LA VISTA
    public ResultSet consultarHerramientas() throws ClassNotFoundException, SQLException {
        mdlHerramienta listP = new mdlHerramienta();
        return listP.listarHerramienta();
    }
    
    //FIN LISTAR HERRAMIENTAS

   //CONSULTAR TODAS LAS PERSONAS QUE SEAN DE TIPO PERSONA TECNICO ID=5
    public ResultSet consultarP() throws ClassNotFoundException, SQLException {
        mdlPersona per = new mdlPersona();

        return per.consultarPersonaTecnico();
    }
    //FIN_ CONSULTAR TECNICOS
    
    
     public ResultSet consultarReportesMantenimiento() throws ClassNotFoundException, SQLException {
        mdlMantenimiento listP = new mdlMantenimiento();
        return listP.consultarReportesMantenimiento();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrMantenimiento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrMantenimiento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrMantenimiento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrMantenimiento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
