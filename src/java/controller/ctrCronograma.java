/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.mdlCronograma;

/**
 *
 * @author Mayerly
 */
@WebServlet(name = "ctrCronograma", urlPatterns = {"/ctrCronograma"})
public class ctrCronograma extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        try (PrintWriter out = response.getWriter()) {

            //Listar tabla del cronograma
            String req = request.getParameter("request");
            if (req != null) {
                mdlCronograma cron = new mdlCronograma();
                ResultSet data = cron.consultarCronograma();
                out.println("<div class=\"t-body tb-padding\">                     \n" +
"            <div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    05/08\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Lunes</h2>\n" +
"                    <div class=\" clearfix\">");   
                while (data.next()) {
                    if(data.getString("iddia").equals("1")){
                         out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");   
                    }
                }
                data.beforeFirst();
                 out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                 out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    10/07\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Martes</h2>\n" +
"                    <div class=\" clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("2")){
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");  
                    }
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    12/06\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Miércoles</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("3")){   
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");  
                    }
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    01/06\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">jueves</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("4")){                      
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                   
                    }                
                }
                data.beforeFirst();
                 out.println(" </div>\n" +
"                </div>\n" +
"            </div>");
                 out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Viernes</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("5")){                       
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                      
                   }                  
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Sábado</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("6")){                     
                         out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                        
                    }                  
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Domingo</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("7")){                       
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                     
                    }                 
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>\n" +
"        </div>");
            } else {

            }
            //Cerro: Listar tabla del cronograma

             //Eliminar registro del cronograma
            String idCronograma = request.getParameter("idCronograma");
            if (idCronograma != null) {
                mdlCronograma delete = new mdlCronograma();
                delete.setIdcronograma(Integer.parseInt(idCronograma));
                if (delete.elimiarCronograma() == 1) {
                    ResultSet data = delete.consultarCronograma();
                out.println("<div class=\"t-body tb-padding\">                     \n" +
"            <div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    05/08\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Lunes</h2>\n" +
"                    <div class=\" clearfix\">");   
                while (data.next()) {
                    if(data.getString("iddia").equals("1")){
                         out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");   
                    }
                }
                data.beforeFirst();
                 out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                 out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    10/07\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Martes</h2>\n" +
"                    <div class=\" clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("2")){
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");  
                    }
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    12/06\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Miercoles</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("3")){   
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");  
                    }
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    01/06\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">jueves</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("4")){                      
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                   
                    }                
                }
                data.beforeFirst();
                 out.println(" </div>\n" +
"                </div>\n" +
"            </div>");
                 out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Viernes</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("5")){                       
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                      
                   }                  
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Sabado</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("6")){                     
                         out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                        
                    }                  
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Domingo</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("7")){                       
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                     
                    }                 
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>\n" +
"        </div>");
                } else {
                    out.println("error");
                }
            } 
            //Cerro: Eliminar registro del cronograma
            
            //insertar cronograma
            String idDia = request.getParameter("idDia");
            String idHorario = request.getParameter("idHorario");
            String idClase = request.getParameter("idClase");
            String idInstructor = request.getParameter("idInstructor");
            if (idDia != "" && idHorario != "" && idClase != "" && idInstructor != "") {
                mdlCronograma insert = new mdlCronograma();
                insert.setIddia(Integer.parseInt(idDia));
                insert.setIdhorario(Integer.parseInt(idHorario));
                insert.setIdclase(Integer.parseInt(idClase));
                insert.setIdpersona(Integer.parseInt(idInstructor));
                if (insert.registrarCronograma() == 1) {
                    ResultSet data = insert.consultarCronograma();
                out.println("<div class=\"t-body tb-padding\">                     \n" +
"            <div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    05/08\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Lunes</h2>\n" +
"                    <div class=\" clearfix\">");   
                while (data.next()) {
                    if(data.getString("iddia").equals("1")){
                         out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");   
                    }
                }
                data.beforeFirst();
                 out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                 out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    10/07\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Martes</h2>\n" +
"                    <div class=\" clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("2")){
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");  
                    }
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    12/06\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Miercoles</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("3")){   
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");  
                    }
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    01/06\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">jueves</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("4")){                      
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                   
                    }                
                }
                data.beforeFirst();
                 out.println(" </div>\n" +
"                </div>\n" +
"            </div>");
                 out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Viernes</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("5")){                       
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                      
                   }                  
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Sabado</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("6")){                     
                         out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                        
                    }                  
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>");
                out.println("<div class=\"p-timeline\">\n" +
"                <div class=\"pt-line c-gray text-right\">\n" +
"                    <span class=\"d-block\">2015</span>\n" +
"                    22/04\n" +
"                </div>  \n" +
"                <div class=\"pt-body\">\n" +
"                    <h2 class=\"ptb-title\">Domingo</h2>\n" +
"                    <div class=\"clearfix\">");
                while (data.next()) {
                    if(data.getString("iddia").equals("7")){                       
                        out.println("<div class=\"col-sm-3\">\n" +
"                            <div class=\"tile bg-blue\">\n" +
"                                <div class=\"t-header th-alt\">\n" +
"                                    <div class=\"th-title\">"+data.getString("descripcion").toUpperCase()+"</div>\n" +
"                                    <div class=\"actions dropdown\">\n" +
"                                        <a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\"><i class=\"zmdi zmdi-more\"></i></a>\n" +
"                                        <ul class=\"dropdown-menu pull-right\">\n" +
"                                            <li><a onclick=\"deleteSchedule("+data.getString("idcronograma")+")\">Eliminar</a></li>\n" +
"                                        </ul>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"t-body tb-padding\">\n" +
"                                    <p>Instructor: "+data.getString("primer_nombre")+"   "+data.getString("primer_apellido")+"</p>\n" +
"                                    <p>Hora: "+data.getString("hora_inicio")+" a "+data.getString("hora_fin")+"</p>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>");                     
                    }                 
                }
                data.beforeFirst();
                out.println("</div>\n" +
"                </div>\n" +
"            </div>\n" +
"        </div>");
                }
            }
            //Cerro: insertar cronograma


            //    tabla clase
            String tabla = request.getParameter("listar");

            if (tabla != null) {
                mdlCronograma md = new mdlCronograma();
                ResultSet rs = md.listarClase();
                out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                        + "    <thead>\n"
                        + "        <tr>\n"
                        + "            <th hidden>id</th>\n"
                        + "            <th>Nombre</th>\n"
                        + "            <th>Descripcion</th>\n"
                        + "            <th>Tipo Clase</th>\n"
                        + "            <th>Estado</th>\n"
                        + "            <th>Action</th>\n"
                        + "            <th>Editar</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (rs.next()) {
                    out.println("<tr>\n"
                            + "            <td hidden>" + rs.getString("idclase") + "</td>\n"
                            + "            <td>" + rs.getString("nombre") + "</td>\n"
                            + "            <td>" + rs.getString("descripcion") + "</td>\n"
                            + "            <td>");

                    if (rs.getString("idtipo_plan").equals("1")) {
                        out.println("Pilates" + "</td>");
                    } else {
                        out.println("Normal" + "</td>");
                    }

                    if (rs.getString("estado").equals("1")) {
                        out.println("<td>" + "Activo" + "</td>");
                    } else {
                        out.println("<td>" + "Inactivo" + "</td>");
                    }

                    if (rs.getString("estado").equals("1")) {
                        out.println("<td><button class=\"btn btn-success\" onclick=\"estadoClase(0," + rs.getString("idclase") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    } else {
                        out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoClase(1," + rs.getString("idclase") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    }
                    out.println("<td><button onclick=\"editarClase(id=" + rs.getString("idclase") + ",nom='" + rs.getString("nombre") + "',des='" + rs.getString("descripcion") + "')\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\".bs-example-modal-xs\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                            + "</tr>");
                }
                out.println(" </tbody>\n"
                        + "</table>");
            }
            //    cierra tabla clase

            //    tabla clase cambiando estado y editando
            String idcla = request.getParameter("idclase");
            String est = request.getParameter("estado");

            if (idcla != null) {
                mdlCronograma eli = new mdlCronograma();
                eli.setIdclase(Integer.parseInt(idcla));
                eli.setEstado(Integer.parseInt(est));

                if (eli.cambiarEstado() == 1) {
                    mdlCronograma mdl = new mdlCronograma();
                    ResultSet res = mdl.listarClase();
                    out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                            + "    <thead>\n"
                            + "        <tr>\n"
                            + "            <th hidden>id</th>\n"
                            + "            <th>Nombre</th>\n"
                            + "            <th>Descripcion</th>\n"
                            + "            <th>Tipo Clase</th>\n"
                            + "            <th>Estado</th>\n"
                            + "            <th>Action</th>\n"
                            + "            <th>Editar</th>\n"
                            + "        </tr>\n"
                            + "    </thead>\n"
                            + "    <tbody>");
                    while (res.next()) {
                        out.println("<tr>\n"
                                + "            <td hidden>" + res.getString("idclase") + "</td>\n"
                                + "            <td>" + res.getString("nombre") + "</td>\n"
                                + "            <td>" + res.getString("descripcion") + "</td>\n"
                                + "            <td>");
                        if (res.getString("idtipo_plan").equals("1")) {
                            out.println("Pilates" + "</td>");
                        } else {
                            out.println("Normal" + "</td>");
                        }

                        if (res.getString("estado").equals("1")) {
                            out.println("<td>" + "Activo" + "</td>");
                        } else {
                            out.println("<td>" + "Inactivo" + "</td>");
                        }

                        if (res.getString("estado").equals("1")) {
                            out.println("<td><button class=\"btn btn-success\" onclick=\"estadoClase(0," + res.getString("idclase") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        } else {
                            out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoClase(1," + res.getString("idclase") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        }
                        out.println("<td><button onclick=\"editarClase(id=" + res.getString("idclase") + ",nom='" + res.getString("nombre") + "',des='" + res.getString("descripcion") + "')\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\".bs-example-modal-xs\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                                + "</tr>");
                    }
                    out.println(" </tbody>\n"
                            + "</table>");
                }
            }

            //    tabla clase cambiando estado y editando
            //    tabla horario
            String tabl = request.getParameter("listare");

            if (tabl != null) {
                mdlCronograma mde = new mdlCronograma();
                ResultSet rse = mde.listarHi();
                out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                        + "    <thead>\n"
                        + "        <tr>\n"
                        + "            <th hidden>id</th>\n"
                        + "            <th>Hora inicio</th>\n"
                        + "            <th>Hora final</th>\n"
                        + "            <th>Estado</th>\n"
                        + "            <th>Action</th>\n"
                        + "            <th>Editar</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (rse.next()) {
                    out.println("<tr>\n"
                            + "            <td hidden >" + rse.getString("idhorario") + "</td>\n"
                            + "            <td>" + rse.getString("hora_inicio") + "</td>\n"
                            + "            <td>" + rse.getString("hora_fin") + "</td>\n"
                            + "            <td>\n");

                    if (rse.getString("estado").equals("1")) {
                        out.println("Activo" + "</td>");
                    } else {
                        out.println("Inactivo" + "</td>");
                    }

                    if (rse.getString("estado").equals("1")) {
                        out.println("<td><button class=\"btn btn-success\" onclick=\"estadoHorario(0," + rse.getString("idhorario") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    } else {
                        out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoHorario(1," + rse.getString("idhorario") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    }
                    out.println("<td><button class=\"btn btn-primary\" onclick =\"editarHorario(id=" + rse.getString("idhorario") + ",hin='" + rse.getString("hora_inicio") + "',hfn='" + rse.getString("hora_fin") + "')\"  data-toggle=\"modal\" data-target=\".bs-example-modal-lg\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                            + "</tr>");
                }
                out.println(" </tbody>\n"
                        + "</table>");
            }

            //    cierra tabla horario
//    tabla horario cambiando estado y editando
            String es = request.getParameter("estado");
            String idhora = request.getParameter("idhorario");

            if (idhora != null) {
                mdlCronograma crr = new mdlCronograma();
                crr.setEstadoHora(Integer.parseInt(es));
                crr.setIdhorario(Integer.parseInt(idhora));

                if (crr.cambiarHora() == 1) {
                    mdlCronograma mdl = new mdlCronograma();
                    ResultSet ra = mdl.listarHi();
                    out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                            + "    <thead>\n"
                            + "        <tr>\n"
                            + "            <th hidden>id</th>\n"
                            + "            <th>Hora inicio</th>\n"
                            + "            <th>Hora final</th>\n"
                            + "            <th>Estado</th>\n"
                            + "            <th>Action</th>\n"
                            + "            <th>Editar</th>\n"
                            + "        </tr>\n"
                            + "    </thead>\n"
                            + "    <tbody>");
                    while (ra.next()) {
                        out.println("<tr>\n"
                                + "            <td hidden>" + ra.getString("idhorario") + "</td>\n"
                                + "            <td>" + ra.getString("hora_inicio") + "</td>\n"
                                + "            <td>" + ra.getString("hora_fin") + "</td>\n"
                                + "<td>\n");

                        if (ra.getString("estado").equals("1")) {
                            out.println("Activo" + "</td>");
                        } else {
                            out.println("Inactivo" + "</td>");
                        }

                        if (ra.getString("estado").equals("1")) {
                            out.println("<td><button class=\"btn btn-success\" onclick=\"estadoHorario(0," + ra.getString("idhorario") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        } else {
                            out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoHorario(1," + ra.getString("idhorario") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        }
                        out.println("<td><button onclick=\"editarHorario(id=" + ra.getString("idhorario") + ",hin ='" + ra.getString("hora_inicio") + "',hfn ='" + ra.getString("hora_fin") + "')\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\".bs-example-modal-lg\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                                + "</tr>");
                    }
                    out.println(" </tbody>\n"
                            + "</table>");
                }
            }

            //    cierra tabla horario cambiando estado y editando
            //    registrar clase 
            String nombre = request.getParameter("txt_clase");
            String descripcion = request.getParameter("txt_descripcionclase");
            String idtipoplan = request.getParameter("txt_idtipoplan");
            String guardare = request.getParameter("txt_guardar-btn1");
            String estado = "1";

            if (guardare != null) {
                mdlCronograma ee = new mdlCronograma();
                ee.setNombre(nombre);
                ee.setEstado(Integer.parseInt(estado));
                ee.setDescripcion(descripcion);
                ee.setIdtipoplan(Integer.parseInt(idtipoplan));

                if (ee.insertarClases() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/clase.jsp');\n"
                            + "</script>");
                }
            }

            //    cierra registrar clase 
            //    registrar horario 
            String horainici = request.getParameter("txt_horai");
            String horafi = request.getParameter("txt_horaf");
            String esto = "1";
            String guardarho = request.getParameter("txt_guardarhora");

            if (guardarho != null) {
                mdlCronograma cr = new mdlCronograma();
                cr.setHoraini(horainici);
                cr.setHorafin(horafi);
                cr.setEstadoHora(Integer.parseInt(esto));

                if (cr.registrarHorario() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/horario.jsp');\n"
                            + "</script>");
                }
            }
            //    cierra registrar horario 

//    registrar objetivo 
            String descripciono = request.getParameter("txt_descripciono");
            String guardaer = request.getParameter("txt_guardaro");

            if (guardaer != null) {
                mdlCronograma eee = new mdlCronograma();
                eee.setObjetivo(descripciono);
                if (eee.registrarObjetivo() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/cronograma.jsp');\n"
                            + "</script>");
                }
            }

            //    cierra registrar objetivo 
//       modificar clase
            String mod = request.getParameter("txtmodific");
            String nomb = request.getParameter("txtnombre");
            String descripc = request.getParameter("txtdescripcionclase");
            String idtipop = request.getParameter("txtidtipoplan");
            String idcl = request.getParameter("txtclase");

            if (mod != null) {
                mdlCronograma ero = new mdlCronograma();
                ero.setNombre(nomb);
                ero.setDescripcion(descripc);
                ero.setIdtipoplan(Integer.parseInt(idtipop));
                ero.setIdclase(Integer.parseInt(idcl));

                if (ero.modificarClase() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/clase.jsp');\n"
                            + "</script>");
                }
            }
//           cierra  modificar clase 
            //             modificar horario 
            String modi = request.getParameter("txt_modif");
            String hi = request.getParameter("txthi");
            String hf = request.getParameter("txthf");
            String idho = request.getParameter("txtidh");

            if (modi != null) {
                mdlCronograma cr = new mdlCronograma();
                cr.setHoraini(hi);
                cr.setHorafin(hf);
                cr.setIdhorario(Integer.parseInt(idho));

                if (cr.modificarHorario() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/horario.jsp');\n"
                            + "</script>");
                }
            }

//           cierra  modificar horario 
        }
    }

//  consultar objetivo cronograma
    public ResultSet listarObjetivo() throws ClassNotFoundException, SQLException {
        mdlCronograma lo = new mdlCronograma();
        return lo.listarObjetivo();
    }
    //  cierra consultar objetivo cronograma

    //  consultar horario cronograma
    public ResultSet listarHi() throws ClassNotFoundException, SQLException {
        mdlCronograma leo = new mdlCronograma();
        return leo.listarHi();
    }

    //  cierra consultar horario cronograma
    //listar dias
    public ResultSet listDay() throws ClassNotFoundException, SQLException {
        mdlCronograma days = new mdlCronograma();
        return days.listarDia();
    }

    //Cerro: listar dias
    //listar clases
    public ResultSet listClass() throws ClassNotFoundException, SQLException {
        mdlCronograma clas = new mdlCronograma();
        return clas.listarClase();
    }

    //Cerro: listar clases
    
    public ResultSet consultarCronograma() throws ClassNotFoundException, SQLException{
        mdlCronograma cronograma = new mdlCronograma();
        return cronograma.consultarCronograma();
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ctrCronograma.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ctrCronograma.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
