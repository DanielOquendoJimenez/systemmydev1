/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlPlanNutricional;
import java.sql.ResultSet;
import model.mdlExtras;

/**
 *
 * @author daniel
 */
public class ctrPlanNutricional extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /*<button type=\"button\" class=\"btn btn-info\">Ver</button>*/

            //seleccionar valoracion
            String idPersona = request.getParameter("idPersona");
            if (idPersona != null) {
                mdlPlanNutricional plan = new mdlPlanNutricional();
                plan.setIdpersona(Integer.parseInt(idPersona));
                ResultSet tabla = plan.consultarValoraciones();
                out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                        + "    <thead id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                        + "        <tr>\n"
                        + "            <th>Fecha de la Valoracion</th>\n"
                        + "            <th>Objetivo</th>\n"
                        + "            <th>Accion</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (tabla.next()) {
                    out.println("<tr>\n"
                            + "            <td>" + tabla.getString("fecha_valoracion") + "</td>\n"
                            + "            <td>" + tabla.getString("objetivo") + "</td>\n"
                            + "            <td><button type=\"button\" class=\"btn btn-primary\" onclick=\"seleccionar(" + tabla.getString("idvaloracion") + ")\">Seleccionar</button></td>\n"
                            + "        </tr>");
                }
                out.println("</tbody>\n"
                        + "</table>");
            }
            //cerro seleccionar valoracion
            String idValoracion = request.getParameter("idvaliracion");
            if (idValoracion != null) {
                mdlPlanNutricional planS = new mdlPlanNutricional();
                planS.setIdvaloracion(Integer.parseInt(idValoracion));
                ResultSet datos = planS.consultarValoracion();
                out.println("<div class=\"pmb-block\">\n"
                        + "                                <div class=\"pmbb-body p-l-30\">\n"
                        + "                                    <div class=\"pmbb-view\">");
                if (datos.next()) {
                    out.println("<dl class=\"dl-horizontal\">\n"
                            + "<input type='text' id=\"txtIdvaloracion\" name=\"txtIdvaloracion\" hidden=\"\" value=" + datos.getString("idvaloracion") + ">"
                            + "                                            <dt>Nombre</dt>\n"
                            + "                                            <dd>" + datos.getString("primer_nombre") + "</dd>\n"
                            + "                                        </dl>\n"
                            + "                                        <dl class=\"dl-horizontal\">\n"
                            + "                                            <dt>Apellido</dt>\n"
                            + "                                            <dd>" + datos.getString("primer_apellido") + "</dd>\n"
                            + "                                        </dl>\n"
                            + "                                        <dl class=\"dl-horizontal\">\n"
                            + "                                            <dt>Objetivo</dt>\n"
                            + "                                            <dd>" + datos.getString("objetivo") + "</dd>\n"
                            + "                                        </dl>");
                }
                datos.beforeFirst();
                out.println("<div class=\"table-responsive m-t-20\">\n"
                        + "                                            <table class=\"table\">\n"
                        + "                                                <thead>\n"
                        + "                                                    <tr>\n"
                        + "                                                        <th>Fecha</th>\n"
                        + "                                                        <th>Item</th>\n"
                        + "                                                        <th>Resultado</th>\n"
                        + "                                                        <th>Accion</th>\n"
                        + "                                                    </tr>\n"
                        + "                                                </thead>\n"
                        + "                                                <tbody>");
                while (datos.next()) {
                    out.println("<tr>\n"
                            + "                                                        <td>" + datos.getString("fecha_valoracion") + "</td>\n"
                            + "                                                        <td>" + datos.getString("nombre") + "</td>\n"
                            + "                                                        <td>" + datos.getString("resultado_item") + "</td>\n"
                            + "                                                        <td><button type=\"button\" class=\"btn btn-danger next-step\"><i class=\"zmdi zmdi-delete m-r-5\"></i></button></td>\n"
                            + "                                                    </tr>");
                }
                out.println("  </tbody>\n"
                        + "                                            </table>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </div>\n"
                        + "                            </div>");
            }

            String idValoracionI = request.getParameter("idValoracion");
            String fechaPlanN = request.getParameter("fechaPlanNutricional");
            if (idValoracionI != null && fechaPlanN != null) {
                mdlPlanNutricional registrar = new mdlPlanNutricional();
                mdlExtras fecha = new mdlExtras();
                fecha.setFecha(fechaPlanN);
                registrar.setIdvaloracion(Integer.parseInt(idValoracionI));
                registrar.setFecha_plan_nutricional(fechaPlanN = fecha.convertirFecha());
                if (registrar.insertarPlanNutricional() == 1) {
                    ResultSet idPlanN = registrar.consultarUltimoPlanN();
                    if (idPlanN.next()) {
                        String idPlanNutricional = idPlanN.getString("idPlanNutricional");
                        out.println(idPlanNutricional);
                    }
                }
            }
            String idPlanNutricional = request.getParameter("idPlanNutricional");
            String idHorario = request.getParameter("idHorario");
            String idAlimento = request.getParameter("idAlimento");
            String porcion = request.getParameter("porcion");

            if (idHorario != null && idAlimento != null && porcion != null) {
                mdlPlanNutricional insertar = new mdlPlanNutricional();
                insertar.setPorcion(porcion);
                insertar.setIdAlimento(Integer.parseInt(idAlimento));
                insertar.setIdHorarioDia(Integer.parseInt(idHorario));
                insertar.setIdplan_nutricional(Integer.parseInt(idPlanNutricional));
                if (insertar.insertarDetallePlanNutricional() == 1) {
                    ResultSet tablaPn = insertar.cosultarDetallePlanNutricional();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                            <thead>\n"
                            + "                                                <tr>\n"
                            + "                                                    <th>Horario</th>\n"
                            + "                                                    <th>Alimento</th>\n"
                            + "                                                    <th>Porcion</th>\n"
                            + "                                                    <th>Acction</th>\n"
                            + "                                                </tr>\n"
                            + "                                            </thead>\n"
                            + "                                            <tbody>");
                    while (tablaPn.next()) {
                        out.println("<tr>\n"
                                + "                                                    <th>" + tablaPn.getString("horario") + "</th>\n"
                                + "                                                    <th>" + tablaPn.getString("alimento") + "</th>\n"
                                + "                                                    <th>" + tablaPn.getString("porcion") + "</th>\n"
                                + "                                                    <th><button type=\"button\" class=\"btn btn-danger\" onclick=\"eliminarPlanN(" + tablaPn.getString("iddetalle_plan_nutricional") + ")\"><i class=\"zmdi zmdi-delete m-r-5\"></i></button></th>\n"
                                + "                                                </tr>");
                    }
                    out.println("  </tbody>\n"
                            + "                                        </table>   ");
                }
            }
            String idDetallePLanNutricional = request.getParameter("idDetallePLanNutricional");
            if (idDetallePLanNutricional != null) {
                mdlPlanNutricional eliminar = new mdlPlanNutricional();
                eliminar.setIdDetallePlanNutricional(Integer.parseInt(idDetallePLanNutricional));
                if (eliminar.eliminarItemPlanNutricional() == 1) {
                    eliminar.setIdplan_nutricional(Integer.parseInt(idPlanNutricional));
                    ResultSet tabla = eliminar.cosultarDetallePlanNutricional();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "                                            <thead>\n"
                            + "                                                <tr>\n"
                            + "                                                    <th>Horario</th>\n"
                            + "                                                    <th>Alimento</th>\n"
                            + "                                                    <th>Porcion</th>\n"
                            + "                                                    <th>Acction</th>\n"
                            + "                                                </tr>\n"
                            + "                                            </thead>\n"
                            + "                                            <tbody>");
                    while (tabla.next()) {
                        out.println("<tr>\n"
                                + "                                                    <th>" + tabla.getString("horario") + "</th>\n"
                                + "                                                    <th>" + tabla.getString("alimento") + "</th>\n"
                                + "                                                    <th>" + tabla.getString("porcion") + "</th>\n"
                                + "                                                    <th><button type=\"button\" class=\"btn btn-danger\" onclick=\"eliminarPlanN(" + tabla.getString("iddetalle_plan_nutricional") + ")\"><i class=\"zmdi zmdi-delete m-r-5\"></i></button></th>\n"
                                + "                                                </tr>");
                    }
                    out.println("  </tbody>\n"
                            + "                                        </table>   ");
                }
            }

        }
    }

    public ResultSet listarAlimento() throws ClassNotFoundException, SQLException {
        mdlPlanNutricional alimento = new mdlPlanNutricional();
        return alimento.consultarAlimento();
    }

    public ResultSet listarHorarioDia() throws ClassNotFoundException, SQLException {
        mdlPlanNutricional horario = new mdlPlanNutricional();
        return horario.ConsultarHorarioDia();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPlanNutricional.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPlanNutricional.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPlanNutricional.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPlanNutricional.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
