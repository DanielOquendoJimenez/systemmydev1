/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.mdlAlimento;

/**
 *
 * @author Mayerly
 */
@WebServlet(name = "ctrAlimento", urlPatterns = {"/ctrAlimento"})
public class ctrAlimento extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {

//            Listar tabla alimento
            String tabla = request.getParameter("listarr");

            if (tabla != null) {
                mdlAlimento md = new mdlAlimento();
                ResultSet rs = md.listarAlimento();
                out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                        + "    <thead>\n"
                        + "        <tr>\n"
                        + "            <th>id</th>\n"
                        + "            <th>Nombre</th>\n"
                        + "            <th>Descripcion</th>\n"
                        + "            <th>Action</th>\n"
                        + "            <th>Estado</th>\n"
                        + "            <th>Editar</th>\n"
                        + "        </tr>\n"
                        + "    </thead>\n"
                        + "    <tbody>");
                while (rs.next()) {
                    out.println("<tr>\n"
                            + "            <td>" + rs.getString("idalimento") + "</td>\n"
                            + "            <td>" + rs.getString("nombre") + "</td>\n"
                            + "            <td>" + rs.getString("descripcion") + "</td>\n"
                            + "            <td>\n");

                    if (rs.getString("estado").equals("1")) {
                        out.println("Activo" + "</td>");
                    } else {
                        out.println("Inactivo" + "</td>");
                    }

                    if (rs.getString("estado").equals("1")) {
                        out.println("<td><button class=\"btn btn-success\" onclick=\"estadoAlimento(0," + rs.getString("idalimento") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    } else {
                        out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoAlimento(1," + rs.getString("idalimento") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                    }
                    out.println("<td><button onclick=\"editarAlimento(id=" + rs.getString("idalimento") + ",nom='" + rs.getString("nombre") + "',des='" + rs.getString("descripcion") + "')\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\".bs-example-modal-xs\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                            + "</tr>");
                }
                out.println(" </tbody>\n"
                        + "</table>");
            }
//            Cierra listar tabla alimento
            
//            Listar tabla alimento con cambio de estado
            String idalimento = request.getParameter("idalimento");
            String estado = request.getParameter("estado");

            if (idalimento != null) {
                mdlAlimento mdd = new mdlAlimento();
                mdd.setIdalimento(Integer.parseInt(idalimento));
                mdd.setEstado(Integer.parseInt(estado));

                if (mdd.cambiarAlimento() == 1) {
                    mdlAlimento mde = new mdlAlimento();
                    ResultSet res = mde.listarAlimento();
                    out.println("<table class=\"table table-bordered table-vmiddle\">\n"
                            + "    <thead>\n"
                            + "        <tr>\n"
                            + "            <th>id</th>\n"
                            + "            <th>Nombre</th>\n"
                            + "            <th>Descripcion</th>\n"
                            + "            <th>Action</th>\n"
                            + "            <th>Estado</th>\n"
                            + "            <th>Editar</th>\n"
                            + "        </tr>\n"
                            + "    </thead>\n"
                            + "    <tbody>");

                    while (res.next()) {
                        out.println("<tr>\n"
                                + "            <td>" + res.getString("idalimento") + "</td>\n"
                                + "            <td>" + res.getString("nombre") + "</td>\n"
                                + "            <td>" + res.getString("descripcion") + "</td>\n"
                                + "            <td>\n");

                        if (res.getString("estado").equals("1")) {
                            out.println("Activo" + "</td>");
                        } else {
                            out.println("Inactivo" + "</td>");
                        }

                        if (res.getString("estado").equals("1")) {
                            out.println("<td><button class=\"btn btn-success\" onclick=\"estadoAlimento(0," + res.getString("idalimento") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        } else {
                            out.println("<td><button class=\"btn btn-danger\" onclick=\"estadoAlimento(1," + res.getString("idalimento") + ")\"><i class=\"zmdi zmdi-refresh-sync\"></i></button></td>\n");
                        }
                        out.println("<td><button onclick=\"editarAlimento(id=" + res.getString("idalimento") + ",nom='" + res.getString("nombre") + "',des='" + res.getString("descripcion") + "')\" class=\"btn btn-primary\" data-toggle=\"modal\" data=\".bs-example-modal-xs\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button></td>\n"
                                + "</tr>");
                    }
                    out.println(" </tbody>\n"
                            + "</table>");
                }
            }
            //            Cierra listar tabla alimento con cambio de estado

//           Registrar alimento
            String nombre = request.getParameter("txt_nombrea");
            String descripcion = request.getParameter("txt_descripciona");
            String estadoo = "1";
            String guardare = request.getParameter("txt_guardar2");

            if (guardare != null) {
                mdlAlimento eo = new mdlAlimento();
                eo.setNombreAlimento(nombre);
                eo.setDescripAlimen(descripcion);
                eo.setEstado(Integer.parseInt(estadoo));

                if (eo.insertarAlimento() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/alimento.jsp');\n"
                            + "</script>");
                }
            }
//           Cierra registrar alimento

//            modificar alimento
            String mod = request.getParameter("txt_modifi");
            String nom = request.getParameter("txt_nomb");
            String des = request.getParameter("txt_des");
            String idd = request.getParameter("txtId");

            if (mod != null) {
                mdlAlimento ero = new mdlAlimento();
                ero.setNombreAlimento(nom);
                ero.setDescripAlimen(des);
                ero.setIdalimento(Integer.parseInt(idd));

                if (ero.modificarAlimento() == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/alimento.jsp');\n"
                            + "</script>");
                }
            }
//            cierra modificar alimento

        }
    }

    public int insertarAlimento() throws ClassNotFoundException, SQLException {
        mdlAlimento l = new mdlAlimento();
        return l.insertarAlimento();
    }

    public ResultSet listarAlimento() throws ClassNotFoundException, SQLException {
        mdlAlimento le = new mdlAlimento();
        return le.listarAlimento();
    }

    public ResultSet modificarAlimento() throws ClassNotFoundException, SQLException {
        mdlAlimento li = new mdlAlimento();
        return li.listarAlimento();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ctrAlimento.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ctrAlimento.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
