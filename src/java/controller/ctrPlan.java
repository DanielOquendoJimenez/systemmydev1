/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.mdlExtras;
import model.mdlPlan;
import model.mdlPromocion;

/**
 *
 * @author daniel
 */
@WebServlet(name = "ctrPlan", urlPatterns = {"/ctrPlan"})
public class ctrPlan extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String nombre = request.getParameter("nombre");
            String valor = request.getParameter("valor");
            String descuento = request.getParameter("descuento");
            String dias = request.getParameter("dias");
            String tipoPlan = request.getParameter("tipoPlan");

            if (nombre != null) {
                mdlPlan re = new mdlPlan();
                re.setNombre(nombre);
                re.setValor(Integer.parseInt(valor));
                re.setDescuento(Integer.parseInt(descuento));
                re.setDias_cliente(Integer.parseInt(dias));
                re.setIdtipo_plan(Integer.parseInt(tipoPlan));
                if (re.registrarPlan() == 1) {
                    //out.println(true);
                    mdlPlan ultimoId = new mdlPlan();
                    ResultSet id = ultimoId.optenerUltimoId();
                    if (id.next()) {
                        String idPlan = id.getString("idplan");
                        out.println(idPlan);
                    }
                }
            }
            String idPlanP = request.getParameter("idPlan");
            String idPromocion = request.getParameter("idPromocion");

            if (idPlanP != null && idPromocion != null) {
                mdlPlan pro = new mdlPlan();
                pro.setIdpromociones(Integer.parseInt(idPromocion));
                pro.setIdplan(Integer.parseInt(idPlanP));
                if (pro.insertarPlanPromocion() == 1) {
                    pro.setIdplan(Integer.parseInt(idPlanP));
                    ResultSet res = pro.listarDetallePromocion();
                    out.println("<ul>");
                    while (res.next()) {
                        out.println("<li>" + res.getString("nombre") + "</li>");
                    }
                    out.println("</ul>");
                } else {
                    out.println(false);
                }
            }

            String estado = request.getParameter("estado");
            String idPlan = request.getParameter("idPlan");

            if (estado != null && idPlan != null) {
                mdlPlan es = new mdlPlan();
                es.setEstado(Integer.parseInt(estado));
                es.setIdplan(Integer.parseInt(idPlan));
                int res = es.cambiarEstado();
                out.println(true);
            }
            String nombreM = request.getParameter("txt_nombre");
            String valorM = request.getParameter("txt_valor");
            String descuentoM = request.getParameter("txt_descuento");
            String diasM = request.getParameter("txt_dias");
            String idPlanM = request.getParameter("txt_idPlan");
            String modificar = request.getParameter("btnModificar");

            if (modificar != null) {
                mdlPlan mod = new mdlPlan();
                mod.setNombre(nombreM);
                mod.setValor(Integer.parseInt(valorM));
                mod.setDescuento(Integer.parseInt(descuentoM));
                mod.setDias_cliente(Integer.parseInt(diasM));
                mod.setIdplan(Integer.parseInt(idPlanM));
                if (mod.modificarPlan() == 1) {
                    out.println("<script>\n"
                            + "alert(\"modificado\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarPlan.jsp');\n"
                            + "</script>");
                } else {
                    out.println("<script>\n"
                            + "alert(\"error\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarPlan.jsp');\n"
                            + "</script>");
                }
            }

            String idPersona = request.getParameter("idPlan");

            if (idPersona != null) {
                mdlPlan mod = new mdlPlan();
                mod.setIdpersona(Integer.parseInt(idPersona));

            }

            String idPlanC = request.getParameter("slcPlan");
            String idPersonaC = request.getParameter("txtIdPersona");
            String fechPlanC = request.getParameter("txtFechaPlan");
            String guardar = request.getParameter("btnGuardarPlan");
            if (guardar != null) {
                mdlPlan planP = new mdlPlan();
                mdlExtras cFecha = new mdlExtras();
                cFecha.setFecha(fechPlanC);
                planP.setIdpersona(Integer.parseInt(idPersonaC));
                planP.setIdplan(Integer.parseInt(idPlanC));
                planP.setFecha_inicio(fechPlanC = cFecha.convertirFecha());
                ResultSet diasP = planP.consultarFechaFinal();
                if (diasP.next()) {
                    int diasPlan = diasP.getInt("plan");
                    int diasPromo = diasP.getInt("promocion");
                    int diasTotal = diasPlan + diasPromo;
                    cFecha.setDiasFecha(diasTotal);
                    cFecha.setFecha(fechPlanC);
                    String fechaFinal = cFecha.agregarDiasF();
                    planP.setFecha_final(fechaFinal);
                }
                if (planP.registrarPlanPersona() == 1) {
                    out.println("<script>\n"
                            + "alert(\"modificado\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultaDetalladaP.jsp');\n"
                            + "</script>");
                }

            }

        }
    }

    public ResultSet listarTipoPersona() throws ClassNotFoundException, SQLException {
        mdlPlan lisT = new mdlPlan();
        return lisT.listarTp();
    }

    public ResultSet listarPlan() throws ClassNotFoundException, SQLException {
        mdlPlan listP = new mdlPlan();
        return listP.listarPlan();
    }

    public ResultSet listarPromcion() throws ClassNotFoundException, SQLException {
        mdlPromocion pro = new mdlPromocion();
        return pro.consultarPromocion();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPlan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPlan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ctrPlan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPlan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPlan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ctrPlan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
