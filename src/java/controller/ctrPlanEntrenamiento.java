/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlExtras;
import model.mdlPlanEntrenamiento;
import org.json.simple.JSONObject;

/**
 *
 * @author daniel
 */
public class ctrPlanEntrenamiento extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        //response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            JSONObject obj = new JSONObject();
            String idPersona = request.getParameter("idPerson");
            String fechaEntrenamiento = request.getParameter("dateTraining");
            String[] clasesEntrena = request.getParameterValues("trainingClass[]");
            String objetivo = request.getParameter("objetive");

            if (idPersona != null && fechaEntrenamiento != null && clasesEntrena.length > 0 && objetivo != null) {
                mdlExtras fecha = new mdlExtras();
                mdlPlanEntrenamiento planE = new mdlPlanEntrenamiento();
                fecha.setFecha(fechaEntrenamiento);
                String fechaF = fecha.convertirFecha();
                planE.setIdPersona(Integer.parseInt(idPersona));
                planE.setFechaPlanEntrenamieno(fechaF);
                planE.setObjetivo(objetivo);
                try {
                    ResultSet value = planE.insertarPlanEntrenamiento();
                    if (value.next()) {
                        planE.setIdPlanEntrenamiento(value.getInt("idPlan"));
                        planE.setFechaInicio(fechaF);
                        fecha.setDiasFecha(30);
                        fecha.setFecha(fechaF);
                        planE.setFechaFinal(fechaEntrenamiento = fecha.agregarDiasF());
                        for (int i = 0; i < clasesEntrena.length; i++) {
                            planE.setIdCronograma(Integer.parseInt(clasesEntrena[i]));
                            planE.insertarDetalleEntrenamiento();
                        }
                        obj.put("error", false);

                        out.print(obj);
                        out.flush();
                    }
                } catch (Exception e) {
                    System.out.println("Error:  " + e);
                }
            }
            /*else {
                obj.put("error", true);
                out.print(obj);
            }*/

            String idPersonaLoad = request.getParameter("request");
            if (idPersonaLoad != null) {

                List<String> lunes = new ArrayList();
                List<String> martes = new ArrayList();
                List<String> miercoles = new ArrayList();
                List<String> jueves = new ArrayList();
                List<String> viernes = new ArrayList();
                List<String> sabado = new ArrayList();
                List<String> domingo = new ArrayList();

                mdlPlanEntrenamiento find = new mdlPlanEntrenamiento();
                find.setIdPersona(Integer.parseInt(idPersonaLoad));
                try {

                    ResultSet data = find.consultarPlanEntrenamientoCliente();

                    out.println();

                    while (data.next()) {

                        if (data.getInt("iddia") == 1) {
                            lunes.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
                        if (data.getInt("iddia") == 2) {
                            martes.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
                        if (data.getInt("iddia") == 3) {
                            miercoles.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
                        if (data.getInt("iddia") == 4) {
                            jueves.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
                        if (data.getInt("iddia") == 5) {
                            viernes.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
                        if (data.getInt("iddia") == 6) {
                            sabado.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
                        if (data.getInt("iddia") == 7) {
                            domingo.add("<div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">\n"
                                    + "                        <div class=\"tile bg-blue\">\n"
                                    + "                            <div class=\"t-header th-alt\">\n"
                                    + "                                <div class=\"th-title\">" + data.getString("nombre") + "</div>\n"
                                    + "                            </div>\n"
                                    + "                            <div class=\"t-body tb-padding\">\n"
                                    + "                                <p>" + data.getString("descripcion") + "</p>\n"
                                    + "                                <p>Hora Inicio: " + data.getString("hora_inicio") + "</p>\n"
                                    + "                            </div>\n"
                                    + "                        </div>\n"
                                    + "                    </div>");
                        }
//                        out.println(data.getString("iddetalle_plan_entrenamiento") + " "
//                                + data.getString("fecha_inicioplan") + " "
//                                + data.getString("fecha_finplan") + " "
//                                + data.getString("idplan_entrenamiento") + " "
//                                + data.getString("idcronograma") + " "
//                                + data.getString("nombre") + " "
//                                + data.getString("descripcion") + " "
//                                + data.getString("primer_nombre") + " "
//                                + data.getString("primer_apellido") + " "
//                                + data.getString("hora_inicio") + " "
//                                + data.getString("hora_fin") + " "
//                                + data.getString("iddia") + " "
//                                + data.getString("nombre_dia") + " "
//                                + data.getString("descripcion_dia"));
                    }

                } catch (Exception e) {

                    System.out.println("error: " + e.getMessage());
                }

                obj.put("lunes", lunes);
                obj.put("martes", martes);
                obj.put("miercoles", miercoles);
                obj.put("jueves", jueves);
                obj.put("viernes", viernes);
                obj.put("sabado", sabado);
                obj.put("domingo", domingo);

                out.print(obj);
                out.flush();

            } else {
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPlanEntrenamiento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPlanEntrenamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPlanEntrenamiento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPlanEntrenamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
