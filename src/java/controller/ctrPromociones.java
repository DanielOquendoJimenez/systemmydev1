/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlPromocion;
import java.sql.ResultSet;

/**
 *
 * @author daniel
 */
public class ctrPromociones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            String nombrePromocion = request.getParameter("nombreP");
            String valor = request.getParameter("valorD");
            String descripcion = request.getParameter("descripcion");

            if (nombrePromocion != null && valor != null) {
                mdlPromocion pro = new mdlPromocion();
                pro.setNombreP(nombrePromocion);
                pro.setValorP(Integer.parseInt(valor));
                pro.setRestriccionP(descripcion);
                if (pro.insertarPromocion() == 1) {
                    out.println(true);
                } else {
                    out.println(false);
                }
            }
            String listarT = request.getParameter("listar");

            if (listarT != null) {
                mdlPromocion lis = new mdlPromocion();
                ResultSet res = lis.consultarPromocion();
                out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                        + "            <thead>\n"
                        + "                <tr>\n"
                        + "                    <th hidden>idpromociones</th>\n"
                        + "                    <th>nombre</th>\n"
                        + "                    <th>valor</th>\n"
                        + "                    <th>restriccion</th>\n"
                        + "                    <th>Accion</th>\n"
                        + "                </tr>\n"
                        + "            </thead>\n"
                        + "            <tbody>");

                while (res.next()) {
                    out.println("<tr>\n"
                            + "                    <td hidden>" + res.getString("idpromociones") + "</td>\n"
                            + "                    <td>" + res.getString("nombre") + "</td>\n"
                            + "                    <td>" + res.getString("valor") + "</td>\n"
                            + "                    <td>" + res.getString("restriccion") + "</td>\n"
                            + "                    <td><button class=\"btn btn-primary\"  id=\"btn_EditarPromocion\" name=\"btn_EditarPromocion\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button> <button  class=\"btn btn-danger\" onclick=\"eliminarPromocion(" + res.getString("idpromociones") + ")\" id=\"btn_EliminarPromocion\" name=\"btn_EliminarPromocion\"><i class=\"zmdi zmdi-delete m-r-5\"></i></button></td>\n"
                            + "                </tr>");
                }
                out.println("  </tbody>\n"
                        + "        </table>");
            }

            String idPromocion = request.getParameter("idPromocion");
            if (idPromocion != null) {
                mdlPromocion eli = new mdlPromocion();
                eli.setIdpromociones(Integer.parseInt(idPromocion));
                if (eli.eliminarPromocion() == 1) {
                    mdlPromocion lis = new mdlPromocion();
                    ResultSet res = lis.consultarPromocion();
                    out.println("<table id=\"myTable\" class=\"table table-bordered table-vmiddle\">\n"
                            + "            <thead>\n"
                            + "                <tr>\n"
                            + "                    <th hidden>idpromociones</th>\n"
                            + "                    <th>nombre</th>\n"
                            + "                    <th>valor</th>\n"
                            + "                    <th>restriccion</th>\n"
                            + "                    <th>Accion</th>\n"
                            + "                </tr>\n"
                            + "            </thead>\n"
                            + "            <tbody>");

                    while (res.next()) {
                        out.println("<tr>\n"
                                + "                    <td hidden>" + res.getString("idpromociones") + "</td>\n"
                                + "                    <td>" + res.getString("nombre") + "</td>\n"
                                + "                    <td>" + res.getString("valor") + "</td>\n"
                                + "                    <td>" + res.getString("restriccion") + "</td>\n"
                                + "                    <td><button class=\"btn btn-primary\"  id=\"btn_EditarPromocion\" name=\"btn_EditarPromocion\"><i class=\"zmdi zmdi-edit m-r-5\"></i></button> <button  class=\"btn btn-danger\" onclick=\"eliminarPromocion(" + res.getString("idpromociones") + ")\" id=\"btn_EliminarPromocion\" name=\"btn_EliminarPromocion\"><i class=\"zmdi zmdi-delete m-r-5\"></i></button></td>\n"
                                + "                </tr>");
                    }
                    out.println("  </tbody>\n"
                            + "        </table>");
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPromociones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPromociones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPromociones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPromociones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
