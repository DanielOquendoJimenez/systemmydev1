/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlLogin;
import model.mdlExtras;
import java.sql.ResultSet;
import javax.servlet.http.HttpSession;
import library.StringEncrypt;

/**
 *
 * @author daniel
 */
@WebServlet(name = "ctrLogin", urlPatterns = {"/ctrLogin"})
public class ctrLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String usuario = request.getParameter("txtUsuario");
            String clave = request.getParameter("txtClave");
            String btnIniciar = request.getParameter("btnIniciar");
            //String r = "";
            int ban = 0;

            if (usuario != "" && clave != "") {
                mdlLogin md = new mdlLogin();
                md.setUsuario(usuario);
                ResultSet rs = md.validarUsuario();
                if (rs.next()) {
                    if (StringEncrypt.decrypt(rs.getString("clave")).equals(clave)) {
                        if (rs.getString("idroll").equals("1")) {
                            HttpSession sc = request.getSession(true);
                            sc.setAttribute("idusuario", rs.getString("idusuario"));
                            sc.setAttribute("nombre", rs.getString("usuario"));
                            sc.setAttribute("roll", rs.getString("idroll"));
                            sc.setAttribute("idpersona", rs.getString("idpersona"));
                            sc.setAttribute("primer_nombre", rs.getString("primer_nombre"));
                            sc.setAttribute("segundo_nombre", rs.getString("segundo_nombre"));
                            sc.setAttribute("primer_apellido", rs.getString("primer_apellido"));
                            sc.setAttribute("segundoApellido", rs.getString("segundoApellido"));
                            sc.setAttribute("fecha_nacimiento", rs.getString("fecha_nacimiento"));
                            sc.setAttribute("correo", rs.getString("correo"));
                            sc.setAttribute("telefono", rs.getString("telefono"));
                            sc.setAttribute("Celular", rs.getString("Celular"));
                            sc.setAttribute("documento_identidad", rs.getString("documento_identidad"));
                            sc.setAttribute("nspecialidad", rs.getString("nspecialidad"));
                            sc.setAttribute("estado", rs.getString("estado"));
                            sc.setAttribute("foto", rs.getString("foto"));
                            sc.setAttribute("idTipo_persona", rs.getString("idTipo_persona"));
                            ban = 1;
                        } else if (rs.getString("idroll").equals("2")) {
                            HttpSession sc = request.getSession(true);
                            sc.setAttribute("idusuario", rs.getString("idusuario"));
                            sc.setAttribute("nombre", rs.getString("usuario"));
                            sc.setAttribute("roll", rs.getString("idroll"));
                            sc.setAttribute("idpersona", rs.getString("idpersona"));
                            sc.setAttribute("primer_nombre", rs.getString("primer_nombre"));
                            sc.setAttribute("segundo_nombre", rs.getString("segundo_nombre"));
                            sc.setAttribute("primer_apellido", rs.getString("primer_apellido"));
                            sc.setAttribute("segundoApellido", rs.getString("segundoApellido"));
                            sc.setAttribute("fecha_nacimiento", rs.getString("fecha_nacimiento"));
                            sc.setAttribute("correo", rs.getString("correo"));
                            sc.setAttribute("telefono", rs.getString("telefono"));
                            sc.setAttribute("Celular", rs.getString("Celular"));
                            sc.setAttribute("documento_identidad", rs.getString("documento_identidad"));
                            sc.setAttribute("nspecialidad", rs.getString("nspecialidad"));
                            sc.setAttribute("estado", rs.getString("estado"));
                            sc.setAttribute("foto", rs.getString("foto"));
                            sc.setAttribute("idTipo_persona", rs.getString("idTipo_persona"));
                            ban = 2;
                        } else if (rs.getString("idroll").equals("3")) {
                            HttpSession sc = request.getSession(true);
                            mdlExtras rfecha = new mdlExtras();
                            sc.setAttribute("idusuario", rs.getString("idusuario"));
                            sc.setAttribute("nombre", rs.getString("usuario"));
                            sc.setAttribute("clave", rs.getString("clave"));
                            sc.setAttribute("roll", rs.getString("idroll"));
                            sc.setAttribute("idpersona", rs.getString("idpersona"));
                            sc.setAttribute("primer_nombre", rs.getString("primer_nombre"));
                            sc.setAttribute("segundo_nombre", rs.getString("segundo_nombre"));
                            sc.setAttribute("primer_apellido", rs.getString("primer_apellido"));
                            sc.setAttribute("segundoApellido", rs.getString("segundoApellido"));
                            rfecha.setFecha(rs.getString("fecha_nacimiento"));
                            String fecha = rfecha.revertirFecha();
                            sc.setAttribute("fecha_nacimiento",fecha);
                            sc.setAttribute("correo", rs.getString("correo"));
                            sc.setAttribute("telefono", rs.getString("telefono"));
                            sc.setAttribute("Celular", rs.getString("Celular"));
                            sc.setAttribute("documento_identidad", rs.getString("documento_identidad"));
                            sc.setAttribute("nspecialidad", rs.getString("nspecialidad"));
                            sc.setAttribute("estado", rs.getString("estado"));
                            sc.setAttribute("foto", rs.getString("foto"));
                            sc.setAttribute("idTipo_persona", rs.getString("idTipo_persona"));

                            ban = 3;
                        }
                    } else {
                        ban = 4;
                    }
                } else {
                    ban = 4;
                }
                if (ban == 1) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/inicio.jsp');\n"
                            + "</script>");
                } else if (ban == 2) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/inicio.jsp');\n"
                            + "</script>");
                } else if (ban == 3) {
                    out.println("<script>\n"
                            + "document.location=('/SystemMYDEv1/view/cliente/inicioCliente.jsp');\n"
                            + "</script>");
                } else if (ban == 4) {
                    out.println("<script>\n"
                            + "alert(\"usuario o contraseña incorrectos\");\n"
                            + "document.location=('/SystemMYDEv1/index.jsp');\n"
                            + "</script>");
                }
            }

            String usuarioR = request.getParameter("usuario");
            String email = request.getParameter("email");

            if (usuarioR != null && email != null) {
                mdlLogin rec = new mdlLogin();
                rec.setUsuario(usuarioR);
                ResultSet res = rec.validarUsuario();
                if (res.next()) {
                    int Nclave = (int) (Math.random() * 1000000000 + 1);
                    String idUsuario = res.getString("idusuario");
                    mdlLogin cam = new mdlLogin();
                    cam.setIdusuario(Integer.parseInt(idUsuario));
                    cam.setClave(StringEncrypt.encrypt(Integer.toString(Nclave)));
                    if (cam.modificarUsuario() == 1) {
                        mdlExtras cor = new mdlExtras();
                        cor.setEmail(email);
                        cor.setClaveEmail(Integer.toString(Nclave));
                        String re = cor.enviarEmail();

                    }
                    out.println(true);
                } else {

                }

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ctrLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ctrLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
