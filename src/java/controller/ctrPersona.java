/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.mdlPersona;
import model.mdlLogin;
import library.StringEncrypt;
import model.mdlExtras;
import model.mdlPlan;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author daniel
 */
@WebServlet(name = "ctrPersona", urlPatterns = {"/ctrPersona"})
public class ctrPersona extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            request.setCharacterEncoding("UTF-8");
            //validar documento---------------------------------------------------------
            String Vdoc = request.getParameter("valor");
            boolean ban = false;
            if (Vdoc != null) {
                mdlPersona vdp = new mdlPersona();
                vdp.setDocumento_identidad(Vdoc);
                ResultSet rs = vdp.ValidarDocumento();
                if (rs.next()) {
                    if (rs.getString("documento_identidad").equals(Vdoc)) {
                        out.println("echo");
                        //r = "bienvenido()";
                    } else {
                        out.println("echo");
                        //r = "bienvenido()";
                    }
                    //request.setAttribute("r", r);
                }
            }
            //fin validar documento---------------------------------------------------------

            //REGISTRAR PERSONA ----------------------------------------------------------------
            // configures upload settings
            DiskFileItemFactory factory = new DiskFileItemFactory();

            // sets temporary location to store files
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

            ServletFileUpload upload = new ServletFileUpload(factory);

            // constructs the directory path to store upload file
            // this path is relative to application's directory
            String uploadPath = getServletContext().getRealPath("")
                    + File.separator + "upload";

            // creates the directory if it does not exist
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            try {
                List<FileItem> formItems = upload.parseRequest(request); //ARRAY
                if (formItems != null && formItems.size() > 0) {
                    // iterates over form's fields
                    for (FileItem item : formItems) {
                        // processes only fields that are not form fields
                        if (!item.isFormField()) {
                            String fileName = new File(item.getName()).getName();
                            String filePath = uploadPath + File.separator + fileName;
                            File storeFile = new File(filePath);
                            item.write(storeFile);

                            String primer_nombre = formItems.get(2).getString();//RECORRE LA ARRAY
                            String segundo_nombre = formItems.get(3).getString();
                            String primer_apellido = formItems.get(4).getString();
                            String segundoApellido = formItems.get(5).getString();
                            String celular = formItems.get(9).getString();
                            String documento = formItems.get(6).getString();
                            String correo = formItems.get(10).getString();
                            String fechaNacimiento = formItems.get(7).getString();
                            String profecion = formItems.get(8).getString();
                            String telefono = formItems.get(11).getString();
                            String idTipo_persona = formItems.get(1).getString();
                            String fechaPlan = formItems.get(12).getString();
                            String planPersona = formItems.get(13).getString();

                            int estado = 1;

                            mdlExtras fec = new mdlExtras();//convertir fecha
                            fec.setFecha(fechaNacimiento);

                            mdlPersona per = new mdlPersona();//envia a modelo
                            per.setPrimer_nombre(primer_nombre);
                            per.setSegundo_nombre(segundo_nombre);
                            per.setPrimer_apellido(primer_apellido);
                            per.setSegundoApellido(segundoApellido);
                            per.setCelular(celular);
                            per.setDocumento_identidad(documento);
                            per.setCorreo(correo);
                            per.setFecha_nacimiento(fechaNacimiento = fec.convertirFecha());
                            per.setNspecialidad(profecion);
                            per.setTelefono(telefono);
                            per.setEstado(estado);
                            per.setIdTipo_persona(Integer.parseInt(idTipo_persona));
                            per.setFoto(fileName);

                            if (per.insertarPersona() == 1) {
                                mdlLogin lo = new mdlLogin();//genera el usuario
                                mdlPersona idp = new mdlPersona();
                                idp.setDocumento_identidad(documento);
                                ResultSet rs = idp.optenerIdpersona();
                                if (rs.next()) {
                                    String id = rs.getString("idpersona");
                                    lo.setUsuario(documento);
                                    lo.setClave(StringEncrypt.encrypt(primer_apellido));
                                    lo.setIdpersona(Integer.parseInt(id));
                                    if (Integer.parseInt(idTipo_persona) == 1) {
                                        int roll = 1;
                                        lo.setIdroll(roll);
                                    } else if (Integer.parseInt(idTipo_persona) == 2) {
                                        int roll = 2;
                                        lo.setIdroll(roll);
                                    } else if (Integer.parseInt(idTipo_persona) == 3 || Integer.parseInt(idTipo_persona) == 4) {
                                        int roll = 3;
                                        lo.setIdroll(roll);
                                        if (fechaPlan != "" && planPersona != "") {
                                            mdlPlan plan = new mdlPlan();
                                            mdlExtras fecPlan = new mdlExtras();
                                            fecPlan.setFecha(fechaPlan);
                                            plan.setFecha_inicio(fechaPlan = fecPlan.convertirFecha());
                                            plan.setIdplan(Integer.parseInt(planPersona));
                                            plan.setIdpersona(Integer.parseInt(id));
                                            ResultSet diasP = plan.consultarFechaFinal();
                                            if (diasP.next()) {
                                                int diasTotal = 0;
                                                if (diasP.getString("promocion") != null && diasP.getString("plan") != null) {
                                                    String diasPro = diasP.getString("promocion");
                                                    String diasPlan = diasP.getString("plan");
                                                    diasTotal = Integer.parseInt(diasPro) + Integer.parseInt(diasPlan);
                                                } else {
                                                    String diasPro = diasP.getString("dias_cliente");
                                                    diasTotal = Integer.parseInt(diasPro);
                                                }
                                                fecPlan.setDiasFecha(diasTotal);
                                                fecPlan.setFecha(fechaPlan = fecPlan.convertirFecha());
                                                String fechaFinal = fecPlan.agregarDiasF();
                                                plan.setFecha_final(fechaFinal);
                                            }
                                            if (plan.registrarPlanPersona() == 1) {

                                            }
                                        }
                                    } else {
                                        out.println("<script>\n"
                                                + "alert(\"Guardado\");\n"
                                                + "document.location=('/SystemMYDEv1/view/registrarPersona.jsp');\n"
                                                + "</script>");
                                    }
                                    if (lo.registrarUsuario() == 1) {
                                        mdlExtras email = new mdlExtras();
                                        email.setEmail(correo);
                                        email.setUsuarioEmail(documento);
                                        email.setClaveEmail(primer_apellido);
                                        String res = email.enviarEmail();
                                    }
                                }
                                out.println("<script>\n"
                                        + "alert(\"Usuario: '" + documento + "'   Contraseña: '" + primer_apellido + "'\");\n"
                                        + "document.location=('/SystemMYDEv1/view/registrarPersona.jsp');\n"
                                        + "</script>");
                            } else {
                                out.println("<script>\n"
                                        + "alert(\"Error\");\n"
                                        + "document.location=('/SystemMYDEv1/view/registrarPersona.jsp');\n"
                                        + "</script>");
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                request.setAttribute("message", "There was an error: " + ex.getMessage());
            }
            //TERMINO DE RESGISTRAR LA PERSONA ----------------------------------------------------------------

            //MODIFICAR PERSONA----------------------------------------------------------------
            String btnModificar = "btn";
            String primer_nombreM = request.getParameter("txtPrimerNombreM");
            String segundo_nombreM = request.getParameter("txtSegundoNombreM");
            String primer_apellidoM = request.getParameter("txtPrimerApellidoM");
            String segundoApellidoM = request.getParameter("txtSegundoApellidoM");
            String celularM = request.getParameter("txtCelularM");
            String documentoM = request.getParameter("txtDocumentoM");
            String correoM = request.getParameter("txtCorreoM");
            String fechaNacimientoM = request.getParameter("txtFechaM");
            String profecionM = request.getParameter("txtProfecionM");
            String telefonoM = request.getParameter("txtTelefonoM");
            String idPersona = request.getParameter("txtIdpersonaM");
            String roll = request.getParameter("txtRollM");
            if (btnModificar != null) {
                mdlExtras fecha = new mdlExtras();
                fecha.setFecha(fechaNacimientoM);
                mdlPersona mdf = new mdlPersona();
                mdf.setPrimer_nombre(primer_nombreM);
                mdf.setSegundo_nombre(segundo_nombreM);
                mdf.setPrimer_apellido(primer_apellidoM);
                mdf.setSegundoApellido(segundoApellidoM);
                mdf.setCelular(celularM);
                mdf.setDocumento_identidad(documentoM);
                mdf.setCorreo(correoM);
                mdf.setFecha_nacimiento(fechaNacimientoM = fecha.convertirFecha());
                mdf.setNspecialidad(profecionM);
                mdf.setTelefono(telefonoM);
                mdf.setIdpersona(Integer.parseInt(idPersona));
                if (mdf.modificarP() == 1) {
                    if (roll.equals("1") || roll.equals("2")) {
                        out.println("<script>\n"
                                + "alert(\"modificado\");\n"
                                + "document.location=('/SystemMYDEv1/view/consultarPersona.jsp');\n"
                                + "</script>");
                    } else {
                        out.println("<script>\n"
                                + "alert(\"modificado\");\n"
                                + "document.location=('/SystemMYDEv1/view/cliente/cambiarDatos.jsp');\n"
                                + "</script>");

                    }

                } else {
                    out.println("<script>\n"
                            + "alert(\"Error\");\n"
                            + "document.location=('/SystemMYDEv1/view/consultarPersona.jsp');\n"
                            + "</script>");
                }
            }

            //CERRAR MODIFICAR PERSONA----------------------------------------------------------------
            //validar que la contraseña anerior sea igual---------------------------------------------
            String usuarioC = request.getParameter("usuario");
            String claveC = request.getParameter("clave");

            if (usuarioC != null && claveC != null) {
                mdlLogin con = new mdlLogin();
                con.setUsuario(usuarioC);
                ResultSet res = con.validarUsuario();
                if (res.next()) {
                    if (StringEncrypt.decrypt(res.getString("clave")).equals(claveC)) {

                    } else {
                        out.println("0");
                    }
                } else {
                    out.println("Error Al recorrer");

                }
            }
            //cerro validar que la contraseña anerior sea igual---------------------------------------------
            //cambiar contraseña----------------------------------------------------------------------------
            String nuevaClave = request.getParameter("txtNuevaClaveC");
            String nuevaClaveC = request.getParameter("txtConfirmaClaveC");
            String idPersonaC = request.getParameter("txtId");

            if (nuevaClave.equals("") && nuevaClaveC.equals("")) {
                out.println("<script>\n"
                        + "alert(\"compruebe los campos \");\n"
                        + "document.location=('/SystemMYDEv1/view/cliente/cambiarDatos.jsp');\n"
                        + "</script>");
            } else if (nuevaClave.equals(nuevaClaveC)) {
                mdlLogin cam = new mdlLogin();
                cam.setIdusuario(Integer.parseInt(idPersonaC));
                cam.setClave(StringEncrypt.encrypt(nuevaClave));
                if (cam.modificarUsuario() == 1) {
                    out.println("<script>\n"
                            + "alert(\"guardado\");\n"
                            + "document.location=('/SystemMYDEv1/view/cliente/cambiarDatos.jsp');\n"
                            + "</script>");
                }
            } else {
                out.println("<script>\n"
                        + "alert(\"compruebe los campos \");\n"
                        + "document.location=('/SystemMYDEv1/view/cliente/cambiarDatos.jsp');\n"
                        + "</script>");
            }
            //cerro cambiar contraseña----------------------------------------------------------------------------

        }
    }

    // LISTA SELECT EN LA VISTA TIPO PERSONA
    public ResultSet listarTP() throws ClassNotFoundException, SQLException {
        mdlPersona pe = new mdlPersona();
        return pe.listarTipoPersona();
    }

    //MUESTRA TODAS LAS PERDONAS EN LA VISTA
    public ResultSet consultarP() throws ClassNotFoundException, SQLException {
        mdlPersona per = new mdlPersona();
        return per.consultarP();
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPersona.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPersona.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ctrPersona.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrPersona.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrPersona.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ctrPersona.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
