/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.mdlPersona;

/**
 *
 * @author daniel
 */
public class ctrConsultaD extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String documento = request.getParameter("documento");
            if (documento != null) {
                mdlPersona idpersona = new mdlPersona();
                idpersona.setDocumento_identidad(documento);
                ResultSet res = idpersona.optenerIdpersona();
                if (res.next()) {
                    int id = res.getInt("idpersona");
                    idpersona.setIdpersona(id);
                    ResultSet data = idpersona.consultarPm();
                    if (data.next()) {
                        out.println("<div class=\"container\">\n"
                                + "    <div class=\"tile\" id=\"profile-main\">\n"
                                + "        <div class=\"pm-overview c-overflow-dark\">\n"
                                + "            <div class=\"pmo-pic\">\n"
                                + "                <div class=\"p-relative\">\n"
                                + "                    <div class=\"panel panel-default\">\n"
                                + "                        <div class=\"panel-body\">\n"
                                + "                            <div class=\"form-group\">\n"
                                + "                                <div id=\"form1\" runat=\"server\">\n"
                                + "                                    <img id=\"image_upload_preview\" height=\"259\" width=\"259\"  src=\"../upload/"+data.getString("foto")+"\" value=\"img/profile-pic.jpg\" />\n"
                                + "                                    <input class=\"btn btn-file\" type='file' id=\"inputFile\" name=\"fleFoto\"  value=\"img/profile-pic.jpg\" />\n"
                                + "                                </div>\n"
                                + "                            </div>\n"
                                + "                        </div>\n"
                                + "                    </div>\n"
                                + "                </div>\n"
                                + "            </div>\n"
                                + "            <div class=\"pmo-block pmo-contact hidden-xs\">\n"
                                + "                <h2>Contact</h2>\n"
                                + "                <ul>\n"
                                + "                    <li><i class=\"zmdi zmdi-phone\"></i> 00971 12345678 9</li>\n"
                                + "                    <li><i class=\"zmdi zmdi-email\"></i> malinda-h@gmail.com</li>\n"
                                + "                    <li><i class=\"zmdi zmdi-facebook-box\"></i> malinda.hollaway</li>\n"
                                + "                    <li><i class=\"zmdi zmdi-twitter\"></i> @malinda (twitter.com/malinda)</li>\n"
                                + "                </ul>\n"
                                + "            </div>\n"
                                + "        </div>\n"
                                + "        <div>\n"
                                + "            <div class=\"pm-body clearfix\">\n"
                                + "                <ul class=\"nav nav-tabs\" role=\"tablist\">\n"
                                + "                    <li role=\"presentation\" class=\"active\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Datos Personales</a></li>\n"
                                + "                    <li role=\"presentation\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">planes adquiridos</a></li>\n"
                                + "                    <li role=\"presentation\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\"></a></li>\n"
                                + "                    <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\"></a></li>\n"
                                + "                </ul>\n"
                                + "                <div class=\"tab-content\">\n"
                                + "                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"home\">            \n"
                                + "                        <div class=\"pmb-block\">\n"
                                + "                            <div class=\"pmbb-header\">\n"
                                + "                                <h2><i class=\"zmdi zmdi-account m-r-5\"></i> Información Basica</h2>\n"
                                + "\n"
                                + "                                <ul class=\"actions\">\n"
                                + "                                    <li class=\"dropdown\">\n"
                                + "                                        <a href=\"#\" data-toggle=\"dropdown\">\n"
                                + "                                            <i class=\"zmdi zmdi-more-vert\"></i>\n"
                                + "                                        </a>\n"
                                + "\n"
                                + "                                        <ul class=\"dropdown-menu pull-right\">\n"
                                + "                                            <li>\n"
                                + "                                                <a data-pmb-action=\"edit\" href=\"#\">Editar</a>\n"
                                + "                                            </li>\n"
                                + "                                        </ul>\n"
                                + "                                    </li>\n"
                                + "                                </ul>\n"
                                + "                            </div>\n"
                                + "                            <div class=\"pmbb-body p-l-30\">\n"
                                + "                                <div class=\"pmbb-view\">\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Primer nombre</dt>\n"
                                + "                                        <dd>"+data.getString("primer_nombre")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Segundo nombre</dt>\n"
                                + "                                        <dd>"+data.getString("segundo_nombre")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Primer apellido</dt>\n"
                                + "                                        <dd>"+data.getString("primer_apellido")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Segundo apellido</dt>\n"
                                + "                                        <dd>"+data.getString("segundoApellido")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Documento</dt>\n"
                                + "                                        <dd>"+data.getString("documento_identidad")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Fecha de nacimiento</dt>\n"
                                + "                                        <dd>"+data.getString("fecha_nacimiento")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Profeciono</dt>\n"
                                + "                                        <dd>"+data.getString("nspecialidad")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                </div>\n"
                                + "                                <div class=\"pmbb-edit\">\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Primer nombre</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("primer_nombre")+"\" placeholder=\"eg. Mallinda Hollaway\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Segundo nombre</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("segundo_nombre")+"\" placeholder=\"eg. Mallinda Hollaway\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Primer apellido</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"dtp-container dropdown fg-line\">\n"
                                + "                                                <input type='text' class=\"form-control date-picker\" data-toggle=\"dropdown\" placeholder=\"Click here...\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Segundo apellido</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("segundoApellido")+"\" placeholder=\"eg. Mallinda Hollaway\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Documento</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("documento_identidad")+"\" placeholder=\"eg. Mallinda Hollaway\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Fecha de nacimiento</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"dtp-container dropdown fg-line\">\n"
                                + "                                                <input type='text' class=\"form-control date-picker\" data-toggle=\"dropdown\" placeholder=\"Click here...\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Profecion</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"dtp-container dropdown fg-line\">\n"
                                + "                                                <input type='text' class=\"form-control\"  data-toggle=\"dropdown\" placeholder=\"Click here...\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "\n"
                                + "                                    <div class=\"m-t-30\">\n"
                                + "                                        <button class=\"btn btn-primary btn-sm\">Guardar</button>\n"
                                + "                                        <button data-pmb-action=\"reset\" class=\"btn btn-link btn-sm\">Cancelar</button>\n"
                                + "                                    </div>\n"
                                + "                                </div>\n"
                                + "                            </div>\n"
                                + "                        </div>\n"
                                + "\n"
                                + "                        <div class=\"pmb-block\">\n"
                                + "                            <div class=\"pmbb-header\">\n"
                                + "                                <h2><i class=\"zmdi zmdi-phone m-r-5\"></i> Informació de contacto</h2>\n"
                                + "\n"
                                + "                                <ul class=\"actions\">\n"
                                + "                                    <li class=\"dropdown\">\n"
                                + "                                        <a href=\"#\" data-toggle=\"dropdown\">\n"
                                + "                                            <i class=\"zmdi zmdi-more-vert\"></i>\n"
                                + "                                        </a>\n"
                                + "\n"
                                + "                                        <ul class=\"dropdown-menu pull-right\">\n"
                                + "                                            <li>\n"
                                + "                                                <a data-pmb-action=\"edit\" href=\"#\">Editar</a>\n"
                                + "                                            </li>\n"
                                + "                                        </ul>\n"
                                + "                                    </li>\n"
                                + "                                </ul>\n"
                                + "                            </div>\n"
                                + "                            <div class=\"pmbb-body p-l-30\">\n"
                                + "                                <div class=\"pmbb-view\">\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Celular</dt>\n"
                                + "                                        <dd>"+data.getString("Celular")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Telefono fijo</dt>\n"
                                + "                                        <dd>"+data.getString("telefono")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Correo</dt>\n"
                                + "                                        <dd>"+data.getString("correo")+"</dd>\n"
                                + "                                    </dl>\n"
                                + "                                </div>\n"
                                + "\n"
                                + "                                <div class=\"pmbb-edit\">\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Celular</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("Celular")+"\" placeholder=\"eg. 00971 12345678 9\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Telefono Fijo</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("telefono")+"\" placeholder=\"eg. malinda.h@gmail.com\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Correo</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" value=\""+data.getString("correo")+"\" placeholder=\"eg. @malinda\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <div class=\"m-t-30\">\n"
                                + "                                        <button class=\"btn btn-primary btn-sm\">Guardar</button>\n"
                                + "                                        <button data-pmb-action=\"reset\" class=\"btn btn-link btn-sm\">Cancelar</button>\n"
                                + "                                    </div>\n"
                                + "                                </div>\n"
                                + "                            </div>\n"
                                + "                        </div>\n"
                                + "\n"
                                + "                        <div class=\"pmb-block\">\n"
                                + "                            <div class=\"pmbb-header\">\n"
                                + "                                <h2><i class=\"zmdi zmdi-lock m-r-5\"></i> Accseso</h2>\n"
                                + "                                <ul class=\"actions\">\n"
                                + "                                    <li class=\"dropdown\">\n"
                                + "                                        <a href=\"#\" data-toggle=\"dropdown\">\n"
                                + "                                            <i class=\"zmdi zmdi-more-vert\"></i>\n"
                                + "                                        </a>\n"
                                + "\n"
                                + "                                        <ul class=\"dropdown-menu pull-right\">\n"
                                + "                                            <li>\n"
                                + "                                                <a data-pmb-action=\"edit\" href=\"#\">Editar</a>\n"
                                + "                                            </li>\n"
                                + "                                        </ul>\n"
                                + "                                    </li>\n"
                                + "                                </ul>\n"
                                + "                            </div>\n"
                                + "                            <div class=\"pmbb-body p-l-30\">\n"
                                + "                                <div class=\"pmbb-view\">\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Usuario</dt>\n"
                                + "                                        <dd></dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt>Contraseña</dt>\n"
                                + "                                        <dd></dd>\n"
                                + "                                    </dl>\n"
                                + "                                </div>\n"
                                + "                                <div class=\"pmbb-edit\">\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Usuario</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"text\" class=\"form-control\" placeholder=\"eg. 00971 12345678 9\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Contraseña anterior</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"email\" class=\"form-control\" placeholder=\"eg. malinda.h@gmail.com\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Nueva contraseña</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"email\" class=\"form-control\" placeholder=\"eg. malinda.h@gmail.com\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <dl class=\"dl-horizontal\">\n"
                                + "                                        <dt class=\"p-t-10\">Confirmar contraseña</dt>\n"
                                + "                                        <dd>\n"
                                + "                                            <div class=\"fg-line\">\n"
                                + "                                                <input type=\"email\" class=\"form-control\" placeholder=\"eg. malinda.h@gmail.com\">\n"
                                + "                                            </div>\n"
                                + "                                        </dd>\n"
                                + "                                    </dl>\n"
                                + "                                    <div class=\"m-t-30\">\n"
                                + "                                        <button class=\"btn btn-primary btn-sm\">Guardar</button>\n"
                                + "                                        <button data-pmb-action=\"reset\" class=\"btn btn-link btn-sm\">Cancelar</button>\n"
                                + "                                    </div>\n"
                                + "                                </div>\n"
                                + "                            </div>\n"
                                + "                        </div>\n"
                                + "                    </div>\n"
                                + "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"profile\">\n"
                                + "                        <div class=\"container\">\n"
                                + "                            <div class=\"tile\">\n"
                                + "                                <div class=\"action-header clearfix\">\n"
                                + "                                    <h2 class=\"ah-label hidden-xs\">Planes</h2>\n"
                                + "\n"
                                + "                                    <div class=\"ah-search\">\n"
                                + "                                        <input type=\"text\" placeholder=\"Start typing...\" class=\"ahs-input\">\n"
                                + "\n"
                                + "                                        <i class=\"ahs-close\">&times;</i>\n"
                                + "                                    </div>\n"
                                + "\n"
                                + "                                    <ul class=\"ah-actions actions\">\n"
                                + "                                        <li>\n"
                                + "                                            <a data-toggle=\"modal\" href=\"#modalDefault\"><i class=\"zmdi zmdi-plus-circle\"></i></a>\n"
                                + "                                        </li>\n"
                                + "                                    </ul>\n"
                                + "                                </div>\n"
                                + "                                <div class=\"p-timeline\">\n"
                                + "                                    <div class=\"pt-line c-gray text-right\">\n"
                                + "                                        <span class=\"d-block\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>\n"
                                + "                                        &nbsp;\n"
                                + "                                    </div>  \n"
                                + "                                    <div class=\"pt-body\">\n"
                                + "                                        <h2 class=\"ptb-title\">Plan Actual</h2>\n"
                                + "                                        <div class=\"lightbox clearfix\">\n"
                                + "                                            <ul class=\"clist clist-star\">\n"
                                + "                                                <li>Trimestre-02/05/2015</li>\n"
                                + "                                                <li>Estado\n"
                                + "                                                    <ul>\n"
                                + "                                                        <li>Activo</li>\n"
                                + "                                                    </ul>\n"
                                + "                                                </li>\n"
                                + "                                            </ul>\n"
                                + "                                        </div>\n"
                                + "                                    </div>\n"
                                + "                                </div>\n"
                                + "                                <div class=\"p-timeline\">\n"
                                + "                                    <div class=\"pt-line c-gray text-right\">\n"
                                + "                                        <span class=\"d-block\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>\n"
                                + "                                        &nbsp;\n"
                                + "                                    </div>  \n"
                                + "                                    <div class=\"pt-body\">\n"
                                + "                                        <h2 class=\"ptb-title\">Adquiridos Pilates</h2>\n"
                                + "                                        <div class=\"lightbox clearfix\">\n"
                                + "                                            <ul class=\"clist clist-star\">\n"
                                + "                                                <li>Trimestre-02/05/2015</li>                                              \n"
                                + "                                            </ul>\n"
                                + "                                        </div>\n"
                                + "                                    </div>\n"
                                + "                                </div>\n"
                                + "                                <div class=\"p-timeline\">\n"
                                + "                                    <div class=\"pt-line c-gray text-right\">\n"
                                + "                                        <span class=\"d-block\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>\n"
                                + "                                        &nbsp;\n"
                                + "                                    </div>  \n"
                                + "                                    <div class=\"pt-body\">\n"
                                + "                                        <h2 class=\"ptb-title\">Adquiridos Normal</h2>\n"
                                + "                                        <div class=\"lightbox clearfix\">\n"
                                + "                                            <ul class=\"clist clist-star\">\n"
                                + "                                                <li>Lorem ipsum dolor sit amet</li>\n"
                                + "                                            </ul>\n"
                                + "                                        </div>\n"
                                + "                                    </div>\n"
                                + "                                </div>\n"
                                + "                            </div>\n"
                                + "                        </div>\n"
                                + "                    </div>\n"
                                + "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"messages\">\n"
                                + "\n"
                                + "                    </div>\n"
                                + "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"settings\">\n"
                                + "\n"
                                + "                    </div>\n"
                                + "                </div>\n"
                                + "            </div>\n"
                                + "        </div>\n"
                                + "    </div> \n"
                                + "</div>"
                                + "<script src=\"js/functions.js\"></script>");
                    }
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrConsultaD.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrConsultaD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ctrConsultaD.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ctrConsultaD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
